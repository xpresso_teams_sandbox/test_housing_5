"""
This is the implementation of data preparation for sklearn
"""
# import system libraries
import sys
import logging
import os

# import common python libraries
import numpy as np
import pandas as pd
from itertools import cycle
import pickle

# import sklearn libraries
import sklearn
from sklearn import metrics
from sklearn.model_selection import GridSearchCV, validation_curve
from sklearn.preprocessing import label_binarize
from sklearn.utils._testing import ignore_warnings
from sklearn.model_selection import train_test_split
from sklearn.utils.multiclass import type_of_target
from sklearn.exceptions import ConvergenceWarning

# libraries for plotting
import matplotlib.pyplot as plt
import seaborn as sn
import matplotlib.colors as mcolors

# import xgboost libraries
import xgboost as xgb

# Following two imports are required for Xpresso. Do not remove this.
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot

# Define default file path for Xpresso to save plots
MOUNT_PATH = "/data"

__author__ = "### Claire Zhang ###"

# To use the logger please provide the name and log level
#   - name is passed as the project name while generating the logs
#   - level can be DEBUG, INFO, WARNING, ERROR, CRITICAL
logger = XprLogger(name="logistic_reg",
                   level=logging.INFO)


class XGBoostComponent(AbstractPipelineComponent):
    """ Specialized class for pipeline jobs that use SKlearn API for model training and evaluation.
     It is extended from AbstractPipelineComponent, which allows xpresso platform to track and manage the pipeline.

    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self, name=None):

        super().__init__(name)
        """ Initialize all the required constansts and data her """

    def start(self, xpresso_run_name):
        """
        The start method starts the experiment corresponding to the xpresso_run_name
        by calling the superclass start method from the abstract_pipeline_component

        Args:
            xpresso_run_name (str) : Unique identifier of that run instance
        """

        super().start(xpresso_run_name=xpresso_run_name)
        # === Your start code base goes here ===

        # except Exception:
        #     import traceback
        #     traceback.print_exc()
        #     self.completed(success=False)
        # self.completed(success=True)



    def report_metrics(self, model, X, y,
                       generate_validation_metrics=True, validation_size=0.2,
                       status_desc="reporting metrics"):
        """

         The method automatically detects user's model type (xgboost.XGBRegressor, xgboost.XGBClassifier) and
         collects, calculates and reports all metrics available in SKlearn library

        Args:
            model : trained XGBoost model using Scikit-Learn Wrapper

            X : input matrix (either training set, validation set, or test set)

            y_true : target variable vector or matrix (either training set, validation set, or test set)

            generate_validation_metrics : boolean (default=True). If True, the method create training and validation set
                                            using sklearn train_test_split, and report metrics for both sets. Metrics
                                            names ending wtih "_tr" represent metrics for training set. Metrics names
                                             ending with "_val" represent metrics for validation set.

            validation_size: float (default=0.2). The percentage is used for test_size parameter of sklearn
                                train_test_split

            status_desc : the status variable that goes into send_metrics method (default value "Reporting Metrics" given)
        """
        # assign attributes
        self.status_desc = status_desc

        # initiate two empty lists for collecting metrics values and names
        self.metrics_value = []
        self.metrics_name = []

        # datatype check : if model is a xgboost model
        if isinstance(model, xgb.Booster) or isinstance(model, xgb.sklearn.XGBModel):
            self.model = model
            # identify best_ntree_limit if model fitting with early_stopping activated
            try:
                self.best_score = self.model.best_score
                self.best_iteration = self.model.best_iteration
                self.best_ntree_limit = self.model.best_ntree_limit
            except Exception as e:
                print(e)
                self.best_ntree_limit = None

        if isinstance(self.model, xgb.Booster):
            self.report_training_best_iteration()

        # datatype check : if model is xgboost model trained Scikit-Learn API
        elif isinstance(self.model, xgb.sklearn.XGBModel):
            #plot learning curve of the trained xgboost model
            self.metrics_value.extend([self.best_score, self.best_iteration])
            self.metrics_name.extend(["best_score", "best_iteration"])
            self.plot_learning_curve(estimator=self.model)

        # if validation = True, create train and validation sets from dataset provided
        if generate_validation_metrics:
            X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=validation_size,
                                                              random_state=42)

            # generate metrics for training and validation set separately
            for x, y, dname in [(X_train, y_train, "_tr"), (X_val, y_val, "_val")]:
                # assign predictors based on model class (xgboost booster or xgboost sklearn wrapper model)
                if isinstance(model, xgb.Booster):
                    self.X = xgb.DMatrix(x, label=y)
                else:
                    self.X = x
                # assign target variable
                self.y_true = y
                # assign dataset name
                self.dataset_name = dname
                # make predictions
                self.y_pred, self.y_pred_proba = self.generate_prediction()
                # populate metrics
                self.populate_metrics()

        # if validation = False
        else:
            # assign predictors based on model class
            if isinstance(model, xgb.Booster):
                self.X = xgb.DMatrix(x, label=y)
            else:
                self.X = x
            # assign target variable
            self.y_true = y
            # assign dataset name to be None
            self.dataset_name =''
            # make predictions
            self.y_pred, self.y_pred_proba= generate_prediction()
            # populate metrics
            self.populate_metrics()

        # collect metrics names and values in a dictionary
        self.metrics_dict = dict(zip(self.metrics_name, self.metrics_value))

        # send metrics to xpresso UI
        self.send_metrics(metrics=self.metrics_dict, status_desc=self.status_desc)
        logger.info("Metrics Reported")

    def report_training_best_iteration(self):
        for k, v in self.model.attributes().items():
            if k == 'best_msg':
                for metric_pair in v.split()[1:]:
                    metric_name, metric_v = metric_pair.split(":")
                    self.metrics_name.append("best_" + metric_name)
                    self.metrics_value.append(metric_v)
            else:
                self.metrics_name.append(k)
                self.metrics_value.append(v)

    def populate_metrics(self):

        # distinguish between regressor or classifier from type_of_target
        self.is_regressor = type_of_target(self.y_true).startswith("continuous")
        self.is_classifier = type_of_target(self.y_true) in ["binary", "multiclass", "multiclass-multioutput",
                                                             "multilabel-indicator"]

        # check if the model is xgboost regressor
        if isinstance(self.model, xgb.XGBRegressor) or self.is_regressor:
            self.populate_regressor_metrics()

        # check if the model is a xgboost classifier
        elif isinstance(self.model, xgb.XGBClassifier) or self.is_classifier:
            self.populate_metrics_classifier()

        # # check if the model is a xgboost ranker
        # elif isinstance(self.model, xgb.XGBRanker):
        #     self.populate_metrics_xgbranker()

    # def populate_metrics_xgbranker(self):
    #
    #     # collect and calculate metrics that use y_true, y_pred as inputs
    #     sklearn_metrics = [metrics.adjusted_mutual_info_score,
    #                          metrics.adjusted_rand_score,
    #                          metrics.completeness_score,
    #                          metrics.fowlkes_mallows_score,
    #                          metrics.homogeneity_score,
    #                          metrics.mutual_info_score,
    #                          metrics.normalized_mutual_info_score,
    #                          metrics.v_measure_score]
    #
    #     self.calculate_metrics_general(sklearn_metrics)

        # # addtional metrics for unsupervised clustering models
        # clustering_specific_metrics = [metrics.calinski_harabasz_score,
        #                                metrics.davies_bouldin_score,
        #                                metrics.silhouette_score]
        #
        # # calculate metrics that use X and y_pred as inputs
        # for metric in clustering_specific_metrics:
        #     try:
        #         self.metrics_value.append(round(metric(self.X, self.y_pred), 4))
        #         self.metrics_name.append(metric.__name__ + self.dataset_name)
        #     except Exception as e:
        #         print(e)
        #         pass


    def calculate_metrics_general(self, sklearn_metrics):
        # calculate metrics that use y_true and y_pred as inputs
        for metric in sklearn_metrics:
            try:
                self.metrics_value.append(round(metric(self.y_true, self.y_pred), 2))
                self.metrics_name.append(metric.__name__ + self.dataset_name)
            except Exception as e:
                print(e)
                pass

    def generate_prediction(self):
        try:
            # Apply trained model to make predictions on given dataset
            y_pred = self.model.predict(self.X, ntree_limit=self.best_ntree_limit )

            # Check if the trained model can predict probability
            if hasattr(self.model, "predict_proba"):
                y_pred_proba = self.model.predict_proba(self.X)
            else:
                y_pred_proba = None

            # if objective is binary:logistic, the output of model.predict() is probabilities not the exact label
            if (type_of_target(self.y_true) in ["binary", "multiclass", "multiclass-multioutput",
                                               "multilabel-indicator"]) and \
                    (type_of_target(y_pred).startswith("continuous")):
                y_pred_proba = y_pred
                y_pred = (y_pred > 0.5).astype(int)

                # reshape y_pred_proba array
                if type_of_target(self.y_true) in ["binary"]:
                    y_pred_proba = np.c_[1 - y_pred, y_pred]

            return y_pred, y_pred_proba

        except Exception as e:
            print(e)
            pass


    def populate_metrics_classifier(self):

        # Calculate metrics that use y_true and y_pred_proba as inputs
        if self.y_pred_proba is not None:
            proba_metrics = [metrics.log_loss]
            self.populate_metrics_yproba(proba_metrics)

        # check if the model is binary classifier:
        if sklearn.utils.multiclass.unique_labels(self.y_true).size == 2:

            self.populate_metrics_binary()

            # plot roc curve for binary classifier
            self.plot_roc_curve_binary()

            # plot precision_recall_curve for binary classifier
            self.plot_precision_recall_curve_binary()

            # plot confusion matrix for binary or multiclass classification task
            self.plot_confusion_matrix()


        # check if the model is a multiclass or multilabel classifier
        elif sklearn.utils.multiclass.unique_labels(self.y_true).size > 2:

            # for multiclass classification
            if not sklearn.utils.multiclass.is_multilabel(self.y_true):
                self.populate_metrics_multiclass()

                # plot confusion matrix for binary or multiclass classification task
                self.plot_confusion_matrix()


            # for multilabel classification
            elif sklearn.utils.multiclass.is_multilabel(self.y_true):
                self.populate_metrics_multilabel()

                # plot multilabel confusion matrix for all types of classification task
                self.plot_multilabel_confusion_matrix()

            # plot roc curve for multiclass or multilabels
            self.plot_roc_curve_multi()

        # save classification report for all types
        self.save_classification_report()

    def populate_metrics_yproba(self, proba_metrics):
        for metric in proba_metrics:
            try:
                self.metrics_value.append(round(metric(self.y_true, self.y_pred_proba), 4))
                self.metrics_name.append(metric.__name__ + self.dataset_name)
            except Exception as e:
                print(e)
                pass

    def populate_metrics_binary(self):
        # metrics using y_true and y_pred as inputs
        sklearn_metrics = [metrics.accuracy_score,
                                     metrics.zero_one_loss,
                                     metrics.hamming_loss,
                                     metrics.balanced_accuracy_score,
                                     metrics.f1_score,
                                     metrics.recall_score,
                                     metrics.precision_score,
                                     metrics.average_precision_score,
                                     metrics.jaccard_score,
                                     metrics.matthews_corrcoef]

        self.calculate_metrics_general(sklearn_metrics)


        # calculate metrics that use prediction probabilities as inputs
        if self.y_pred_proba is not None:
            clf_metrics_proba = [metrics.roc_auc_score,
                                 metrics.brier_score_loss]
            # calculate metrics that use y_true and y_pred_proba as inputs
            for metric in clf_metrics_proba:
                try:
                    self.metrics_value.append(round(metric(self.y_true, self.y_pred_proba[:, 1]), 2))
                    self.metrics_name.append(metric.__name__ + self.dataset_name)
                except Exception as e:
                    print(e)
                    pass

        # add additional metrics from confusion matrix
        try:
            tn, fp, fn, tp = metrics.confusion_matrix(self.y_true, self.y_pred).ravel()
            self.metrics_value.extend([self.true_negative_rate(tn, fp),
                                       self.false_positive_rate(fp, tn),
                                       self.false_negative_rate(fn, tp)])
            self.metrics_name.extend(["true_negative_rate" + self.dataset_name,
                                      "false_positive_rate" + self.dataset_name,
                                      "false_negative_rate" + self.dataset_name])
        except Exception as e:
            print(e)
            pass



    # define functions true_negative_rate
    def true_negative_rate(self,tn, fp):
        return round(tn / (tn + fp), 2)

    # define functions false_positive_rate
    def false_positive_rate(self,fp, tn):
        return round(fp / (fp + tn), 2)

    # define functions false_negative_rate
    def false_negative_rate(self,fn, tp):
        return round(fn / (fn + tp), 2)

    def populate_metrics_multiclass(self):

        sklearn_metrics = [metrics.accuracy_score,
                                     metrics.zero_one_loss,
                                     metrics.hamming_loss,
                                     metrics.balanced_accuracy_score,
                                     metrics.matthews_corrcoef]
        self.calculate_metrics_general(sklearn_metrics)


        # averaging metrics applicable to both multiclass
        averaging_metrics = [metrics.f1_score,
                             metrics.recall_score,
                             metrics.precision_score,
                             metrics.jaccard_score]

        for metric in averaging_metrics:
            for avg_method in ["micro", "macro", "weighted"]:
                try:
                    self.metrics_value.append(round(metric(self.y_true, self.y_pred, average=avg_method), 4))
                    self.metrics_name.append(metric.__name__ + "_" + avg_method + self.dataset_name)
                except Exception as e:
                    print(e)
                    pass

        # roc_auc_score
        if self.y_pred_proba is not None:
            for avg_method in ["macro", "weighted"]:
                for config in ["ovr", "ovo"]:
                    try:
                        self.metrics_value.append(round(metrics.roc_auc_score(self.y_true, self.y_pred_proba,
                                                                              average=avg_method,
                                                                              multi_class=config), 4))
                        self.metrics_name.append(metrics.roc_auc_score.__name__ + "_" + avg_method + "_" + config
                                                 + self.dataset_name)

                    except Exception as e:
                        print(e)
                        pass

    def populate_metrics_multilabel(self):
        # collect and calculate metrics that use y_true, y_pred as inputs

        sklearn_metrics = [metrics.accuracy_score,
                                metrics.zero_one_loss,
                                metrics.hamming_loss,
                                metrics.average_precision_score]

        self.calculate_metrics_general(sklearn_metrics)

        # Calculate metrics that use y_true and y_pred_proba as inputs
        if self.y_pred_proba is not None:
            proba_metrics = [metrics.log_loss]
            self.populate_metrics_yproba(proba_metrics)

        # averaging metrics
        averaging_metrics = [metrics.f1_score,
                             metrics.recall_score,
                             metrics.precision_score,
                             metrics.jaccard_score]

        for metric in averaging_metrics:
            for avg_method in ["micro", "macro", "weighted", "samples"]:
                try:
                    self.metrics_value.append(round(metric(self.y_true, self.y_pred, average=avg_method), 4))
                    self.metrics_name.append(metric.__name__ + "_" + avg_method + self.dataset_name)
                except Exception as e:
                    print(e)
                    pass

        #roc_auc_score
        if self.y_pred_proba is not None:
            # reshape prediction probability
            y_pred_proba_t = np.transpose(np.array(self.y_pred_proba)[:, :, 1])

            # roc_auc_score for multilabel
            for avg_method in ["micro", "macro", "weighted", "samples"]:
                try:
                    self.metrics_value.append(round(metrics.roc_auc_score(self.y_true, y_pred_proba_t,
                                                                          average=avg_method), 4))
                    self.metrics_name.append(metrics.roc_auc_score.__name__ + "_" + avg_method +  self.dataset_name)

                except Exception as e:
                    print(e)
                    pass

        # Multilabel ranking metrics using y_pred and y_pred_proba
        if self.y_pred_proba is not None:
            ranking_metrics = [metrics.coverage_error,
                                metrics.label_ranking_average_precision_score,
                                metrics.label_ranking_loss,
                                metrics.ndcg_score]

            self.populate_metrics_yproba(ranking_metrics)

    def plot_precision_recall_curve_binary(self):
        if self.y_pred_proba is not None:
            try:
                average_precision = metrics.average_precision_score(self.y_true, self.y_pred_proba[:,1])
                disp_prec = metrics.plot_precision_recall_curve(self.model, self.X, self.y_true)
                disp_prec.ax_.set_title('Precision-Recall curve' + self.dataset_name +
                                        ' : AP={0:0.2f}'.format(average_precision))
                plt.show()
                xpresso_save_plot("Precision_recall_curve" + self.dataset_name, output_path=MOUNT_PATH,
                                  output_folder="report_metrics/" + self.status_desc)

            except Exception as e:
                print(e)
                pass

    def plot_roc_curve_binary(self):
        if self.y_pred_proba is not None:
            try:
                average_roc_auc_score = metrics.roc_auc_score(self.y_true, self.y_pred_proba[:, 1])
                disp_roc = metrics.plot_roc_curve(self.model, self.X, self.y_true)
                disp_roc.ax_.set_title('ROC curve' + self.dataset_name +
                                       ' : AUC ROC={0:0.2f}'.format(average_roc_auc_score))
                plt.show()
                xpresso_save_plot("ROC_curve", output_path=MOUNT_PATH,
                                  output_folder="report_metrics/" + self.status_desc)
            except Exception as e:
                print(e)
                pass

    def plot_multilabel_confusion_matrix(self):
        try:
            # generate multilabel_confusion_matrix
            cm_arrays = metrics.multilabel_confusion_matrix(self.y_true, self.y_pred)

            fig, axes = plt.subplots(len(cm_arrays), figsize=(6, 10))

            # plot a confusion matrix for each class
            for i in range(len(cm_arrays)):
                df_cm = pd.DataFrame(cm_arrays[i])
                sn.set(font_scale=1.2)  # for label size
                sn.heatmap(df_cm, annot=True,
                           cmap=plt.cm.Blues, ax=axes[i], fmt='g')
                cm_plot_title = metrics.multilabel_confusion_matrix.__name__ + "_class_" + str(
                    model_classes[i] + self.dataset_name)
                axes[i].set_xlabel('Predicted label')
                axes[i].set_ylabel('True label')
                axes[i].set_title(cm_plot_title)

            fig.tight_layout(pad=1.0)
            plt.show()
            xpresso_save_plot(cm_plot_title, output_path=MOUNT_PATH,
                              output_folder="report_metrics/" + self.status_desc)
            logger.info("Multilable Confusion Matrix Saved")
        except Exception as e:
            print(e)
            pass

    def plot_confusion_matrix(self):
        # plot confusion matrix for binary or multiclass classification task
        try:
            cm_disp = metrics.plot_confusion_matrix(self.model, self.X, self.y_true,
                                                    display_labels=self.model.classes_,
                                                    cmap=plt.cm.Blues)
            cm_disp.ax_.set_title("Confusion Matrix" + self.dataset_name)
            plt.show()
            xpresso_save_plot("Confusion_matrix" + self.dataset_name, output_path=MOUNT_PATH,
                              output_folder="report_metrics/" + self.status_desc)
            logger.info("Confusion Matrix Saved")
        except Exception as e:
            print(e)
            pass

    def plot_roc_curve_multi(self):
        # Compute ROC curve and ROC area for multiclass and multilabel classifiers
        if self.y_pred_proba is not None:
            try:
                model_classes = self.model.classes_
            except:
                model_classes = np.unique(self.y_pred)
            try:
                y_true_binarized = label_binarize(self.y_true, classes=model_classes)
                fpr = dict()
                tpr = dict()
                roc_auc = dict()
                for i in range(len(model_classes)):
                    fpr[i], tpr[i], _ = metrics.roc_curve(y_true_binarized[:, i], self.y_pred_proba[:, i])
                    roc_auc[i] = metrics.auc(fpr[i], tpr[i])

                # Compute micro-average ROC curve and ROC area
                fpr["micro"], tpr["micro"], _ = metrics.roc_curve(y_true_binarized.ravel(), self.y_pred_proba.ravel())
                roc_auc["micro"] = metrics.auc(fpr["micro"], tpr["micro"])

                # First aggregate all false positive rates
                all_fpr = np.unique(np.concatenate([fpr[i] for i in range(len(model_classes))]))

                # Then interpolate all ROC curves at this points
                mean_tpr = np.zeros_like(all_fpr)
                for i in range(len(model_classes)):
                    mean_tpr += np.interp(all_fpr, fpr[i], tpr[i])

                # Finally average it and compute AUC
                mean_tpr /= len(model_classes)

                fpr["macro"] = all_fpr
                tpr["macro"] = mean_tpr
                roc_auc["macro"] = metrics.auc(fpr["macro"], tpr["macro"])

                # Plot all ROC curves
                plt.figure(figsize=(12, 6))
                lw = 2
                plt.plot(fpr["micro"], tpr["micro"],
                         label='micro-average ROC curve (area = {0:0.2f})'
                               ''.format(roc_auc["micro"]),
                         color='deeppink', linestyle=':', linewidth=4)

                plt.plot(fpr["macro"], tpr["macro"],
                         label='macro-average ROC curve (area = {0:0.2f})'
                               ''.format(roc_auc["macro"]),
                         color='navy', linestyle=':', linewidth=4)

                # create different colors for each class
                colors = cycle([i for i in mcolors.TABLEAU_COLORS.values()])
                for i, color in zip(range(len(model_classes)), colors):
                    plt.plot(fpr[i], tpr[i], color=color, lw=lw,
                             label='ROC curve of class {0} (area = {1:0.2f})'
                                   ''.format(i, roc_auc[i]))

                plt.plot([0, 1], [0, 1], 'k--', lw=lw)
                plt.xlim([0.0, 1.0])
                plt.ylim([0.0, 1.05])
                plt.xlabel('False Positive Rate')
                plt.ylabel('True Positive Rate')
                roc_plot_title = self.model.__class__.__name__ + '_ROC_curve' + self.dataset_name
                plt.title(roc_plot_title)
                plt.legend(loc="lower right")
                plt.show()
                xpresso_save_plot(roc_plot_title, output_path=MOUNT_PATH,
                                  output_folder="report_metrics/" + self.status_desc)
                logger.info("Multiclass/Multilable ROC Curve Saved")
            except Exception as e:
                print(e)
                pass

    def save_classification_report(self):
        # save classification report for classification task
        try:
            cls_report = metrics.classification_report(self.y_true, self.y_pred,
                                                       output_dict=True)
            cls_report_df = pd.DataFrame(cls_report).transpose()
            # save model to data folder
            pickle.dump(cls_report_df, open(os.path.join(MOUNT_PATH, "report_metrics/classification_report"
                                                         + self.dataset_name + ".pkl"), 'wb'))

            logger.info("Classification Report Saved")
        except Exception as e:
            print(e)
            pass

    def populate_regressor_metrics(self):
        # collect sklearn regression metrics
        sklearn_metrics = [metrics.explained_variance_score,  # Explained variance regression score function
                           metrics.max_error,  # max_error metric calculates the maximum residual error
                           metrics.mean_absolute_error,  # Mean absolute error regression loss
                           metrics.mean_squared_error,  # Mean squared error regression loss
                           metrics.mean_squared_log_error,  # Mean squared logarithmic error regression loss
                           metrics.median_absolute_error,  # Median absolute error regression loss
                           metrics.r2_score,  # R^2 (coefficient of determination) regression score function
                           metrics.mean_poisson_deviance,  # Mean Poisson deviance regression loss.
                           metrics.mean_gamma_deviance,  # Mean Gamma deviance regression loss.
                           metrics.mean_tweedie_deviance] # Mean Tweedie deviance regression loss

        self.calculate_metrics_general(sklearn_metrics)

    # use default metrics if no values provided to scoring_metrics by end-users
    def plot_learning_curve_cv(params, dtrain, num_boost_round=10,
                               nfold=3, metrics=None, early_stopping_rounds=5,
                               **kwargs):
        """
        Generate cross-validated learning plots: the test, train scores vs num_boost_round

        Parameters
        ----------
        params : parameters for xgboost models

        dtrain :  feature matrix and label

        num_boost_round : number

        nfold : cv folds

        metrics: str or list of str, scoring parameters pre-defined in XGboost, e.g.,"error"
                see documentation of "eval_metric" parameter within "XGBoost Parameters : Learning Task Parameters"
                default value for  regressor is "rmse", "mae"
                default value for  binary classifier is 'auc', 'logloss'
                default value for multiclass classifier is 'merror', 'mlogloss'

        early_stopping_rounds: number of rounds when validation metrics stop improving before early stopping activated

        **kwargs could be any parameters that goes into xgboost.cv
        """

        # provide default metrics if no metrics provided
        if metrics is None:
            # check if objective is provided in params
            try:
                params["objective"]
            # if not, default objective is given
            except:
                params["objective"] = "reg:squarederror"

            # provide default metrics
            if "reg" in params["objective"]:
                metrics = ["rmse", "mae"]
            elif "binary" in params["objective"]:
                metrics = ['auc', 'logloss']
            elif "multi" in params["objective"]:
                metrics = ['mlogloss', "merror"]

        # create xgboost cross validation
        cv_results = xgb.cv(params=params, dtrain=dtrain,
                            num_boost_round=num_boost_round, nfold=nfold,
                            metrics=metrics,
                            early_stopping_rounds=early_stopping_rounds, **kwargs)

        # save cv_results to data folder
        pickle.dump(cv_results, open(os.path.join(MOUNT_PATH, "report_metrics/cv_results_df.pkl"), 'wb'))

        fig, axes = plt.subplots(len(metrics), 1, figsize=(8,14))

        # plot learning curve for each scoring metrics
        for i in range(len(metrics)):
            try:
                train_scores_mean = cv_results["train-{}-mean".format(metrics[i])]
                train_scores_std = cv_results["train-{}-std".format(metrics[i])]
                val_scores_mean = cv_results["test-{}-mean".format(metrics[i])]
                val_scores_std = cv_results["test-{}-std".format(metrics[i])]
                num_boost_range = list(range(1, len(train_scores_mean) + 1))

                if ("loss" in metrics[i]) or ("error" in metrics[i]):
                        val_score_best = val_scores_mean.min()
                        max_iters_best = np.argmin(val_scores_mean) + 1
                else:
                    val_score_best = val_scores_mean.max()
                    max_iters_best = np.argmax(val_scores_mean) + 1

                # Plot score vs max_iters
                axes[i].grid()
                axes[i].fill_between(num_boost_range,
                                     train_scores_mean - train_scores_std,
                                     train_scores_mean + train_scores_std,
                                     alpha=0.1, color="y")
                axes[i].fill_between(num_boost_range,
                                     val_scores_mean - val_scores_std,
                                     val_scores_mean + val_scores_std,
                                     alpha=0.1, color="g")
                axes[i].plot(num_boost_range,
                             train_scores_mean,
                             'o-', color="y",
                             label="Training score")
                axes[i].plot(num_boost_range,
                             val_scores_mean,
                             'o-', color="g",
                             label="Cross-validation score")
                axes[i].plot(max_iters_best, val_score_best, 'r*', label='Best num_iteration : ' +
                                                                         str(max_iters_best))
                axes[i].set_xlabel("num_iteration")
                axes[i].set_ylabel(metrics[i])
                axes[i].set_title("Cross Validation Curve : " + metrics[i] + " vs " + "num_iteration")
                axes[i].legend(loc="best")

            except Exception as e:
                print(e)
                pass

        fig.tight_layout(pad=3.0)
        plt.show()
        xpresso_save_plot("cross_validation_curve_xgboost", output_path=MOUNT_PATH,
                          output_folder="report_metrics/")
        logger.info("Learning curve plot saved")

    def plot_learning_curve(self, estimator=None, evals_result=None):
        """
        users can choose to pass in either a trained XGBoost model using Scikit-Learn API
        or a dictionary containing evaluation results returned by XBGoost Learning API xgboost.train
        when parameter evals_result is specified
        """

        # get evaluation results
        if (estimator is not None) and (isinstance(estimator, xgb.sklearn.XGBModel)):
            evals_result_dict = estimator.evals_result()
        if (evals_result is not None) and (isinstance(evals_result, dict)):
            evals_result_dict = evals_result

        # identify unique evaluation metrics used when fitting the xgboost model
        metrics_list = []
        for val_name, e_mtrs in evals_result_dict.items():
            for e_mtr_name, e_mtr_vals in e_mtrs.items():
                metrics_list.append(e_mtr_name)
        metrics = np.unique(metrics_list)

        # create subplots
        fig, axes = plt.subplots(len(metrics), 1, figsize=(8, 12))

        # plot learning curve for each scoring metrics
        for i in range(len(metrics)):
            try:
                results = pd.DataFrame()
                for dataset_name, e_mtrs in evals_result_dict.items():
                    for e_mtr_name, e_mtr_vals in e_mtrs.items():
                        if e_mtr_name == metrics[i]:
                            results[dataset_name + "_" + metrics[i]] = e_mtr_vals

                # collect number of boost run
                num_boost_range = list(range(1, results.shape[0] + 1))

                # identify best xgboost run number and the score
                if ("loss" in metrics[i]) or ("error" in metrics[i]):
                    val_score_best = results.iloc[:, 1].min()
                    max_iters_best = np.argmin(results.iloc[:, 1]) + 1
                else:
                    val_score_best = results.iloc[:, 1].max()
                    max_iters_best = np.argmax(results.iloc[:, 1]) + 1

                # Plot score vs max_iters
                axes[i].grid()

                axes[i].plot(num_boost_range,
                             results.iloc[:, 0],
                             'o-', color="y",
                             label=results.columns[0])
                axes[i].plot(num_boost_range,
                             results.iloc[:, 1],
                             'o-', color="g",
                             label=results.columns[1])
                axes[i].plot(max_iters_best, val_score_best, 'r*', label='Best num_iteration : ' +
                                                                         str(max_iters_best))
                axes[i].set_xlabel("num_iteration")
                axes[i].set_ylabel(metrics[i])
                axes[i].set_title("XGboost Learning Curve :" + metrics[i])
                axes[i].legend(loc="best")

            except Exception as e:
                print(e)
                pass

        fig.tight_layout(pad=3.0)
        plt.show()

    def send_metrics(self, metrics, status_desc ="reporting metrics"):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": status_desc},
                "metric": metrics
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()


    def completed(self, push_exp=True, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===

        try:
            # create Output folder
            if not os.path.exists(self.OUTPUT_DIR):
                os.makedirs(self.OUTPUT_DIR)

            # save xgboost Booster
            if isinstance(self.model, xgb.Booster):
                # save model
                self.model.save_model(os.path.join(self.OUTPUT_DIR, self.model.__class__.__name__ +'.model'))

            # save xgboost sklearn model
            elif isinstance(self.model, xgb.sklearn.XGBModel):
                # save model to output folder
                pickle.dump(self.model, open(os.path.join(self.OUTPUT_DIR, self.model.__class__.__name__ + ".pkl"), 'wb'))

        except Exception as e:
            print(e)
            pass
        
        
        try:
            super().completed(push_exp=push_exp, success=success)

        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)


    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)


    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)


    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    # if __name__ == "__main__":
    #     # To run locally. Use following command:
    #     # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py
    #
    #     data_prep = SKLearnComponent()
    #     if len(sys.argv) >= 2:
    #         data_prep.start(run_name=sys.argv[1])
    #     else:
    #         data_prep.start(run_name="")
