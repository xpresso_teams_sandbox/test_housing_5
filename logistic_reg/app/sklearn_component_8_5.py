"""
This is the implementation of data preparation for sklearn
"""
# import system libraries
import sys
import logging
import os

# import common python libraries
import numpy as np
import pandas as pd
import itertools
from itertools import cycle
import pickle

# import sklearn libraries
import sklearn
from sklearn import metrics
from sklearn.model_selection import GridSearchCV, validation_curve
from sklearn.preprocessing import label_binarize
from sklearn.utils._testing import ignore_warnings
from sklearn.exceptions import ConvergenceWarning
from sklearn.model_selection import train_test_split

# libraries for plotting
import matplotlib.pyplot as plt
import seaborn as sn
import matplotlib.colors as mcolors

# Following two imports are required for Xpresso. Do not remove this.
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot

# Define default file path for Xpresso to save plots
MOUNT_PATH = "/data"
PICKLE_PATH = "/data"
#MOUNT_PATH ="/Users/yuanhongzhang/test_housing_5/data"
#PICKLE_PATH ="/Users/yuanhongzhang/test_housing_5/data"

__author__ = "### Claire Zhang ###"

# To use the logger please provide the name and log level
#   - name is passed as the project name while generating the logs
#   - level can be DEBUG, INFO, WARNING, ERROR, CRITICAL
logger = XprLogger(name="logistic_reg",
                   level=logging.INFO)


class SKLearnComponent(AbstractPipelineComponent):
    """ Specialized class for pipeline jobs that use SKlearn API for model training and evaluation.
     It is extended from AbstractPipelineComponent, which allows xpresso platform to track and manage the pipeline.

    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self, name=None):

        super().__init__(name)
        """ Initialize all the required constansts and data her """

    def start(self, xpresso_run_name):
        """
        The start method starts the experiment corresponding to the xpresso_run_name
        by calling the superclass start method from the abstract_pipeline_component

        Args:
            xpresso_run_name (str) : Unique identifier of that run instance
        """

        super().start(xpresso_run_name=xpresso_run_name)
        # === Your start code base goes here ===

        # except Exception:
        #     import traceback
        #     traceback.print_exc()
        #     self.completed(success=False)
        # self.completed(success=True)



    def report_metrics(self, model, X, y,
                       pred_thld = 0.5,
                       generate_validation_metrics=True,
                       validation_size=0.2,
                       status_desc="reporting metrics"):
        """
        The report_metrics method collects, calculate and report all metrics available in SKlearn library
         The method automatically detects user's model type (regressor, classifiers-binary, classifiers-multiclass,
         classifiers-multilabel, unsupervised-clustering)

        Args:
            model : SKlearn model object
                    Trained model using SKlearn library (see https://scikit-learn.org/stable/modules/classes.html)
            
            X : array_like or sparse matrix, shape (n_samples, n_features)
                input matrix (either training set, validation set, or test set)

            y : array-like of shape (n_samples,) or (n_samples, n_classes)
                target variable vector or matrix (either training set, validation set, or test set)

            pred_thld : float (default = 0.5)
                        Prediction threshold that is compared to prediction probability output by tensorflow
                        model.predict() when determining the prediction label for classification model.

            generate_validation_metrics : boolean (default=True)
                                          If True, the method create training and validation set
                                          using sklearn train_test_split, and report metrics for both sets. Metrics
                                          names ending wtih "_tr" represent metrics for training set. Metrics names
                                          ending with "_val" represent metrics for validation set.

            validation_size: float (default=0.2).
                            The percentage is used for the test_size parameter of sklearn train_test_split if
                            generate_validation_metrics is True

            status_desc : string (default = "Reporting Metrics")
                        user can provide any description, such as name of the dataset that is being sent to report metrics method
        """

        if not isinstance(model, sklearn.base.BaseEstimator):
            return

        # assign attributes
        self.status_desc = status_desc
        self.pred_thld = pred_thld
        self.model = model

        # initiate three empty lists for collecting metrics values and names
        self.metrics_value = []
        self.metrics_name = []

        # if validation = True, create train and validation sets from dataset provided by user
        if generate_validation_metrics:
            try:
                X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=validation_size,
                                                                  random_state=42)

                # report metrics for training and validation set separately
                for x, y, dname in [(X_train, y_train, " (Tr)"), (X_val, y_val, " (Val)")]:
                    # make prediction and report metrics
                    self.predict_and_evaluate(x, y, dname)

            except Exception as e:
                print(e)
                generate_validation_metrics = False

        # if validation = False
        if not generate_validation_metrics:
            dname = ""
            self.predict_and_evaluate(X, y, dname)

        # collect metrics names and values in a dictionary
        metrics_dict = dict(zip(self.metrics_name, self.metrics_value))

        # send metrics to xpresso UI
        self.send_metrics(metrics=metrics_dict, status_desc=status_desc)
        logger.info("Metrics Reported")

    def predict_and_evaluate(self, x, y, dname):
        """

        Args:
            x: array_like or sparse matrix, shape (n_samples, n_features)
                input matrix (either training set, validation set, or test set)
            y: array-like of shape (n_samples,) or (n_samples, n_classes)
                target variable vector or matrix (either training set, validation set, or test set)
            dname: string
                 A label indicating the dataset name, automaticall assigned by SKlearnComponent.
                The value will be "tr" and "val", indicating training set, validation set, if generate_validation_metrics
                 is True. Otherwise, the value will be "" if generate_validation_metrics is False or the data type is
                 not able to be split into training and validation set with sklearn train_test_split.

        Returns: metrics values(list) and metrics names(list)

        """
        # assign predictors and target variable sets and dataset name
        self.X = x
        self.y_true = y
        self.dataset_name = dname

        # make predictions
        self.generate_prediction()
        # populate metrics
        self.populate_metrics()

    def generate_prediction(self):
        """
        Generate prediction using model.predict()

        Returns: The output from running sklearn model.predict() (labeled as y_pred).
                For sklearen classification models that have "decision_function" or "predict_proba" methods,
                generate predicted probabilities or predicted decisions

        """
        # Apply trained model to make predictions on given dataset
        self.y_pred = self.model.predict(self.X)

        # check if the trained model has decision_function
        if hasattr(self.model, "decision_function"):
            self.y_score = self.model.decision_function(self.X)
        else:
            self.y_score = None

        # Check if the trained model can predict probability
        if hasattr(self.model, "predict_proba"):
            self.y_pred_proba = self.model.predict_proba(self.X)
        else:
            self.y_pred_proba = None

    def populate_metrics(self):
        """

        Returns: populate metrics based on model type (regressor, classifier, or unsupervised clustering)

        """
        # create a folder to save images or csv files generated from calling report metrics method
        report_metrics_path = MOUNT_PATH + "/report_metrics"
        if not os.path.exists(report_metrics_path):
            os.makedirs(report_metrics_path)

        # check if the model is sklearn regressor
        if sklearn.base.is_regressor(self.model):
            self.populate_metrics_regressor()

        # check if the model is a sklearn classifier
        elif sklearn.base.is_classifier(self.model):
            self.populate_metrics_classifier()

        # check if the model is a unsupervised clustering algorithm within sklearn.cluster module
        elif self.model.__class__.__name__ in sklearn.cluster.__all__:
            self.populate_metrics_unsupervised_clustering()


    def calculate_metrics_general(self, sklearn_metrics):
        """
        Calculate and record metrics that take y_true and y_pred as inputs
        Args:
            sklearn_metrics: sklearn metrics APIs, automatically assigned by SKlearnComponent based on model type.

        Returns:
            metrics values and metrics name

        """
        for metric in sklearn_metrics:
            try:
                self.metrics_value.append(round(metric(self.y_true, self.y_pred), 2))
                self.metrics_name.append(self.get_name(metric) + self.dataset_name)
            except Exception as e:
                print(e)
                pass

    def populate_metrics_regressor(self):
        """
        Populate metrics for regression models using sklearn metrics APIs

        Returns: metrics values and metrics name

        """
        # collect sklearn regression metrics
        sklearn_metrics = [metrics.mean_absolute_error,
                           metrics.mean_squared_error,
                           metrics.mean_squared_log_error,
                           metrics.median_absolute_error,
                           metrics.explained_variance_score,
                           metrics.max_error,
                           metrics.r2_score,
                           metrics.mean_poisson_deviance,
                           metrics.mean_gamma_deviance,
                           metrics.mean_tweedie_deviance]

        self.calculate_metrics_general(sklearn_metrics)


    def populate_metrics_classifier(self):
        """
        Populate metrics, save classification report (as csv file) and plot confusion matrix, roc curve and
        precision_recall curve for all three type of classifiers ((binary, multiclass or multilable).
        SKlearnComponent leverage metrics available from sklearn library. The metrics that will be
        finally reported depends on the classifier type (binary, multiclass or multilable)

        Returns: metrics values and metrics name

        """

        # save classification report for all types
        self.save_classification_report()

        # Calculate metrics that use y_true and y_score as inputs
        if self.y_score is not None:
            score_metrics = [metrics.hinge_loss]
            self.populate_metrics_yscore(score_metrics)

        # Calculate metrics that use y_true and y_pred_proba as inputs
        if self.y_pred_proba is not None:
            proba_metrics = [metrics.log_loss]
            self.populate_metrics_yproba(proba_metrics)

        # check if the model is binary classifier:
        if sklearn.utils.multiclass.unique_labels(self.y_true).size == 2:

            self.populate_metrics_binary()

            # plot roc curve for binary classifier
            self.plot_roc_curve_binary()

            # plot precision_recall_curve for binary classifier
            self.plot_precision_recall_curve_binary()

            # plot confusion matrix for binary or multiclass classification task
            self.plot_confusion_matrix()


        # check if the model is a multiclass or multilabel classifier
        elif sklearn.utils.multiclass.unique_labels(self.y_true).size > 2:

            # plot roc curve for multiclass or multilabels
            self.plot_roc_curve_multi()

            # plot precision recall curve for multiclass or multilabels
            self.plot_precision_recall_curve_multi()

            # for multiclass classification
            if not sklearn.utils.multiclass.is_multilabel(self.y_true):
                self.populate_metrics_multiclass()

                # plot confusion matrix for binary or multiclass classification task
                self.plot_confusion_matrix()

            # for multilabel classification
            elif sklearn.utils.multiclass.is_multilabel(self.y_true):
                self.populate_metrics_multilabel()

                # plot multilabel confusion matrix for all types of classification task
                self.plot_multilabel_confusion_matrix()


    def populate_metrics_yproba(self, proba_metrics):
        """
        Populate metrics for classification models that use y_true and y_pred_proba (prediction probability) as inputs

        Args:
            proba_metrics: list of sklearn metrics APIs

        Returns: metrics values and metrics name

        """

        for metric in proba_metrics:
            try:
                self.metrics_value.append(round(metric(self.y_true, self.y_pred_proba), 4))
                self.metrics_name.append(self.get_name(metric) + self.dataset_name)
            except Exception as e:
                print(e)
                pass

    def populate_metrics_yscore(self, score_metrics):
        """
        Populate metrics for classification models that use y_true and y_score (predicted desicion) as inputs

        Args:
            proba_metrics: list of sklearn metrics APIs

        Returns: metrics values and metrics name

        """
        for metric in score_metrics:
            try:
                self.metrics_value.append(round(metric(self.y_true, self.y_score), 4))
                self.metrics_name.append(self.get_name(metric) + self.dataset_name)
            except Exception as e:
                print(e)
                pass

    def populate_metrics_binary(self):
        """
        Populate metrics for binary classifiers using both sklearn library.

        Returns: metrics values and metrics name

        """

        # metrics using y_true and y_pred as inputs
        sklearn_metrics = [sklearn.metrics.accuracy_score,
                           sklearn.metrics.balanced_accuracy_score,
                           sklearn.metrics.recall_score,
                           sklearn.metrics.precision_score,
                           sklearn.metrics.f1_score,
                           sklearn.metrics.zero_one_loss,
                           sklearn.metrics.hamming_loss,
                           sklearn.metrics.jaccard_score,
                           sklearn.metrics.matthews_corrcoef]

        self.calculate_metrics_general(sklearn_metrics)

        # calculate metrics that use prediction probabilities as inputs
        if self.y_pred_proba is not None:
            try:
                fpr, tpr, thresholds = sklearn.metrics.roc_curve(self.y_true, self.y_pred_proba[:, 1])
                self.metrics_value.append(round(sklearn.metrics.auc(fpr, tpr), 2))
                self.metrics_name.append(self.get_name(sklearn.metrics.auc) + self.dataset_name)
            except Exception as e:
                print(e)
                raise

            clf_metrics_proba = [sklearn.metrics.roc_auc_score,
                                 sklearn.metrics.average_precision_score,
                                 sklearn.metrics.brier_score_loss]
            # calculate metrics that use y_true and y_pred_proba as inputs
            for metric in clf_metrics_proba:
                try:
                    self.metrics_value.append(round(metric(self.y_true, self.y_pred_proba[:, 1]), 2))
                    self.metrics_name.append(self.get_name(metric) + self.dataset_name)
                except Exception as e:
                    print(e)
                    pass

        # add additional metrics from confusion matrix
        try:
            tn, fp, fn, tp = metrics.confusion_matrix(self.y_true, self.y_pred).ravel()
            self.metrics_value.extend([self.true_negative_rate(tn, fp),
                                       self.false_positive_rate(fp, tn),
                                       self.false_negative_rate(fn, tp)])
            self.metrics_name.extend(["true_negative_rate" + self.dataset_name,
                                      "false_positive_rate" + self.dataset_name,
                                      "false_negative_rate" + self.dataset_name])
        except Exception as e:
            print(e)
            pass

    def true_negative_rate(self, tn, fp):
        """
        Args:
            tn: True Negatives (count)
            fp: False Positives (count)

        Returns: true_negative_rate
        """
        return round(tn / (tn + fp), 2)


    def false_positive_rate(self, fp, tn):
        """
        Args:
            fp: False Positives (count)
            tn: True Negatives (count)

        Returns: false_positive_rate
        """
        return round(fp / (fp + tn), 2)


    def false_negative_rate(self, fn, tp):
        """
        Args:
            fn: False Negatives (count)
            tp: True Positives (count)

        Returns: false_negative_rate
        """
        return round(fn / (fn + tp), 2)


    def populate_metrics_multiclass(self):
        """

        Populate metrics for multiclass classifiers using both sklearn library.

        Returns: metrics values and metrics name

        """

        sklearn_metrics = [sklearn.metrics.accuracy_score,
                           sklearn.metrics.balanced_accuracy_score,
                           sklearn.metrics.zero_one_loss,
                           sklearn.metrics.hamming_loss,
                           sklearn.metrics.matthews_corrcoef]

        self.calculate_metrics_general(sklearn_metrics)


        # sklearn metrics needs averaging method
        averaging_metrics = [metrics.f1_score,
                             metrics.recall_score,
                             metrics.precision_score,
                             metrics.jaccard_score]

        for metric in averaging_metrics:
            for avg_method in ["micro", "macro", "weighted"]:
                try:
                    self.metrics_value.append(round(metric(self.y_true, self.y_pred, average=avg_method), 4))
                    self.metrics_name.append(self.get_name(metric) + "_" + avg_method + self.dataset_name)
                except Exception as e:
                    print(e)
                    pass

        # roc_auc_score
        if self.y_pred_proba is not None:
            for avg_method, config in itertools.product(["macro", "weighted"], ["ovr", "ovo"]):
                try:
                    self.metrics_value.append(round(metrics.roc_auc_score(self.y_true, self.y_pred_proba,
                                                                     average=avg_method,
                                                                     multi_class=config), 4))
                    self.metrics_name.append(self.get_name(metrics.roc_auc_score)
                                             + "_" + avg_method + "_" + config
                                             + self.dataset_name)

                except Exception as e:
                    print(e)
                    pass

    def populate_metrics_multilabel(self):
        """

        Populate metrics for multilabel classifiers with sklearn library

        Returns: metrics values and metrics name

        """

        # collect and calculate metrics that use y_true, y_pred as inputs
        sklearn_metrics = [metrics.accuracy_score,
                            metrics.zero_one_loss,
                            metrics.hamming_loss]

        self.calculate_metrics_general(sklearn_metrics)


        # metrics that needs averaging method
        averaging_metrics = [metrics.f1_score,
                             metrics.recall_score,
                             metrics.precision_score,
                             metrics.jaccard_score,
                             metrics.average_precision_score]
        for metric in averaging_metrics:
            for avg_method in ["micro", "macro", "weighted", "samples"]:
                try:
                    if metric == metrics.average_precision_score:
                        self.metrics_value.append(round(metric(self.y_true, self.y_pred_proba, average=avg_method), 4))
                    else:
                        self.metrics_value.append(round(metric(self.y_true, self.y_pred, average=avg_method), 4))
                    self.metrics_name.append(self.get_name(metric) + "_" + avg_method + self.dataset_name)
                except Exception as e:
                    print(e)
                    pass

        #roc_auc_score
        if self.y_pred_proba is not None:
            # reshape prediction probability
            y_pred_proba_t = np.transpose(np.array(self.y_pred_proba)[:, :, 1])

            # roc_auc_score for multilabel
            for avg_method in ["micro", "macro", "weighted", "samples"]:
                try:
                    self.metrics_value.append(round(metrics.roc_auc_score(self.y_true, y_pred_proba_t,
                                                                          average=avg_method), 4))
                    self.metrics_name.append(self.get_name(metrics.roc_auc_score) + "_" + avg_method + self.dataset_name)

                except Exception as e:
                    print(e)
                    pass

        # populate ranking metrics
        ranking_metrics = [sklearn.metrics.coverage_error,
                           sklearn.metrics.label_ranking_average_precision_score,
                           sklearn.metrics.label_ranking_loss,
                           sklearn.metrics.ndcg_score]

        self.populate_metrics_yscore(ranking_metrics)


    def plot_precision_recall_curve_binary(self):
        """
        Plot precision recall curve for binary classifiers

        """
        if self.y_score is not None:
            try:
                average_precision = metrics.average_precision_score(self.y_true, self.y_score)
                disp_prec = metrics.plot_precision_recall_curve(self.model, self.X, self.y_true)
                disp_prec.ax_.set_title('Precision-Recall curve' + self.dataset_name +
                                        ' : AP={0:0.2f}'.format(average_precision))
                plt.show()
                xpresso_save_plot("Precision_recall_curve", output_path=MOUNT_PATH,
                                  output_folder="report_metrics/" + self.status_desc)

            except Exception as e:
                print(e)
                pass

    def plot_precision_recall_curve_multi(self):
        """
        Plot precision recall curve for multiclass and multilabel classifiers

        """
        try:
            precision = dict()
            recall = dict()
            average_precision = dict()

            y_true_binarized = sklearn.preprocessing.label_binarize(self.y_true, classes=self.model_classes)

            for i in range(len(self.model_classes)):
                precision[i], recall[i], _ = sklearn.metrics.precision_recall_curve(y_true_binarized[:, i],
                                                                                    self.y_pred_proba[:, i])
                average_precision[i] = sklearn.metrics.average_precision_score(y_true_binarized[:, i],
                                                                               self.y_pred_proba[:, i])

            # A "micro-average": quantifying score on all classes jointly
            precision["micro"], recall["micro"], _ = sklearn.metrics.precision_recall_curve(y_true_binarized.ravel(),
                                                                                            self.y_pred_proba.ravel())
            average_precision["micro"] = sklearn.metrics.average_precision_score(y_true_binarized, self.y_pred_proba,
                                                                                 average="micro")
            # Plot all precision_recall curves
            plt.figure(figsize=(12, 6))

            colors = cycle([i for i in mcolors.TABLEAU_COLORS.values()])
            lines = []
            labels = []
            l, = plt.plot(recall["micro"], precision["micro"], color='gold', lw=2)
            lines.append(l)
            labels.append('micro-average Precision-recall (area = {0:0.2f})'
                          ''.format(average_precision["micro"]))
            for i, color in zip(range(len(self.model_classes)), colors):
                l, = plt.plot(recall[i], precision[i], color=color, lw=2)
                lines.append(l)
                labels.append('Precision-recall for class {0} (area = {1:0.2f})'
                              ''.format(i, average_precision[i]))
            fig = plt.gcf()
            fig.subplots_adjust(bottom=0.25)
            plt.xlim([0.0, 1.0])
            plt.ylim([0.0, 1.05])
            plt.xlabel('Recall')
            plt.ylabel('Precision')
            plt.title(
                'Precision-Recall curve' + self.dataset_name + ': micro AP={0:0.2f}'.format(average_precision["micro"]))
            plt.legend(lines, labels, loc="lower left", prop=dict(size=9))
            plt.show()
            xpresso_save_plot("Precision_recall_curve" + self.dataset_name, output_path=MOUNT_PATH,
                              output_folder="report_metrics/" + self.status_desc)

        except Exception as e:
            print(e)
            raise

    def plot_roc_curve_binary(self):
        """

        Plot roc curve and compute roc area for binary classifiers

        """
        if self.y_pred_proba is not None:
            try:
                average_roc_auc_score = metrics.roc_auc_score(self.y_true, self.y_pred_proba[:, 1])
                disp_roc = metrics.plot_roc_curve(self.model, self.X, self.y_true)
                disp_roc.ax_.set_title('ROC curve' + self.dataset_name +
                                       ' : AUC ROC={0:0.2f}'.format(average_roc_auc_score))
                plt.show()
                xpresso_save_plot("ROC_curve" + self.dataset_name, output_path=MOUNT_PATH,
                                  output_folder="report_metrics/" + self.status_desc)
            except Exception as e:
                print(e)
                pass


    def plot_roc_curve_multi(self):
        """

        Plot ROC curve and compute ROC area for multiclass and multilabel classifiers

        """

        if self.y_score is not None:
            try:
                y_true_binarized = label_binarize(self.y_true, classes=self.model.classes_)
                fpr = dict()
                tpr = dict()
                roc_auc = dict()
                for i in range(len(self.model.classes_)):
                    fpr[i], tpr[i], _ = metrics.roc_curve(y_true_binarized[:, i], self.y_score[:, i])
                    roc_auc[i] = metrics.auc(fpr[i], tpr[i])

                # Compute micro-average ROC curve and ROC area
                fpr["micro"], tpr["micro"], _ = metrics.roc_curve(y_true_binarized.ravel(), self.y_score.ravel())
                roc_auc["micro"] = metrics.auc(fpr["micro"], tpr["micro"])

                # First aggregate all false positive rates
                all_fpr = np.unique(np.concatenate([fpr[i] for i in range(len(self.model.classes_))]))

                # Then interpolate all ROC curves at this points
                mean_tpr = np.zeros_like(all_fpr)
                for i in range(len(self.model.classes_)):
                    mean_tpr += np.interp(all_fpr, fpr[i], tpr[i])

                # Finally average it and compute AUC
                mean_tpr /= len(self.model.classes_)

                fpr["macro"] = all_fpr
                tpr["macro"] = mean_tpr
                roc_auc["macro"] = metrics.auc(fpr["macro"], tpr["macro"])

                # Plot all ROC curves
                plt.figure(figsize=(12, 6))
                lw = 2
                plt.plot(fpr["micro"], tpr["micro"],
                         label='micro-average ROC curve (area = {0:0.2f})'
                               ''.format(roc_auc["micro"]),
                         color='deeppink', linestyle=':', linewidth=4)

                plt.plot(fpr["macro"], tpr["macro"],
                         label='macro-average ROC curve (area = {0:0.2f})'
                               ''.format(roc_auc["macro"]),
                         color='navy', linestyle=':', linewidth=4)

                colors = cycle([i for i in mcolors.TABLEAU_COLORS.values()])
                for i, color in zip(range(len(self.model.classes_)), colors):
                    plt.plot(fpr[i], tpr[i], color=color, lw=lw,
                             label='ROC curve of class {0} (area = {1:0.2f})'
                                   ''.format(i, roc_auc[i]))

                plt.plot([0, 1], [0, 1], 'k--', lw=lw)
                plt.xlim([0.0, 1.0])
                plt.ylim([0.0, 1.05])
                plt.xlabel('False Positive Rate')
                plt.ylabel('True Positive Rate')
                roc_plot_title = self.model.__class__.__name__ + '_ROC_curve' + self.dataset_name
                plt.title(roc_plot_title)
                plt.legend(loc="lower right")
                plt.show()
                xpresso_save_plot(roc_plot_title, output_path=MOUNT_PATH,
                                  output_folder="report_metrics/" + self.status_desc)
                logger.info("Multiclass/Multilable ROC Curve Saved")
            except Exception as e:
                print(e)
                pass

    def plot_multilabel_confusion_matrix(self):
        """

        Plot confusion matrix for multilabel classifiers

        """
        try:
            # generate multilabel_confusion_matrix
            cm_arrays = metrics.multilabel_confusion_matrix(self.y_true, self.y_pred)

            fig, axes = plt.subplots(len(cm_arrays), figsize=(6, 10))

            # plot a confusion matrix for each class
            for i in range(len(cm_arrays)):
                df_cm = pd.DataFrame(cm_arrays[i])
                sn.set(font_scale=1.2)  # for label size
                sn.heatmap(df_cm, annot=True,
                           cmap=plt.cm.Blues, ax=axes[i], fmt='g')
                cm_plot_title = self.get_name(metrics.multilabel_confusion_matrix) + self.dataset_name + "_class_" + str(
                    self.model.classes_[i])
                axes[i].set_xlabel('Predicted label')
                axes[i].set_ylabel('True label')
                axes[i].set_title(cm_plot_title)

            fig.tight_layout(pad=1.0)
            plt.show()
            xpresso_save_plot(cm_plot_title, output_path=MOUNT_PATH,
                              output_folder="report_metrics/" + self.status_desc)
            logger.info("Multilable Confusion Matrix Saved")
        except Exception as e:
            print(e)
            pass

    def plot_confusion_matrix(self):
        """
        Plot confusion matrix for binary or multiclass classifiers

        """
        try:
            cm_disp = metrics.plot_confusion_matrix(self.model, self.X, self.y_true,
                                                    display_labels=self.model.classes_,
                                                    cmap=plt.cm.Blues)
            cm_disp.ax_.set_title("Confusion Matrix" + self.dataset_name)
            plt.show()
            xpresso_save_plot("Confusion_matrix" + self.dataset_name, output_path=MOUNT_PATH,
                              output_folder="report_metrics/" + self.status_desc)
            logger.info("Confusion Matrix Saved")
        except Exception as e:
            print(e)
            pass

    def save_classification_report(self):
        """
        Save classification report(as csv file) for all types of classification models

        """
        try:
            cls_report = metrics.classification_report(self.y_true, self.y_pred,
                                                       output_dict=True)
            cls_report_df = pd.DataFrame(cls_report).transpose()
            # save model to data folder
            pickle.dump(cls_report_df, open(os.path.join(MOUNT_PATH, "report_metrics/classification_report_" +
                                                         self.dataset_name + ".pkl"), 'wb'))

            logger.info("Classification Report Saved")
        except Exception as e:
            print(e)
            pass



    def populate_metrics_unsupervised_clustering(self):
        """

        Populate metrics for unsupervised clustering model

        """

        # collect and calculate metrics that use y_true, y_pred as inputs
        sklearn_metrics = [metrics.adjusted_mutual_info_score,
                             metrics.adjusted_rand_score,
                             metrics.completeness_score,
                             metrics.fowlkes_mallows_score,
                             metrics.homogeneity_score,
                             metrics.mutual_info_score,
                             metrics.normalized_mutual_info_score,
                             metrics.v_measure_score]

        self.calculate_metrics_general(sklearn_metrics)

        # addtional metrics for unsupervised clustering models
        clustering_specific_metrics = [metrics.calinski_harabasz_score,
                                       metrics.davies_bouldin_score,
                                       metrics.silhouette_score]

        # calculate metrics that use X and y_pred as inputs
        for metric in clustering_specific_metrics:
            try:
                self.metrics_value.append(round(metric(self.X, self.y_pred), 4))
                self.metrics_name.append(self.get_name(metric) + self.dataset_name)
            except Exception as e:
                print(e)
                pass

    @ignore_warnings(category=ConvergenceWarning)
    def plot_validation_curves(self, model, X, y,
                               param_name="max_iter",
                               param_range=None,
                               scoring_metrics=None,
                               **kwargs):

        """
            Generate validation plots: the test, train scores vs param_name plot
            While default parameters (param_name) for the plot is max_iter,
             users have the options to plot validation curve for any parameters of the estimator provided.

            Parameters
            ----------
            estimator : object type that implements the "fit" and "predict" methods from SKlearn

            X : array-like, shape (n_samples, n_features) feature matrix

            y : array-like, shape (n_samples) or (n_samples, n_classes) target variable

            param_name : str, any parameter of the estimator the user wants to evaluate
                        default value is max_iter

            param_range : list, range of parameter values
                        default value is range[1,20]

            scoring_metrics: str or list of str, scoring parameters pre-defined in sklearn, e.g.,"accuracy"
                            see section "The scoring parameter: defining model evaluation rules" within sklearn documentation
                            default for regressor is "neg_mean_squared_error", "neg_root_mean_squared_error"
                            default for classifier is "accuracy", "neg_log_loss"

            groups : groups variable array-like of shape (n_samples,), default=None
                     Group labels for the samples used while splitting the dataset into train/test set.
                     Only used in conjunction with a “Group” cv instance (e.g., GroupKFold).

            **kwargs includes: n_jobs , cv , pre_dispatch , verbose, error_score
            n_jobs :  default None, optional
            cv :  default 5,  optional
            pre_dispatch :  default None, optional
            verbose : int, default=0
            error_score : ‘raise’ or numeric, default=np.nan
        """

        # use default metrics if no values provided to scoring_metrics by end-users
        if scoring_metrics is None:
            if sklearn.base.is_regressor(model):
                scoring_metrics = ["neg_mean_absolute_error", "neg_root_mean_squared_error"]
            elif sklearn.base.is_classifier(model):
                scoring_metrics = ["accuracy", "neg_log_loss"]

        # if end-users provide only one metrics
        elif type(scoring_metrics) == str:
            scoring_metrics = [scoring_metrics]

        # if no param_range is provided, param_range is list of integers range from 1 to 20 for max_iter
        if param_range is None:
            param_range = list(range(1, 21))

        fig, axes = plt.subplots(len(scoring_metrics), 1, figsize=(6, 8))

        # plot learning curve for each scoring metrics
        for i in range(len(scoring_metrics)):
            try:

                train_scores, test_scores = validation_curve(
                                            estimator=model, X=X, y=y,
                                            param_name=param_name, param_range=param_range,
                                            scoring=scoring_metrics[i], **kwargs)


                train_scores_mean = np.mean(train_scores, axis=1)
                train_scores_std = np.std(train_scores, axis=1)
                val_scores_mean = np.mean(test_scores, axis=1)
                val_scores_std = np.std(test_scores, axis=1)

                cv_results_df = pd.DataFrame({param_name: param_range,
                                              "val_scores_mean": val_scores_mean,
                                              "train_scores_mean": train_scores_mean})
                val_score_best = val_scores_mean.max()
                max_iters_best = cv_results_df.loc[cv_results_df.val_scores_mean == val_score_best,
                                                   param_name].values[0]

                # Plot score vs max_iters
                axes[i].grid()
                axes[i].fill_between(param_range,
                                     train_scores_mean - train_scores_std,
                                     train_scores_mean + train_scores_std,
                                     alpha=0.1, color="y")
                axes[i].fill_between(param_range,
                                     val_scores_mean - val_scores_std,
                                     val_scores_mean + val_scores_std,
                                     alpha=0.1, color="g")
                axes[i].plot(param_range,
                             train_scores_mean,
                             'o-', color="y",
                             label="Training score")
                axes[i].plot(param_range,
                             val_scores_mean,
                             'o-', color="g",
                             label="Cross-validation score")
                axes[i].plot(max_iters_best, val_score_best, 'r*', label='Best Param : ' +
                                                                         str(max_iters_best))
                axes[i].set_xlabel(param_name)
                axes[i].set_ylabel(scoring_metrics[i])
                axes[i].set_title("Validation curve : Train/Val Scores vs. " + param_name)
                axes[i].legend(loc="best")

            except Exception as e:
                print(e)
                pass

        fig.tight_layout(pad=3.0)
        plt.show()
        xpresso_save_plot("validation_curve_" + model.__class__.__name__, output_path=MOUNT_PATH,
                          output_folder="report_metrics/")

        logger.info("Learning curve plot saved")

    def get_name(self, metric):
        """
        Reformat metrics name to make sure metric from different libraries to be consistent
        Args:
            metrics_name_str: string, metrics name

        Returns: reformatted metrics name

        """

        if "tensorflow.python.keras" in metric.__module__:
            metric_name = metric.__class__.__name__
            metrics_name_split = [w for w in re.split("([A-Z][^A-Z]*)", metric_name) if w]
            reformated_name = " ".join(metrics_name_split)
        else:
            # "sklearn" in metric.__module__ or custom metrics
            metric_name = metric.__name__
            removed_punct = metric_name.replace("_", " ")
            reformated_name = " ".join([w.capitalize() for w in removed_punct.split()])
        return reformated_name



    def send_metrics(self, metrics, status_desc ="reporting metrics"):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": status_desc},
                "metric": metrics
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()


    def completed(self, push_exp=True, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===

        # make sure data type is sklearn model
        if isinstance(self.model, sklearn.base.BaseEstimator):
            try:
                # create Output folder
                if not os.path.exists(self.OUTPUT_DIR):
                    os.makedirs(self.OUTPUT_DIR)
                # save model to output folder
                pickle.dump(self.model, open(os.path.join(self.OUTPUT_DIR, self.model.__class__.__name__ + ".pkl"), 'wb'))
            except:
                pass

        try:
            super().completed(push_exp=push_exp, success=success)

        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)


    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)


    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)


    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    # if __name__ == "__main__":
    #     # To run locally. Use following command:
    #     # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py
    #
    #     data_prep = SKLearnComponent()
    #     if len(sys.argv) >= 2:
    #         data_prep.start(run_name=sys.argv[1])
    #     else:
    #         data_prep.start(run_name="")
