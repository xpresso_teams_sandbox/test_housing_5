"""
This is the implementation of data preparation for sklearn
"""
# import system libraries
import sys
import logging
import os

import shap

shap.initjs()

# import common python libraries
import re
import pickle
import random
import numpy as np
import pandas as pd
import itertools
from itertools import cycle
from itertools import combinations

# import sklearn libraries
import sklearn
from sklearn import metrics
from sklearn.model_selection import GridSearchCV, validation_curve
from sklearn.preprocessing import label_binarize
from sklearn.utils._testing import ignore_warnings
from sklearn.exceptions import ConvergenceWarning
from sklearn.model_selection import train_test_split

# libraries for plotting
import matplotlib.pyplot as plt
import seaborn as sn
import matplotlib.colors as mcolors
from matplotlib import rcParams

# Following two imports are required for Xpresso. Do not remove this.
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot

# Define default file path for Xpresso to save plots
PICKLE_PATH = "/data"
MOUNT_PATH = "/data"


__author__ = "### Claire Zhang ###"

# To use the logger please provide the name and log level
#   - name is passed as the project name while generating the logs
#   - level can be DEBUG, INFO, WARNING, ERROR, CRITICAL
logger = XprLogger(name="logistic_reg",
                   level=logging.INFO)


class SKLearnComponent(AbstractPipelineComponent):
    """ Specialized class for pipeline jobs that use SKlearn API for model training and evaluation.
     It is extended from AbstractPipelineComponent, which allows xpresso platform to track and manage the pipeline.

    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self, name=None):

        super().__init__(name)
        """ Initialize all the required constansts and data her """

    def start(self, xpresso_run_name):
        """
        The start method starts the experiment corresponding to the xpresso_run_name
        by calling the superclass start method from the abstract_pipeline_component

        Args:
            xpresso_run_name (str) : Unique identifier of that run instance
        """

        super().start(xpresso_run_name=xpresso_run_name)
        # === Your start code base goes here ===

        # except Exception:
        #     import traceback
        #     traceback.print_exc()
        #     self.completed(success=False)
        # self.completed(success=True)

    def report_metrics(self, model, X, y,
                       is_classifier=None,
                       pred_thld=0.5,
                       generate_validation_metrics=True,
                       validation_size=0.2,
                       status_desc="reporting metrics"):
        """
        The report_metrics method collects, calculate and report all metrics available in SKlearn library
         The method automatically detects user's model type (regressor, classifiers-binary, classifiers-multiclass,
         classifiers-multilabel)

        Args:
            model (SKlearn model object) : Trained classification or regression model using scikit-learn library

            X: numpy array, pandas DataFrame, H2O DataTable's Frame or scipy.sparse of shape (n_samples, n_features)
                Feature matrix consists of independent variables (either training set, validation set, or test set)

            y : array-like of shape (n_samples,) or (n_samples, n_classes)
                Target variable vector or matrix (either training set, validation set, or test set)

            is_classifier : boolean (default = None)
                          An indicator whether the model is a classification model as opposite to a regression model.
                          If no value is given,the model type will be inferred from the model object or the data type of
                           target variable (y)

            pred_thld : float (default = 0.5)
                        Prediction threshold used for determining the prediction label for classification model if the
                        model outputs prediction probability

            generate_validation_metrics : boolean (default=True)
                                          An indicator of whether to create a validation set and report validation
                                          metrics.
                                          If True, the method creates training and validation set from X using sklearn
                                          train_test_split, and report metrics for both sets. Metrics names ending with
                                          “(Tr)” represent metrics for the training set. Metrics names ending with
                                          “(Val)” represent metrics for the validation set.
                                          If False, one set of metrics will be computed on the entire X dataset.


            validation_size: float (default=0.2)
                            Proportional of X dataset to create a validation set (used only when
                            generate_validation_metrics is True)

            status_desc : string (default = "Reporting Metrics")
                          Any description that users want to provide, such as name of the dataset that is being sent to
                           report metrics method
        """

        if not isinstance(model, sklearn.base.BaseEstimator):
            return

        # assign attributes
        self.model = model
        self.pred_thld = pred_thld
        self.is_classifier = is_classifier
        self.status_desc = status_desc

        # identify model type (regressor or classifier) based on user's input
        self.identify_model_type()

        # initiate three empty lists for collecting metrics values and names
        self.metrics_value = []
        self.metrics_name = []

        # if validation = True, create train and validation sets from X, y dataset provided by user
        if generate_validation_metrics:
            try:
                X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=validation_size,
                                                                  random_state=42)

                # report metrics for training and validation set separately
                for x, y, dname in [(X_train, y_train, " (Tr)"), (X_val, y_val, " (Val)")]:
                    # make prediction and report metrics
                    self.predict_and_evaluate(x, y, dname)

            except Exception:
                generate_validation_metrics = False
                import traceback
                traceback.print_exc()


        # if validation = False
        if not generate_validation_metrics:
            dname = ""
            self.predict_and_evaluate(X, y, dname)

        # collect metrics names and values in a dictionary
        metrics_dict = dict(zip(self.metrics_name, self.metrics_value))

        # send metrics to xpresso UI
        self.send_metrics(metrics=metrics_dict, status_desc=status_desc)

        logger.info("Metrics Reported")

    def identify_model_type(self):
        # user provides a value ( True or False) to is_classifier parameter
        if type(self.is_classifier) == bool:
            self.is_regressor = not self.is_classifier
        else:
            self.is_regressor = None

    def predict_and_evaluate(self, x, y, dname):
        """

        Args:
            x: array_like or sparse matrix, shape (n_samples, n_features)
                input matrix (either training set, validation set, or test set)
            y: array-like of shape (n_samples,) or (n_samples, n_classes)
                target variable vector or matrix (either training set, validation set, or test set)
            dname: string
                 A label indicating the dataset name, automaticall assigned by SKlearnComponent.
                The value will be "tr" and "val", indicating training set, validation set, if generate_validation_metrics
                 is True. Otherwise, the value will be "" if generate_validation_metrics is False or the data type is
                 not able to be split into training and validation set with sklearn train_test_split.

        Returns: metrics values(list) and metrics names(list)

        """
        # assign predictors and target variable sets and dataset name
        self.X = x
        self.y_true = y
        self.dataset_name = dname

        # identify target_type
        self.target_type = sklearn.utils.multiclass.type_of_target(self.y_true)

        # make predictions
        self.generate_prediction()
        # populate metrics
        self.populate_metrics()

    def generate_prediction(self):
        """
        Generate prediction using model.predict()

        Returns: The output from running sklearn model.predict() (labeled as y_pred).
                For sklearen classification models that have "decision_function" or "predict_proba" methods,
                generate predicted probabilities or predicted decisions

        """
        # Apply trained model to make predictions on given dataset
        self.y_pred = self.model.predict(self.X)

        # check if the trained model has decision_function
        if hasattr(self.model, "decision_function"):
            self.y_score = self.model.decision_function(self.X)
        else:
            self.y_score = None

        # Check if the trained model can predict probability
        if hasattr(self.model, "predict_proba"):
            self.y_pred_proba = self.model.predict_proba(self.X)
        else:
            self.y_pred_proba = None

    def populate_metrics(self):
        """

        Returns: populate metrics based on model type (regressor or classifier)

        """
        # create a folder to save images or csv files generated from calling report metrics method
        report_metrics_path = MOUNT_PATH + "/report_metrics"
        if not os.path.exists(report_metrics_path):
            os.makedirs(report_metrics_path)

        # check if the model is sklearn regressor
        if sklearn.base.is_regressor(self.model) or self.is_regressor:
            self.populate_metrics_regressor()

        # check if the model is a sklearn classifier
        elif sklearn.base.is_classifier(self.model) or self.is_classifier:
            self.populate_metrics_classifier()

    def calculate_metrics_general(self, sklearn_metrics):
        """
        Calculate and record metrics that take y_true and y_pred as inputs
        Args:
            sklearn_metrics: sklearn metrics APIs, automatically assigned by SKlearnComponent based on model type.

        Returns:
            metrics values and metrics name

        """
        for metric in sklearn_metrics:
            try:
                self.metrics_value.append(round(metric(self.y_true, self.y_pred), 2))
                self.metrics_name.append(self.get_name(metric) + self.dataset_name)
            except Exception:
                import traceback
                traceback.print_exc()

    def populate_metrics_regressor(self):
        """
        Populate metrics for regression models using sklearn metrics APIs

        Returns: metrics values and metrics name

        """
        # collect sklearn regression metrics
        sklearn_metrics = [metrics.mean_absolute_error,
                           metrics.mean_squared_error,
                           metrics.mean_squared_log_error,
                           metrics.median_absolute_error,
                           metrics.explained_variance_score,
                           metrics.max_error,
                           metrics.r2_score,
                           metrics.mean_poisson_deviance,
                           metrics.mean_gamma_deviance,
                           metrics.mean_tweedie_deviance]

        self.calculate_metrics_general(sklearn_metrics)

    def populate_metrics_classifier(self):
        """
        Populate metrics, save classification report (as csv file) and plot confusion matrix, roc curve and
        precision_recall curve for all three type of classifiers ((binary, multiclass or multilable).
        SKlearnComponent leverage metrics available from sklearn library. The metrics that will be
        finally reported depends on the classifier type (binary, multiclass or multilable)

        Returns: metrics values and metrics name

        """

        # identify model classes and number of classes
        try:
            self.model_classes = self.model.classes_
        except:
            self.model_classes = sklearn.utils.multiclass.unique_labels(self.y_true)

        self.model_n_classes = len(self.model_classes)

        # return if the target type is not able to be identified or number of classes is less than 2
        if self.target_type == "unknown" or (self.model_n_classes < 2):
            return

        # mutliclass or multilabel indicators
        multi_ind = ["multiclass", "multiclass-multioutput", "multilabel-indicator"]

        # save classification report for all types
        self.save_classification_report()

        # Calculate metrics that use y_true and y_score as inputs
        if self.y_score is not None:
            score_metrics = [metrics.hinge_loss]
            self.populate_metrics_yscore(score_metrics)

        # Calculate metrics that use y_true and y_pred_proba as inputs
        if self.y_pred_proba is not None:
            proba_metrics = [metrics.log_loss]
            self.populate_metrics_yproba(proba_metrics)

        # check if the model is binary classifier:
        if self.target_type == "binary" or self.model_n_classes == 2:
            self.populate_metrics_binary()

            # plot roc curve for binary classifier
            self.plot_roc_curve_binary()

            # plot precision_recall_curve for binary classifier
            self.plot_precision_recall_curve_binary()

            # plot confusion matrix for binary or multiclass classification task
            self.plot_confusion_matrix()

        # check if the model is a multiclass or multilabel classifier
        if self.target_type in multi_ind or self.model_n_classes > 2:

            # plot roc curve for multiclass or multilabels
            self.plot_roc_curve_multi()

            # plot precision recall curve for multiclass or multilabels
            self.plot_precision_recall_curve_multi()

            # for multiclass classification
            if not sklearn.utils.multiclass.is_multilabel(self.y_true):
                self.populate_metrics_multiclass()

                # plot confusion matrix for binary or multiclass classification task
                self.plot_confusion_matrix()

            # for multilabel classification
            elif sklearn.utils.multiclass.is_multilabel(self.y_true):
                self.populate_metrics_multilabel()

                # plot multilabel confusion matrix for all types of classification task
                self.plot_multilabel_confusion_matrix()

    def populate_metrics_yproba(self, proba_metrics):
        """
        Populate metrics for classification models that use y_true and y_pred_proba (prediction probability) as inputs

        Args:
            proba_metrics: list of sklearn metrics APIs

        Returns: metrics values and metrics name

        """

        for metric in proba_metrics:
            try:
                self.metrics_value.append(round(metric(self.y_true, self.y_pred_proba), 4))
                self.metrics_name.append(self.get_name(metric) + self.dataset_name)
            except Exception:
                import traceback
                traceback.print_exc()

    def populate_metrics_yscore(self, score_metrics):
        """
        Populate metrics for classification models that use y_true and y_score (predicted desicion) as inputs

        Args:
            proba_metrics: list of sklearn metrics APIs

        Returns: metrics values and metrics name

        """
        for metric in score_metrics:
            try:
                self.metrics_value.append(round(metric(self.y_true, self.y_score), 4))
                self.metrics_name.append(self.get_name(metric) + self.dataset_name)
            except Exception:
                import traceback
                traceback.print_exc()

    def populate_metrics_binary(self):
        """
        Populate metrics for binary classifiers using both sklearn library.

        Returns: metrics values and metrics name

        """

        # metrics using y_true and y_pred as inputs
        sklearn_metrics = [sklearn.metrics.accuracy_score,
                           sklearn.metrics.balanced_accuracy_score,
                           sklearn.metrics.recall_score,
                           sklearn.metrics.precision_score,
                           sklearn.metrics.f1_score,
                           sklearn.metrics.zero_one_loss,
                           sklearn.metrics.hamming_loss,
                           sklearn.metrics.jaccard_score,
                           sklearn.metrics.matthews_corrcoef]

        self.calculate_metrics_general(sklearn_metrics)

        # calculate metrics that use prediction probabilities as inputs
        if self.y_pred_proba is not None:
            try:
                fpr, tpr, thresholds = sklearn.metrics.roc_curve(self.y_true, self.y_pred_proba[:, 1])
                self.metrics_value.append(round(sklearn.metrics.auc(fpr, tpr), 2))
                self.metrics_name.append(self.get_name(sklearn.metrics.auc) + self.dataset_name)
            except Exception:
                import traceback
                traceback.print_exc()

            clf_metrics_proba = [sklearn.metrics.roc_auc_score,
                                 sklearn.metrics.average_precision_score,
                                 sklearn.metrics.brier_score_loss]
            # calculate metrics that use y_true and y_pred_proba as inputs
            for metric in clf_metrics_proba:
                try:
                    self.metrics_value.append(round(metric(self.y_true, self.y_pred_proba[:, 1]), 2))
                    self.metrics_name.append(self.get_name(metric) + self.dataset_name)
                except Exception:
                    import traceback
                    traceback.print_exc()

        # add additional metrics from confusion matrix
        try:
            tn, fp, fn, tp = metrics.confusion_matrix(self.y_true, self.y_pred).ravel()
            self.metrics_value.extend([self.true_negative_rate(tn, fp),
                                       self.false_positive_rate(fp, tn),
                                       self.false_negative_rate(fn, tp)])
            self.metrics_name.extend(["true_negative_rate" + self.dataset_name,
                                      "false_positive_rate" + self.dataset_name,
                                      "false_negative_rate" + self.dataset_name])
        except Exception:
            import traceback
            traceback.print_exc()

    def true_negative_rate(self, tn, fp):
        """
        Args:
            tn: True Negatives (count)
            fp: False Positives (count)

        Returns: true_negative_rate
        """
        return round(tn / (tn + fp), 2)

    def false_positive_rate(self, fp, tn):
        """
        Args:
            fp: False Positives (count)
            tn: True Negatives (count)

        Returns: false_positive_rate
        """
        return round(fp / (fp + tn), 2)

    def false_negative_rate(self, fn, tp):
        """
        Args:
            fn: False Negatives (count)
            tp: True Positives (count)

        Returns: false_negative_rate
        """
        return round(fn / (fn + tp), 2)

    def populate_metrics_multiclass(self):
        """

        Populate metrics for sklearn multiclass classifiers.

        Returns: metrics values and metrics name

        """

        sklearn_metrics = [sklearn.metrics.accuracy_score,
                           sklearn.metrics.balanced_accuracy_score,
                           sklearn.metrics.zero_one_loss,
                           sklearn.metrics.hamming_loss,
                           sklearn.metrics.matthews_corrcoef]

        self.calculate_metrics_general(sklearn_metrics)

        # sklearn metrics needs averaging method
        averaging_metrics = [metrics.f1_score,
                             metrics.recall_score,
                             metrics.precision_score,
                             metrics.jaccard_score]

        for metric in averaging_metrics:
            for avg_method in ["micro", "macro", "weighted"]:
                try:
                    self.metrics_value.append(round(metric(self.y_true, self.y_pred, average=avg_method), 4))
                    self.metrics_name.append(self.get_name(metric) + "_" + avg_method + self.dataset_name)
                except Exception:
                    import traceback
                    traceback.print_exc()

        # roc_auc_score
        if self.y_pred_proba is not None:
            for avg_method, config in itertools.product(["macro", "weighted"], ["ovr", "ovo"]):
                try:
                    self.metrics_value.append(round(metrics.roc_auc_score(self.y_true, self.y_pred_proba,
                                                                          average=avg_method,
                                                                          multi_class=config), 4))
                    self.metrics_name.append(self.get_name(metrics.roc_auc_score)
                                             + "_" + avg_method + "_" + config
                                             + self.dataset_name)

                except Exception:
                    import traceback
                    traceback.print_exc()

    def populate_metrics_multilabel(self):
        """

        Populate metrics for sklearn multilabel classifiers.

        Returns: metrics values and metrics name

        """

        # collect and calculate metrics that use y_true, y_pred as inputs
        sklearn_metrics = [metrics.accuracy_score,
                           metrics.zero_one_loss,
                           metrics.hamming_loss]

        self.calculate_metrics_general(sklearn_metrics)

        # metrics that needs averaging method
        averaging_metrics = [metrics.f1_score,
                             metrics.recall_score,
                             metrics.precision_score,
                             metrics.jaccard_score,
                             metrics.average_precision_score]
        for metric in averaging_metrics:
            for avg_method in ["micro", "macro", "weighted", "samples"]:
                try:
                    if metric == metrics.average_precision_score:
                        self.metrics_value.append(round(metric(self.y_true, self.y_pred_proba, average=avg_method), 4))
                    else:
                        self.metrics_value.append(round(metric(self.y_true, self.y_pred, average=avg_method), 4))
                    self.metrics_name.append(self.get_name(metric) + "_" + avg_method + self.dataset_name)
                except Exception:
                    import traceback
                    traceback.print_exc()

        # roc_auc_score
        if self.y_pred_proba is not None:
            # reshape prediction probability
            y_pred_proba_t = np.transpose(np.array(self.y_pred_proba)[:, :, 1])

            # roc_auc_score for multilabel
            for avg_method in ["micro", "macro", "weighted", "samples"]:
                try:
                    self.metrics_value.append(round(metrics.roc_auc_score(self.y_true, y_pred_proba_t,
                                                                          average=avg_method), 4))
                    self.metrics_name.append(
                        self.get_name(metrics.roc_auc_score) + "_" + avg_method + self.dataset_name)

                except Exception:
                    import traceback
                    traceback.print_exc()

        # populate ranking metrics
        ranking_metrics = [sklearn.metrics.coverage_error,
                           sklearn.metrics.label_ranking_average_precision_score,
                           sklearn.metrics.label_ranking_loss,
                           sklearn.metrics.ndcg_score]

        self.populate_metrics_yscore(ranking_metrics)

    def plot_precision_recall_curve_binary(self):
        """
        Plot precision recall curve for binary classifiers

        """
        if self.y_score is not None:
            try:
                average_precision = metrics.average_precision_score(self.y_true, self.y_score)
                disp_prec = metrics.plot_precision_recall_curve(self.model, self.X, self.y_true)
                disp_prec.ax_.set_title('Precision-Recall curve' + self.dataset_name +
                                        ' : AP={0:0.2f}'.format(average_precision))
                plt.show()
                xpresso_save_plot("Precision_recall_curve", output_path=MOUNT_PATH,
                                  output_folder="report_metrics/")
            except Exception:
                import traceback
                traceback.print_exc()

    def plot_precision_recall_curve_multi(self):
        """
        Plot precision recall curve for multiclass and multilabel classifiers

        """
        try:
            precision = dict()
            recall = dict()
            average_precision = dict()

            y_true_binarized = sklearn.preprocessing.label_binarize(self.y_true, classes=self.model_classes)

            for i in range(len(self.model_classes)):
                precision[i], recall[i], _ = sklearn.metrics.precision_recall_curve(y_true_binarized[:, i],
                                                                                    self.y_pred_proba[:, i])
                average_precision[i] = sklearn.metrics.average_precision_score(y_true_binarized[:, i],
                                                                               self.y_pred_proba[:, i])

            # A "micro-average": quantifying score on all classes jointly
            precision["micro"], recall["micro"], _ = sklearn.metrics.precision_recall_curve(y_true_binarized.ravel(),
                                                                                            self.y_pred_proba.ravel())
            average_precision["micro"] = sklearn.metrics.average_precision_score(y_true_binarized, self.y_pred_proba,
                                                                                 average="micro")
            # Plot all precision_recall curves
            plt.figure(figsize=(12, 6))

            colors = cycle([i for i in mcolors.TABLEAU_COLORS.values()])
            lines = []
            labels = []
            l, = plt.plot(recall["micro"], precision["micro"], color='gold', lw=2)
            lines.append(l)
            labels.append('micro-average Precision-recall (area = {0:0.2f})'
                          ''.format(average_precision["micro"]))
            for i, color in zip(range(len(self.model_classes)), colors):
                l, = plt.plot(recall[i], precision[i], color=color, lw=2)
                lines.append(l)
                labels.append('Precision-recall for class {0} (area = {1:0.2f})'
                              ''.format(i, average_precision[i]))
            fig = plt.gcf()
            fig.subplots_adjust(bottom=0.25)
            plt.xlim([0.0, 1.0])
            plt.ylim([0.0, 1.05])
            plt.xlabel('Recall')
            plt.ylabel('Precision')
            plt.title(
                'Precision-Recall curve' + self.dataset_name + ': micro AP={0:0.2f}'.format(average_precision["micro"]))
            plt.legend(lines, labels, loc="lower left", prop=dict(size=9))
            plt.show()
            xpresso_save_plot("Precision_recall_curve" + self.dataset_name, output_path=MOUNT_PATH,
                              output_folder="report_metrics/")


        except Exception:
            import traceback
            traceback.print_exc()

    def plot_roc_curve_binary(self):
        """

        Plot roc curve and compute roc area for binary classifiers

        """
        if self.y_pred_proba is not None:
            try:
                average_roc_auc_score = metrics.roc_auc_score(self.y_true, self.y_pred_proba[:, 1])
                disp_roc = metrics.plot_roc_curve(self.model, self.X, self.y_true)
                disp_roc.ax_.set_title('ROC curve' + self.dataset_name +
                                       ' : AUC ROC={0:0.2f}'.format(average_roc_auc_score))
                plt.show()
                xpresso_save_plot("ROC_curve" + self.dataset_name, output_path=MOUNT_PATH,
                                  output_folder="report_metrics/")
            except Exception:
                import traceback
                traceback.print_exc()

    def plot_roc_curve_multi(self):
        """

        Plot ROC curve and compute ROC area for multiclass and multilabel classifiers

        """

        if self.y_score is not None:
            try:
                y_true_binarized = label_binarize(self.y_true, classes=self.model.classes_)
                fpr = dict()
                tpr = dict()
                roc_auc = dict()
                for i in range(len(self.model.classes_)):
                    fpr[i], tpr[i], _ = metrics.roc_curve(y_true_binarized[:, i], self.y_score[:, i])
                    roc_auc[i] = metrics.auc(fpr[i], tpr[i])

                # Compute micro-average ROC curve and ROC area
                fpr["micro"], tpr["micro"], _ = metrics.roc_curve(y_true_binarized.ravel(), self.y_score.ravel())
                roc_auc["micro"] = metrics.auc(fpr["micro"], tpr["micro"])

                # First aggregate all false positive rates
                all_fpr = np.unique(np.concatenate([fpr[i] for i in range(len(self.model.classes_))]))

                # Then interpolate all ROC curves at this points
                mean_tpr = np.zeros_like(all_fpr)
                for i in range(len(self.model.classes_)):
                    mean_tpr += np.interp(all_fpr, fpr[i], tpr[i])

                # Finally average it and compute AUC
                mean_tpr /= len(self.model.classes_)

                fpr["macro"] = all_fpr
                tpr["macro"] = mean_tpr
                roc_auc["macro"] = metrics.auc(fpr["macro"], tpr["macro"])

                # Plot all ROC curves
                plt.figure(figsize=(12, 6))
                lw = 2
                plt.plot(fpr["micro"], tpr["micro"],
                         label='micro-average ROC curve (area = {0:0.2f})'
                               ''.format(roc_auc["micro"]),
                         color='deeppink', linestyle=':', linewidth=4)

                plt.plot(fpr["macro"], tpr["macro"],
                         label='macro-average ROC curve (area = {0:0.2f})'
                               ''.format(roc_auc["macro"]),
                         color='navy', linestyle=':', linewidth=4)

                colors = cycle([i for i in mcolors.TABLEAU_COLORS.values()])
                for i, color in zip(range(len(self.model.classes_)), colors):
                    plt.plot(fpr[i], tpr[i], color=color, lw=lw,
                             label='ROC curve of class {0} (area = {1:0.2f})'
                                   ''.format(i, roc_auc[i]))

                plt.plot([0, 1], [0, 1], 'k--', lw=lw)
                plt.xlim([0.0, 1.0])
                plt.ylim([0.0, 1.05])
                plt.xlabel('False Positive Rate')
                plt.ylabel('True Positive Rate')
                roc_plot_title = self.model.__class__.__name__ + '_ROC_curve' + self.dataset_name
                plt.title(roc_plot_title)
                plt.legend(loc="lower right")
                plt.show()
                xpresso_save_plot(roc_plot_title, output_path=MOUNT_PATH,
                                  output_folder="report_metrics/")
                logger.info("Multiclass/Multilable ROC Curve Saved")
            except Exception:
                import traceback
                traceback.print_exc()

    def plot_multilabel_confusion_matrix(self):
        """

        Plot confusion matrix for multilabel classifiers

        """
        try:
            # generate multilabel_confusion_matrix
            cm_arrays = metrics.multilabel_confusion_matrix(self.y_true, self.y_pred)

            fig, axes = plt.subplots(len(cm_arrays), figsize=(6, 10))

            # plot a confusion matrix for each class
            for i in range(len(cm_arrays)):
                df_cm = pd.DataFrame(cm_arrays[i])
                sn.set(font_scale=1.2)  # for label size
                sn.heatmap(df_cm, annot=True,
                           cmap=plt.cm.Blues, ax=axes[i], fmt='g')
                cm_plot_title = self.get_name(metrics.multilabel_confusion_matrix) + self.dataset_name + "_class_" + \
                                str(self.model.classes_[i])
                axes[i].set_xlabel('Predicted label')
                axes[i].set_ylabel('True label')
                axes[i].set_title(cm_plot_title)

            fig.tight_layout(pad=1.0)
            plt.show()
            xpresso_save_plot(cm_plot_title, output_path=MOUNT_PATH,
                              output_folder="report_metrics/")
            logger.info("Multilable Confusion Matrix Saved")
        except Exception:
            import traceback
            traceback.print_exc()

    def plot_confusion_matrix(self):
        """
        Plot confusion matrix for binary or multiclass classifiers

        """
        try:
            cm_disp = metrics.plot_confusion_matrix(self.model, self.X, self.y_true,
                                                    display_labels=self.model.classes_,
                                                    cmap=plt.cm.Blues)
            cm_disp.ax_.set_title("Confusion Matrix" + self.dataset_name)
            plt.show()
            xpresso_save_plot("Confusion_matrix" + self.dataset_name, output_path=MOUNT_PATH,
                              output_folder="report_metrics/")
            logger.info("Confusion Matrix Saved")
        except Exception:
            import traceback
            traceback.print_exc()

    def save_classification_report(self):
        """
        Save classification report(as csv file) for all types of classification models

        """
        try:
            cls_report = metrics.classification_report(self.y_true, self.y_pred,
                                                       output_dict=True)
            cls_report_df = pd.DataFrame(cls_report).transpose()
            # save model to data folder
            pickle.dump(cls_report_df, open(os.path.join(MOUNT_PATH, "report_metrics/classification_report_" +
                                                         self.dataset_name + ".pkl"), 'wb'))

            logger.info("Classification Report Saved")
        except Exception:
            import traceback
            traceback.print_exc()

    @ignore_warnings(category=ConvergenceWarning)
    def plot_validation_curves(self, model, X, y,
                               param_name="max_iter",
                               param_range=None,
                               scoring_metrics=None,
                               **kwargs):

        """
            Generate validation plots: the test, train scores vs param_name plot
            While default parameters (param_name) for the plot is max_iter,
             users have the options to plot validation curve for any parameters of the estimator provided.

            Parameters
            ----------
            estimator : object type that implements the "fit" and "predict" methods from SKlearn

            X : array-like, shape (n_samples, n_features) feature matrix

            y : array-like, shape (n_samples) or (n_samples, n_classes) target variable

            param_name : str, any parameter of the estimator the user wants to evaluate
                        default value is max_iter

            param_range : list, range of parameter values
                        default value is range[1,20]

            scoring_metrics: str or list of str, scoring parameters pre-defined in sklearn, e.g.,"accuracy"
                            see section "The scoring parameter: defining model evaluation rules" within sklearn documentation
                            default for regressor is "neg_mean_squared_error", "neg_root_mean_squared_error"
                            default for classifier is "accuracy", "neg_log_loss"

            groups : groups variable array-like of shape (n_samples,), default=None
                     Group labels for the samples used while splitting the dataset into train/test set.
                     Only used in conjunction with a “Group” cv instance (e.g., GroupKFold).

            **kwargs includes: n_jobs , cv , pre_dispatch , verbose, error_score
            n_jobs :  default None, optional
            cv :  default 5,  optional
            pre_dispatch :  default None, optional
            verbose : int, default=0
            error_score : ‘raise’ or numeric, default=np.nan
        """

        # use default metrics if no values provided to scoring_metrics by end-users
        if scoring_metrics is None:
            if sklearn.base.is_regressor(model):
                scoring_metrics = ["neg_mean_absolute_error", "neg_root_mean_squared_error"]
            elif sklearn.base.is_classifier(model):
                scoring_metrics = ["accuracy", "neg_log_loss"]

        # if end-users provide only one metrics
        elif type(scoring_metrics) == str:
            scoring_metrics = [scoring_metrics]

        # if no param_range is provided, param_range is list of integers range from 1 to 20 for max_iter
        if param_range is None:
            param_range = list(range(1, 21))

        fig, axes = plt.subplots(len(scoring_metrics), 1, figsize=(6, 8))

        # plot learning curve for each scoring metrics
        for i in range(len(scoring_metrics)):
            try:

                train_scores, test_scores = validation_curve(
                    estimator=model, X=X, y=y,
                    param_name=param_name, param_range=param_range,
                    scoring=scoring_metrics[i], **kwargs)

                train_scores_mean = np.mean(train_scores, axis=1)
                train_scores_std = np.std(train_scores, axis=1)
                val_scores_mean = np.mean(test_scores, axis=1)
                val_scores_std = np.std(test_scores, axis=1)

                cv_results_df = pd.DataFrame({param_name: param_range,
                                              "val_scores_mean": val_scores_mean,
                                              "train_scores_mean": train_scores_mean})
                val_score_best = val_scores_mean.max()
                max_iters_best = cv_results_df.loc[cv_results_df.val_scores_mean == val_score_best,
                                                   param_name].values[0]

                # Plot score vs max_iters
                axes[i].grid()
                axes[i].fill_between(param_range,
                                     train_scores_mean - train_scores_std,
                                     train_scores_mean + train_scores_std,
                                     alpha=0.1, color="b")
                axes[i].fill_between(param_range,
                                     val_scores_mean - val_scores_std,
                                     val_scores_mean + val_scores_std,
                                     alpha=0.1, color="g")
                axes[i].plot(param_range,
                             train_scores_mean,
                              color="b",
                             label="Training score")
                axes[i].plot(param_range,
                             val_scores_mean,
                             color="g",
                             label="Cross-validation score")
                axes[i].plot(max_iters_best, val_score_best, 'r*', label='Best Param : ' +
                                                                         str(max_iters_best))
                axes[i].set_xlabel(param_name)
                axes[i].set_ylabel(scoring_metrics[i])
                axes[i].set_title("Validation curve : Train/Val Scores vs. " + param_name)
                axes[i].legend(loc="best")

            except Exception:
                import traceback
                traceback.print_exc()

        fig.tight_layout(pad=3.0)
        plt.show()
        xpresso_save_plot("validation_curve_" + model.__class__.__name__, output_path=MOUNT_PATH,
                          output_folder="report_metrics/")

        logger.info("Learning curve plot saved")

    def get_name(self, metric):
        """
        Reformat metrics name to make sure metric from different libraries to be consistent
        Args:
            metrics_name_str: string, metrics name

        Returns: reformatted metrics name

        """

        if "tensorflow.python.keras" in metric.__module__:
            metric_name = metric.__class__.__name__
            metrics_name_split = [w for w in re.split("([A-Z][^A-Z]*)", metric_name) if w]
            reformated_name = " ".join(metrics_name_split)
        else:
            # "sklearn" in metric.__module__ or custom metrics
            metric_name = metric.__name__
            removed_punct = metric_name.replace("_", " ")
            reformated_name = " ".join([w.capitalize() for w in removed_punct.split()])
        return reformated_name

    def explain_shap(self,
                     model,
                     X,
                     shap_explainer=None,
                     shap_explaining_set_size=None,
                     shap_n_individual_instance=1,
                     shap_n_top_features=5,
                     shap_n_top_interaction_features=3,
                     shap_link="identity",
                     shap_feature_names=None,
                     feature_perturbation="interventional"):
        """
        Create Shapley plots for the trained model.

       Allows an approximation of shapley values for a given model based on a number of samples
       from data source X

        Args:
            X (dataframe, numpy array, or pixel_values) : dataset to be explain

             model(object) : model to be explained

             shap_explainer (string, optional): choose one of explainer in the following list or if leave as None, the
                                                 method will try each explainer in the list in order until it reaches
                                                 one that that is compatible with the given model type

                            {"TreeExplainer","LinearExplainer","DeepExplainer","GradientExplainer",
                            "KernelExplainer", "SamplingExplainer"}

            shap_feature_names (list of strings, optional) : Feature names to be used in creating SHAP plots.
                                                            If left as None, feature names will be inferred from
                                                            the X dataset (if DatraFrame) or created with index
                                                            ( Feature_1, Feature_2, …)

            shap_explaining_set_size(int, optional): Number of samples in X to be explained by SHPA.
                                                    Any number ranging from 1 to the size of X set is valid, and if the
                                                    number is smaller than size of X, the explaining set is created by
                                                    random sampling from the X set.
                                                    If pass -1, the entire dataset will be used as explaining set.
                                                    If no values provided, some default numbers will be used according
                                                    to the type of SHAP explainer: for Kernel explainer,
                                                    200 random samples from X will be used. For any other type of
                                                    explainers,the entire X set will be used as the explaining set.
                                                    Typically 1K to 10K samples is good for visualization and enough to
                                                    detect patterns in the data. Users are encouraged to experiment
                                                    with the number of samples in order to determine a value that
                                                    balances explanation accuracy and runtime.

            shap_n_individual_instance (int, optional): Number of individual instance(s)/sample(s)/observation(s) in the
                                                        explaining set to be randomly chosen to create SHAP Force Plots
                                                        and SHAP Decision Plots.  Default is 1.

            shap_n_top_features(int, optional): Number of top features (by SHAP Values) to create SHAP Dependence Plots.
                                                If values is set to -1, all independent features will be used (one SHAP
                                                Dependence Plot for each feature).
                                                Default is 5.

             shap_n_top_interaction_features(int, optional): Number of top features (by SHAP Interaction Values) to
                                                create SHAP Interaction Values Dependence Plot (only supporting
                                                TreeExpaliner for now ).
                                                If values is set to -1, all independent features will be used (one SHAP
                                                Interaction Values Dependence Plot for each pair of features).
                                                Default is 3.

            shap_link (string, optional) : default is 'identity' or use 'logit' to transform log odds to probabilities

            feature_perturbation: A parameter that goes into SHAP TreeExplainer indicating whether to break the
                                    dependencies between features. Accepted values are "interventional" (default) or
                                    "tree_path_dependent" (default when the background data is None)


            Returns:
                SHAP plots

            """
        #assign variables
        self.model = model
        self.feature_perturbation = feature_perturbation

        # generate explainer based on model type
        self.explainer = self.generate_explainer(X, shap_explainer)

        # end the program if the explainer is None
        if not self.explainer:
            return

        # print the type of SHAP explainer used if explainer generated successfully
        print("SHAP Explainer used : ", self.return_name(self.explainer))

        # assign variables
        self.shap_explaining_set_size = shap_explaining_set_size
        self.shap_n_individual_instance = shap_n_individual_instance

        # define number of top features to plot
        if shap_n_top_features == -1:
            self.shap_n_top_features = X.shape[1]
        else:
            self.shap_n_top_features = shap_n_top_features

        # define number of top features to plot interaction values plots
        if shap_n_top_interaction_features == -1:
            self.shap_n_top_interaction_features = X.shape[1]
        else:
            self.shap_n_top_interaction_features = shap_n_top_interaction_features

        # generate explaining set
        x_explain = self.generate_explaining_set(X)

        # compute SHAP value, expected value, interaction values(if support)
        shap_values = self.explainer.shap_values(x_explain)
        expected_value = self.explainer.expected_value
        try:
            shap_interaction_values = self.explainer.shap_interaction_values(x_explain)
        except:
            shap_interaction_values = None

        # obtain feature names
        feature_names = self.shap_generate_feature_names(X, shap_feature_names)

        # check if shap_values have multiple elements, as a result of a multioutput model
        if isinstance(shap_values, list):
            n_outputs = len(shap_values)

            for i in range(n_outputs):
                # provide a label for the output
                multioutput_n = f"multioutput_{i}"

                if shap_interaction_values is not None:
                    shap_interaction_values_i = shap_interaction_values[i]
                else:
                    shap_interaction_values_i = None

                self.shap_plots_single_output(shap_values=shap_values[i],
                                              expected_value=expected_value[i],
                                              shap_interaction_values=shap_interaction_values_i,
                                              x_explain=x_explain,
                                              feature_names=feature_names,
                                              multioutput_n=multioutput_n,
                                              link=shap_link)

        else:
            multioutput_n = ""
            self.shap_plots_single_output(shap_values=shap_values,
                                          expected_value=expected_value,
                                          shap_interaction_values=shap_interaction_values,
                                          x_explain=x_explain,
                                          feature_names=feature_names,
                                          multioutput_n=multioutput_n,
                                          link=shap_link)

    def shap_generate_feature_names(self, X, shap_feature_names):
        """
        Define feature names for SHAP plots. If user provides feature names(as a list), use user's input, or use column
        names of the X dataframe if exist, or create index-based feature names ("Feature_1", "Feature_2",...)

        Args:
            X: feature matrix
            shap_feature_names: a list of feature names given by users (a parameter of explain_shap)

        Returns:
            feature names for SHAP plots

        """
        # Define feature names
        if shap_feature_names:
            # if user provide feature names, apply user's inputs
            feature_names = shap_feature_names
        # if X is a dataframe, get feature names from dataframe
        elif isinstance(X, pd.DataFrame):
            feature_names = X.columns.values.tolist()
        # else, label feature with index
        else:
            feature_names = [f"Feature_{i}".format(i) for i in range(X.shape[1])]
        return feature_names

    def generate_explaining_set(self, X):
        """

        Args:
            X: dataframe to be explained (provided by user)

        Returns:
            A dataset to be explained by SHAP explainers

        """
        # Define SHAP explaining set size
        # if user provide a value, apply user's input
        if self.shap_explaining_set_size:
            n_explain = self.shap_explaining_set_size
        # if user doesn't provide number of data to explain
        else:
            # For Kernel explainer, use up to 1000 samples to speed up the process as
            if isinstance(self.explainer, shap.KernelExplainer):
                n_explain = min(1000, X.shape[0])
            # Use all samples in X as explaining set
            else:
                n_explain = X.shape[0]

        # prepare data
        # if X is a dataframe, convert X to numpy array
        if isinstance(X, pd.DataFrame):
            X = X.to_numpy()

        # obtain explaining set (x matrix to be explained by SHAP)
        x_explain_ind = np.random.choice(X.shape[0], min(n_explain, X.shape[0]), replace=False)
        x_explain = X[x_explain_ind]
        return x_explain

    def generate_explainer(self, X, shap_explainer):
        """

        Args:
            X: dataset to be explained by SHAP
            shap_explainer: user's preferred SHAP explainer to used (optional)

        Returns:
            A SHAP explainer

        """

        # convert background data as numpy array,
        # Based on SHAP, anywhere from 100 to 1000 random background samples are good sizes to use
        if isinstance(X, pd.DataFrame):
            X = X.to_numpy()
        bdata = X[np.random.choice(X.shape[0], min(1000, X.shape[0]), replace=False)]

        # Listing of all SHAP Explainers
        shapExplainers = [shap.TreeExplainer,
                          shap.LinearExplainer,
                          shap.KernelExplainer,
                          shap.SamplingExplainer]
        shapExplainers_names = ["TreeExplainer",
                                "LinearExplainer",
                                "KernelExplainer",
                                "SamplingExplainer"]
        shapExplainer_dict = dict(zip(shapExplainers_names, shapExplainers))

        # if user choose a SHAPExplainer, go with user's preference
        if shap_explainer:
            if shap_explainer in ["SamplingExplainer", "KernelExplainer"]:
             # if using KernelExplainer, the size of background data is limited to 100 otherwise shap gives a warning
                if shap_explainer == "KernelExplainer":
                    bdata = bdata[:100]
                try:
                    explainer = shapExplainer_dict[shap_explainer](self.model.predict, bdata)
                    return explainer
                except Exception:
                    import traceback
                    traceback.print_exc()

            # For SHAP TreeExplainer, when feature_perturbation is interventional, background data is required
            elif (shap_explainer == "TreeExplainer") and (self.feature_perturbation == "interventional"):
                try:

                    explainer = shapExplainer_dict[shap_explainer](self.model, bdata,
                                                      feature_perturbation=self.feature_perturbation)
                    return explainer
                except Exception:
                    import traceback
                    traceback.print_exc()

            # For SHAP TreeExplainer, when feature_perturbation is tree_path_dependent, background data is not required
            elif (shap_explainer == "TreeExplainer") and (self.feature_perturbation == "tree_path_dependent"):
                try:
                    explainer = shapExplainer_dict[shap_explainer](self.model,
                                                                   feature_perturbation=self.feature_perturbation)
                    return explainer
                except Exception:
                    import traceback
                    traceback.print_exc()

            # for any other SHAP explainers
            else:
                try:
                    explainer = shapExplainer_dict[shap_explainer](self.model, bdata)
                    return explainer
                except Exception:
                    import traceback
                    traceback.print_exc()

        # if user doesn't provide a SHAPExplainer of choice or the SHAPExplainer provided by user is not compatible with
        # the model, try SHAPexplainers in the order of the list
        for i in shapExplainers_names:

            if i in ["SamplingExplainer", "KernelExplainer"]:
                # if using KernelExplainer, the size of background data is limited to 100 otherwise shap gives a warning
                if shap_explainer == "KernelExplainer":
                    bdata = bdata[:100]
                try:
                    explainer = shapExplainer_dict[i](self.model.predict, bdata)
                    return explainer
                except Exception:
                    import traceback
                    traceback.print_exc()

            # For SHAP TreeExplainer, when feature_perturbation is interventional, background data is required
            elif (i == "TreeExplainer") and (self.feature_perturbation == "interventional"):
                try:
                    # First try initiating TreeExplainer with a background dataset
                    # When a background dataset is giving, default value of feature_perturbation is "interventional"
                    explainer = shapExplainer_dict[i](self.model, bdata,
                                                      feature_perturbation=self.feature_perturbation)
                    return explainer
                except Exception:
                    import traceback
                    traceback.print_exc()

            # For SHAP TreeExplainer, when feature_perturbation is tree_path_dependent, background data is not required
            elif (i == "TreeExplainer") and (self.feature_perturbation == "tree_path_dependent"):
                try:
                    explainer = shapExplainer_dict[i](self.model, feature_perturbation=self.feature_perturbation)
                    return explainer
                except Exception:
                    import traceback
                    traceback.print_exc()

            # for any other SHAP explainers
            else:
                try:
                    explainer = shapExplainer_dict[i](self.model, bdata)
                    return explainer
                except Exception:
                    import traceback
                    traceback.print_exc()

        return None

    def shap_plots_single_output(self, shap_values,
                                 expected_value,
                                 shap_interaction_values,
                                 x_explain,
                                 feature_names,
                                 multioutput_n,
                                 link):
        """
        Greate SHAP plots for single output of regressor or classifier model


        """

        # save features and shap values as dataframe in csv file
        self.save_shap_values_df(feature_names, multioutput_n, shap_values)

        # Summary Plot with SHAP Values (dot plot)
        self.shap_summary_plot_dot(feature_names, multioutput_n, shap_values, x_explain)

        # # Summary Plot with SHAP mean absolute Values (bar plot)
        self.shap_summary_plot_bar(feature_names, multioutput_n, shap_values, x_explain)

        # dependence_plot for top n most important features
        self.shap_dependence_plot(feature_names, multioutput_n, shap_values, x_explain)

        # SHAP force plot collective
        self.shap_force_plot_collective(expected_value, feature_names, link, multioutput_n, shap_values, x_explain)

        # SHAP force plot single instance
        self.shap_force_plot_single_instance(expected_value, feature_names, link, multioutput_n,
                                             shap_values, x_explain)

        # SHAP decision plot collective
        self.shap_decision_plot_collective(expected_value, feature_names, link,
                                                                           multioutput_n, shap_values, x_explain)

        # SHAP decision plot single instance
        self.shap_decision_plot_single_instance(expected_value, feature_names, link, multioutput_n,
                                                shap_values, x_explain)

        # SHAP waterfall plots
        self.shap_waterfall_plots(expected_value, feature_names, multioutput_n, shap_values, x_explain)

        # if SHAP interaction values are available
        if shap_interaction_values is not None:

            # SHAP summary plot with interaction values
            self.shap_summary_plot_interaction(feature_names, multioutput_n, shap_interaction_values, x_explain)

            # Heatmap by SHAP Interaction Values
            self.shap_inter_values_heatmap(feature_names, multioutput_n,
                                                                   shap_interaction_values)

            # SHAP dependence plot with interaction values
            self.shap_dependence_plot_interaction(feature_names, multioutput_n, shap_interaction_values, x_explain)

            # SHAP interaction values decision plot
            self.shap_decision_plot_interaction(shap_values, expected_value, feature_names, link, multioutput_n,
                                                shap_interaction_values, x_explain)

    def shap_summary_plot_bar(self, feature_names, multioutput_n, shap_values, x_explain):
        """
        Create SHAP Summary Plot - SHAP Values(mean absolute value)

        """
        # Summary Plot - SHAP Values(mean absolute value)
        try:
            shap.summary_plot(shap_values,
                              x_explain,
                              feature_names=feature_names,
                              plot_type="bar",
                              show=False)
            plt.title(f"Feature Importance by SHAP Values(mean absolute value) {multioutput_n}")
            plt.show()
            xpresso_save_plot(f"shap_values_summary_bar_plot{multioutput_n}", output_path=MOUNT_PATH,
                              output_folder="report_metrics/")
        except Exception:
            import traceback
            traceback.print_exc()

    def shap_summary_plot_interaction(self, feature_names, multioutput_n, shap_interaction_values, x_explain):
        """
        Create SHAP Summary Plot with SHAP Interaction Values

        """
        try:
            shap.summary_plot(shap_interaction_values,
                              # x_explain[0:n_exp_inter],
                              x_explain,
                              plot_type="compact_dot",
                              feature_names=feature_names,
                              show=False)
            plt.title(f"SHAP Interaction Value Summary Plot {multioutput_n}")
            plt.show()
            xpresso_save_plot(f"shap_interaction_values_summary_plot{multioutput_n}", output_path=MOUNT_PATH,
                              output_folder="report_metrics/")
        except Exception:
            import traceback
            traceback.print_exc()

    def shap_decision_plot_interaction(self, shap_values, expected_value, feature_names, link, multioutput_n,
                                       shap_interaction_values, x_explain):
        """
        Create SHAP Decision Plot with SHAP Interaction Values

        """
        # use up to 200 samples for better visualization
        sm_x_explain = min(200, len(shap_values))

        if shap_interaction_values is not None:
            try:
                shap.decision_plot(expected_value,
                                   shap_interaction_values[0:sm_x_explain],
                                   x_explain[0:sm_x_explain],
                                   feature_names=feature_names,
                                   link=link,
                                   show=False)
                plt.title(f"SHAP Interaction Values Decision Plot (collective) {multioutput_n}")
                plt.show()
                xpresso_save_plot(f"shap_interaction_values_decision_plot_collective{multioutput_n}",
                                  output_path=MOUNT_PATH,
                                  output_folder="report_metrics/")
            except Exception:
                import traceback
                traceback.print_exc()

    def shap_decision_plot_single_instance(self, expected_value, feature_names, link, multioutput_n,
                                           shap_values, x_explain):
        """
        Create SHAP Decision Plot with one instance/observation

        """
        # Individual Decision Plot
        for ob in self.random_obs:
            try:
                # plot the SHAP values for the randomly selected observations
                shap.decision_plot(expected_value,
                                   shap_values[ob],
                                   x_explain[ob],
                                   feature_names=feature_names,
                                   link=link,
                                   show=False)
                plt.title(f"SHAP Decision Plot of observation {ob} {multioutput_n}")
                plt.show()
                xpresso_save_plot(f"shap_decision_plot_sample#{ob}{multioutput_n}",
                                  output_path=MOUNT_PATH,
                                  output_folder="report_metrics/")
            except Exception:
                import traceback
                traceback.print_exc()

    def shap_decision_plot_collective(self, expected_value, feature_names, link, multioutput_n, shap_values, x_explain):
        """
        Create SHAP Decision Plot with a number of instances/observations

        """
        # For better visualization, limit to 200 samples, otherwise SHAP gives a warning
        sm_x_explain = min(200, len(shap_values))
        try:
            rcParams['axes.titlepad'] = 24
            shap.decision_plot(expected_value,
                             shap_values[0:sm_x_explain],
                             x_explain[0:sm_x_explain],
                             feature_names=feature_names,
                             link=link,
                             return_objects=True,
                             show=False)
            plt.title(f"SHAP Decision Plot (collective) {multioutput_n}")
            plt.show()
            xpresso_save_plot(f"shap_decision_plot_collective{multioutput_n}", output_path=MOUNT_PATH,
                              output_folder="report_metrics/")
        except Exception:
            import traceback
            traceback.print_exc()



    def shap_force_plot_single_instance(self, expected_value, feature_names, link, multioutput_n, shap_values,
                                        x_explain):
        """
        Create SHAP Force Plot with one random sampled instance/observation

        """
        # Randome Sample observations
        self.random_obs = random.sample(range(len(x_explain)), self.shap_n_individual_instance)
        # Individual force plot
        for ob in self.random_obs:
            try:
                # plot the SHAP values for the random sampled observations
                rcParams['axes.titlepad'] = 24
                ind_force_plot = shap.force_plot(expected_value,
                                                 shap_values[ob],
                                                 x_explain[ob],
                                                 feature_names=feature_names,
                                                 link=link)
                shap.save_html(
                    os.path.join(MOUNT_PATH, f"report_metrics/shap_force_plot_sample#{ob}{multioutput_n}.html"),
                    ind_force_plot)
            except Exception:
                import traceback
                traceback.print_exc()


    def shap_force_plot_collective(self, expected_value, feature_names, link, multioutput_n, shap_values, x_explain):
        """
        Create SHAP Force Plot with a number of instances/observations

        """
        try:
            rcParams['axes.titlepad'] = 24
            force_plot = shap.force_plot(expected_value,
                                         shap_values,
                                         x_explain,
                                         feature_names=feature_names,
                                         link=link)
            shap.save_html(os.path.join(MOUNT_PATH, f"report_metrics/shap_force_plot(collective){multioutput_n}.html"),
                           force_plot)
        except Exception as e:
            raise

    def shap_waterfall_plots(self, expected_value, feature_names, multioutput_n, shap_values, x_explain):
        """
        Create SHAP Waterfall Plot with a number of instances/observations

        """
        # make sure expected value is a float value instead of a array containing the float value
        try:
            expected_value = expected_value[0]
        except Exception:
            import traceback
            traceback.print_exc()
        # plot the waterfall plots for the random selected observations
        for ob in self.random_obs:
            try:
                shap.waterfall_plot(expected_value,
                                    shap_values[ob],
                                    x_explain[ob],
                                    feature_names=feature_names,
                                    show=False)
                plt.title(f"SHAP Waterfall Plot of observation {ob} {multioutput_n}")
                plt.show()
                xpresso_save_plot(f"shap_waterfall_plot_sample#{ob}{multioutput_n}",
                                  output_path=MOUNT_PATH,
                                  output_folder="report_metrics/")
            except Exception:
                import traceback
                traceback.print_exc()

    def shap_dependence_plot(self, feature_names, multioutput_n, shap_values, x_explain):
        """
        Create SHAP Dependence Plot for a number of features (determined by users or default is 5)

        """
        rank_inds = np.argsort(-np.sum(np.abs(shap_values), 0))
        top_inds = rank_inds[:self.shap_n_top_features]
        for i in top_inds:
            try:
                shap.dependence_plot(feature_names[i],
                                     shap_values,
                                     x_explain,
                                     feature_names=feature_names,
                                     show=False)
                plt.title(f"Dependence Plot : {feature_names[i]} {multioutput_n}")
                plt.show()
                xpresso_save_plot(f"shap_dependence_plot_{feature_names[i]}{multioutput_n}", output_path=MOUNT_PATH,
                                  output_folder="report_metrics/")
            except Exception:
                import traceback
                traceback.print_exc()

    def shap_dependence_plot_interaction(self, feature_names, multioutput_n, shap_interaction_values, x_explain):
        """
        Create SHAP Dependence Plot with SHAP interaction values for a number of feature pairs (determined by users or
        default is 3)

        """

        # sort features based on interaction values(absolute)
        tmp_abs = np.abs(shap_interaction_values).sum(0)
        for i in range(tmp_abs.shape[0]):
            tmp_abs[i, i] = 0
        sorted_inds = np.argsort(-tmp_abs.sum(0))
        sorted_features_inter = [feature_names[ind] for ind in sorted_inds]

        # Plot n top features interaction values dependence plot, default is 3
        top_features_inter = sorted_features_inter[0:self.shap_n_top_interaction_features]
        top_feature_combs = []
        for c in combinations(top_features_inter, 2):
            top_feature_combs.append(c)
        for i in range(len(top_feature_combs)):
            try:
                fea_0 = top_feature_combs[i][0]
                fea_1 = top_feature_combs[i][1]
                # make plots
                shap.dependence_plot((fea_0, fea_1),
                                     shap_interaction_values,
                                     x_explain,
                                     feature_names=feature_names,
                                     show=False)
                plt.title(f"Dependence Plot Interaction Values: {fea_0} vs {fea_1}")
                plt.show()
                xpresso_save_plot(f"shap_dependence_plot_{fea_0}_vs_{fea_1}{multioutput_n}", output_path=MOUNT_PATH,
                                  output_folder="report_metrics/")
            except Exception:
                import traceback
                traceback.print_exc()

    def shap_inter_values_heatmap(self, feature_names, multioutput_n, shap_interaction_values):
        """

        Create a heat map for SHAP Interaction Values (Within SHAP library, interaction values are only available for
        TreeExplainer for now)

        """
        try:
            # sort features based on interaction values(absolute)
            tmp_abs = np.abs(shap_interaction_values).sum(0)
            for i in range(tmp_abs.shape[0]):
                tmp_abs[i, i] = 0
            sorted_inds = np.argsort(-tmp_abs.sum(0))
            sorted_features_inter = [feature_names[ind] for ind in sorted_inds]

            # sum interaction values across all samples for each feature
            tmp = shap_interaction_values.sum(0)
            # re-arrange heatmap feature based on interaction values (absoluate value) use top 20 features
            tmp_inds = sorted_inds[0: max(20, self.shap_n_top_features)]
            tmp_ranked = tmp[tmp_inds, :][:, tmp_inds]
            tem_feature_inter = [feature_names[ind] for ind in tmp_inds]

            # plot heatmap
            plt.figure(figsize=(12, 12))
            heat_map = sn.heatmap(tmp_ranked, annot=True, cmap="YlGnBu")
            plt.ylabel('Features')
            heat_map.set_yticklabels(tem_feature_inter,
                                     rotation=50.4,
                                     horizontalalignment="right")
            heat_map.set_xticklabels(tem_feature_inter,
                                     rotation=50.4,
                                     horizontalalignment="left")
            plt.title(f"Heatmap by SHAP Interaction Values {multioutput_n}")
            plt.gca().xaxis.tick_top()
            plt.show()
            xpresso_save_plot(f"shap_heatmap_by_interaction_values{multioutput_n}", output_path=MOUNT_PATH,
                              output_folder="report_metrics/")
        except Exception:
            import traceback
            traceback.print_exc()


    def shap_summary_plot_dot(self, feature_names, multioutput_n, shap_values, x_explain):
        """
        Create SHAP summary plots (dot plot type)

        """


        try:
            shap.summary_plot(shap_values,
                              x_explain,
                              feature_names=feature_names,
                              show=False)
            plt.title(f"Feature Importance by SHAP Values {multioutput_n}")
            plt.ylabel("Features")
            plt.show()
            xpresso_save_plot(f"shap_values_summary_plot{multioutput_n}", output_path=MOUNT_PATH,
                              output_folder="report_metrics/")
        except Exception:
            import traceback
            traceback.print_exc()

    def save_shap_values_df(self, feature_names, multioutput_n, shap_values):
        """

        Compute SHAP Values for the explaining set and save as a dataframe in NFS

        """
        # Rank features based on SHAP Values (sum of the absolute value of shap values)
        shap_values_abs = (np.abs(shap_values)).sum(axis=0)
        sorted_features = [f for _, f in sorted(zip(shap_values_abs, feature_names), reverse=True)]
        sorted_shap_values = [round(v, 4) for v, _ in sorted(zip(shap_values_abs, feature_names),
                                                             reverse=True)]
        # save SHAP values to a dataframe
        try:
            shap_values_df = pd.DataFrame({"features": sorted_features,
                                           "SHAP_values": sorted_shap_values})
            shap_values_df.to_csv(f"shap_values" + multioutput_n + ".csv")
        except Exception:
            import traceback
            traceback.print_exc()

    def return_name(self, shap_explainer_obj):
        """

        Return the name of SHAP Explainer in string type

        """
        type_str = re.search(r"<class '(.*)'>", str(type(shap_explainer_obj)))
        return type_str.group(1)

    def send_metrics(self, metrics, status_desc="reporting metrics"):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": status_desc},
                "metric": metrics
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=True, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===

        # make sure data type is sklearn model
        if isinstance(self.model, sklearn.base.BaseEstimator):
            try:
                # create Output folder
                if not os.path.exists(self.OUTPUT_DIR):
                    os.makedirs(self.OUTPUT_DIR)
                # save model to output folder
                pickle.dump(self.model,
                            open(os.path.join(self.OUTPUT_DIR, self.model.__class__.__name__ + ".pkl"), 'wb'))
            except Exception:
                import traceback
                traceback.print_exc()

        try:
            super().completed(push_exp=push_exp, success=success)

        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    # if __name__ == "__main__":
    #     # To run locally. Use following command:
    #     # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py
    #
    #     data_prep = SKLearnComponent()
    #     if len(sys.argv) >= 2:
    #         data_prep.start(run_name=sys.argv[1])
    #     else:
    #         data_prep.start(run_name="")
