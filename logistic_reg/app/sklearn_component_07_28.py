"""
This is the implementation of data preparation for sklearn
"""
# import system libraries
import sys
import logging
import os

# import common python libraries
import numpy as np
import pandas as pd
from itertools import cycle
import pickle

# import sklearn libraries
import sklearn
from sklearn import metrics
from sklearn.model_selection import GridSearchCV, validation_curve
from sklearn.preprocessing import label_binarize
from sklearn.utils._testing import ignore_warnings
from sklearn.exceptions import ConvergenceWarning
from sklearn.model_selection import train_test_split

# libraries for plotting
import matplotlib.pyplot as plt
import seaborn as sn
import matplotlib.colors as mcolors

# Following two imports are required for Xpresso. Do not remove this.
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot

# Define default file path for Xpresso to save plots
MOUNT_PATH = "/data"

__author__ = "### Claire Zhang ###"

# To use the logger please provide the name and log level
#   - name is passed as the project name while generating the logs
#   - level can be DEBUG, INFO, WARNING, ERROR, CRITICAL
logger = XprLogger(name="logistic_reg",
                   level=logging.INFO)


class SKLearnComponent(AbstractPipelineComponent):
    """ Specialized class for pipeline jobs that use SKlearn API for model training and evaluation.
     It is extended from AbstractPipelineComponent, which allows xpresso platform to track and manage the pipeline.

    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self, name=None):

        super().__init__(name)
        """ Initialize all the required constansts and data her """

    def start(self, xpresso_run_name):
        """
        The start method starts the experiment corresponding to the xpresso_run_name
        by calling the superclass start method from the abstract_pipeline_component

        Args:
            xpresso_run_name (str) : Unique identifier of that run instance
        """

        super().start(xpresso_run_name=xpresso_run_name)
        # === Your start code base goes here ===

        # except Exception:
        #     import traceback
        #     traceback.print_exc()
        #     self.completed(success=False)
        # self.completed(success=True)



    def report_metrics(self, model, X, y,
                       generate_validation_metrics = True, validation_size = 0.2,
                       status_desc="reporting metrics"):
        """
        The report_metrics method collects, calculate and report all metrics available in SKlearn library
         The method automatically detects user's model type (regressor, classifiers-binary, classifiers-multiclass,
         classifiers-multilabel, unsupervised-clustering)

        Args:
            model : trained model using SKlearn library
            
            X : input matrix (either training set, validation set, or test set)
            
            y_true : target variable vector or matrix (either training set, validation set, or test set)
            
            generate_validation_metrics : boolean (default=True). If True, the method create training and validation set
                                            using sklearn train_test_split, and report metrics for both sets. Metrics
                                            names ending wtih "_tr" represent metrics for training set. Metrics names
                                             ending with "_val" represent metrics for validation set.

            validation_size: float (default=0.2). The percentage is used for test_size parameter of sklearn
                                train_test_split

            status_desc : the status variable that goes into send_metrics method (default value "Reporting Metrics" given)
        """
        # assign attributes
        self.status_desc = status_desc

        # initiate three empty lists for collecting metrics values and names
        self.metrics_value = []
        self.metrics_name = []

        if isinstance(model, sklearn.base.BaseEstimator):
            self.model = model

        # if validation = True, create train and validation sets from dataset provided
        if generate_validation_metrics:
            X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=validation_size,
                                                              random_state=42)

            # report metrics for training and validation set separately
            for x, y, dname in [(X_train, y_train, "_tr"), (X_val, y_val, "_val")]:
                # assign predictors and target variable sets and dataset name
                self.X = x
                self.y_true = y
                self.dataset_name = dname

                # make predictions
                self.y_pred, self.y_score, self.y_pred_proba = self.generate_prediction()
                # populate metrics
                self.populate_metrics()


        # if validation = False
        else:
            self.X = X
            self.y_true = y
            self.dataset_name = ''
            # make predictions
            self.y_pred, self.y_score, self.y_pred_proba = self.generate_prediction()
            # populate metrics
            self.populate_metrics()

        
        # collect metrics names and values in a dictionary
        metrics_dict = dict(zip(self.metrics_name, self.metrics_value))

        # send metrics to xpresso UI
        self.send_metrics(metrics=metrics_dict, status_desc=status_desc)
        logger.info("Metrics Reported")

    def populate_metrics(self):
        # check if the model is sklearn regressor
        if sklearn.base.is_regressor(self.model):
            self.populate_regressor_metrics()

        # check if the model is a sklearn classifier
        elif sklearn.base.is_classifier(self.model):
            self.populate_metrics_classifier()

        # check if the model is a unsupervised clustering algorithm within sklearn.cluster module
        elif self.model.__class__.__name__ in sklearn.cluster.__all__:
            self.populate_metrics_unsupervised_clustering()

    def populate_metrics_unsupervised_clustering(self):

        # collect and calculate metrics that use y_true, y_pred as inputs
        sklearn_metrics = [metrics.adjusted_mutual_info_score,
                             metrics.adjusted_rand_score,
                             metrics.completeness_score,
                             metrics.fowlkes_mallows_score,
                             metrics.homogeneity_score,
                             metrics.mutual_info_score,
                             metrics.normalized_mutual_info_score,
                             metrics.v_measure_score]

        self.calculate_metrics_general(sklearn_metrics)

        # addtional metrics for unsupervised clustering models
        clustering_specific_metrics = [metrics.calinski_harabasz_score,
                                       metrics.davies_bouldin_score,
                                       metrics.silhouette_score]

        # calculate metrics that use X and y_pred as inputs
        for metric in clustering_specific_metrics:
            try:
                self.metrics_value.append(round(metric(self.X, self.y_pred), 4))
                self.metrics_name.append(metric.__name__ + self.dataset_name)
            except Exception as e:
                print(e)
                pass


    def calculate_metrics_general(self, sklearn_metrics):
        # calculate metrics that use y_true and y_pred as inputs
        for metric in sklearn_metrics:
            try:
                self.metrics_value.append(round(metric(self.y_true, self.y_pred), 2))
                self.metrics_name.append(metric.__name__ + self.dataset_name)
            except Exception as e:
                print(e)
                pass

    def generate_prediction(self):
        # Apply trained model to make predictions on given dataset
        y_pred = self.model.predict(self.X)
        # check if the trained model has decision_function
        if hasattr(self.model, "decision_function"):
            y_score = self.model.decision_function(self.X)
        else:
            y_score = None
        # Check if the trained model can predict probability
        if hasattr(self.model, "predict_proba"):
            y_pred_proba = self.model.predict_proba(self.X)
        else:
            y_pred_proba = None
        return y_pred, y_score, y_pred_proba

    def populate_metrics_classifier(self):

        if self.y_score is not None:
            self.populate_metrics_yscore()

        # Calculate metrics that use y_true and y_pred_proba as inputs
        if self.y_pred_proba is not None:
            self.populate_metrics_yproba()

        # check if the model is binary classifier:
        if sklearn.utils.multiclass.unique_labels(self.y_true).size == 2:

            self.populate_metrics_binary()

            # plot roc curve for binary classifier
            self.plot_roc_curve_binary()

            # plot precision_recall_curve for binary classifier
            self.plot_precision_recall_curve_binary()

            # plot confusion matrix for binary or multiclass classification task
            self.plot_confusion_matrix()


        # check if the model is a multiclass or multilabel classifier
        elif sklearn.utils.multiclass.unique_labels(self.y_true).size > 2:

            # for multiclass classification
            if not sklearn.utils.multiclass.is_multilabel(self.y_true):
                self.populate_metrics_multiclass()

                # plot confusion matrix for binary or multiclass classification task
                self.plot_confusion_matrix()


            # for multilabel classification
            elif sklearn.utils.multiclass.is_multilabel(self.y_true):
                self.populate_metrics_multilabel()

                # plot multilabel confusion matrix for all types of classification task
                self.plot_multilabel_confusion_matrix()

            # plot roc curve for multiclass or multilabels
            self.plot_roc_curve_multi()

        # save classification report for all types
        self.save_classification_report()

    def populate_metrics_yproba(self):
        # for binary, multiclass and multilabel
        proba_metrics = [metrics.log_loss]
        for metric in proba_metrics:
            try:
                self.metrics_value.append(round(metric(self.y_true, self.y_pred_proba), 4))
                self.metrics_name.append(metric.__name__ + self.dataset_name)
            except Exception as e:
                print(e)
                pass

    def populate_metrics_binary(self):
        # metrics using y_true and y_pred as inputs
        sklearn_metrics = [metrics.accuracy_score,
                                     metrics.zero_one_loss,
                                     metrics.hamming_loss,
                                     metrics.balanced_accuracy_score,
                                     metrics.f1_score,
                                     metrics.recall_score,
                                     metrics.precision_score,
                                     metrics.average_precision_score,
                                     metrics.jaccard_score,
                                     metrics.matthews_corrcoef]

        self.calculate_metrics_general(sklearn_metrics)


        # calculate metrics that use prediction probabilities as inputs
        if self.y_pred_proba is not None:
            clf_metrics_proba = [metrics.roc_auc_score,
                                 metrics.brier_score_loss]
            # calculate metrics that use y_true and y_pred_proba as inputs
            for metric in clf_metrics_proba:
                try:
                    self.metrics_value.append(round(metric(self.y_true, self.y_pred_proba[:, 1]), 2))
                    self.metrics_name.append(metric.__name__ + self.dataset_name)
                except Exception as e:
                    print(e)
                    pass

        # add additional metrics from confusion matrix
        try:
            tn, fp, fn, tp = metrics.confusion_matrix(self.y_true, self.y_pred).ravel()
            self.metrics_value.extend([self.true_negative_rate(tn, fp),
                                       self.false_positive_rate(fp, tn),
                                       self.false_negative_rate(fn, tp)])
            self.metrics_name.extend(["true_negative_rate" + self.dataset_name,
                                      "false_positive_rate" + self.dataset_name,
                                      "false_negative_rate" + self.dataset_name])
        except Exception as e:
            print(e)
            pass


    # define functions true_negative_rate
    def true_negative_rate(self, tn, fp):
        return round(tn / (tn + fp), 2)

    # define functions false_positive_rate
    def false_positive_rate(self, fp, tn):
        return round(fp / (fp + tn), 2)

    # define functions false_negative_rate
    def false_negative_rate(self, fn, tp):
        return round(fn / (fn + tp), 2)

    def populate_metrics_multiclass(self):

        sklearn_metrics = [metrics.accuracy_score,
                                     metrics.zero_one_loss,
                                     metrics.hamming_loss,
                                     metrics.balanced_accuracy_score,
                                     metrics.matthews_corrcoef]
        self.calculate_metrics_general(sklearn_metrics)


        # averaging metrics applicable to both multiclass
        averaging_metrics = [metrics.f1_score,
                             metrics.recall_score,
                             metrics.precision_score,
                             metrics.jaccard_score]

        for metric in averaging_metrics:
            for avg_method in ["micro", "macro", "weighted"]:
                try:
                    self.metrics_value.append(round(metric(self.y_true, self.y_pred, average=avg_method), 4))
                    self.metrics_name.append(metric.__name__ + "_" + avg_method + self.dataset_name)
                except Exception as e:
                    print(e)
                    pass
        # roc_auc_score
        if self.y_pred_proba is not None:
            for avg_method in ["macro", "weighted"]:
                for config in ["ovr", "ovo"]:
                    try:
                        self.metrics_value.append(round(metrics.roc_auc_score(self.y_true, self.y_pred_proba,
                                                                         average=avg_method,
                                                                         multi_class=config), 4))
                        self.metrics_name.append(metrics.roc_auc_score.__name__ + "_" + avg_method + "_" + config
                                                 + self.dataset_name)

                    except Exception as e:
                        print(e)
                        pass

    def populate_metrics_multilabel(self):
        # collect and calculate metrics that use y_true, y_pred as inputs

        sklearn_metrics = [metrics.accuracy_score,
                                metrics.zero_one_loss,
                                metrics.hamming_loss,
                                metrics.average_precision_score]

        self.calculate_metrics_general(sklearn_metrics)


        # averaging metrics
        averaging_metrics = [metrics.f1_score,
                             metrics.recall_score,
                             metrics.precision_score,
                             metrics.jaccard_score]
        for metric in averaging_metrics:
            for avg_method in ["micro", "macro", "weighted", "samples"]:
                try:
                    self.metrics_value.append(round(metric(self.y_true, self.y_pred, average=avg_method), 4))
                    self.metrics_name.append(metric.__name__ + "_" + avg_method + self.dataset_name)
                except Exception as e:
                    print(e)
                    pass

        #roc_auc_score
        if self.y_pred_proba is not None:
            # reshape prediction probability
            y_pred_proba_t = np.transpose(np.array(self.y_pred_proba)[:, :, 1])

            # roc_auc_score for multilabel
            for avg_method in ["micro", "macro", "weighted", "samples"]:
                try:
                    self.metrics_value.append(round(metrics.roc_auc_score(self.y_true, y_pred_proba_t,
                                                                          average=avg_method), 4))
                    self.metrics_name.append(metrics.roc_auc_score.__name__ + "_" + avg_method + self.dataset_name)

                except Exception as e:
                    print(e)
                    pass

    def populate_metrics_yscore(self):

        # for binary, multiclass and multilabel
        score_metrics = [metrics.hinge_loss]

        # for multilabel, collect ranking metrics
        if sklearn.utils.multiclass.is_multilabel(self.y_true):
            ranking_metrics = [metrics.coverage_error,
                               metrics.label_ranking_average_precision_score,
                               metrics.label_ranking_loss,
                               metrics.ndcg_score]

            score_metrics.append(ranking_metrics)

        for metric in score_metrics:
            try:
                self.metrics_value.append(round(metric(self.y_true, self.y_score), 4))
                self.metrics_name.append(metric.__name__ + self.dataset_name)
            except Exception as e:
                print(e)
                pass


    def plot_precision_recall_curve_binary(self):
        if self.y_score is not None:
            try:
                average_precision = metrics.average_precision_score(self.y_true, self.y_score)
                disp_prec = metrics.plot_precision_recall_curve(self.model, self.X, self.y_true)
                disp_prec.ax_.set_title('Precision-Recall curve' + self.dataset_name +
                                        ' : AP={0:0.2f}'.format(average_precision))
                plt.show()
                xpresso_save_plot("Precision_recall_curve", output_path=MOUNT_PATH,
                                  output_folder="report_metrics/" + self.status_desc)

            except Exception as e:
                print(e)
                pass

    def plot_roc_curve_binary(self):
        if self.y_pred_proba is not None:
            try:
                average_roc_auc_score = metrics.roc_auc_score(self.y_true, self.y_pred_proba[:, 1])
                disp_roc = metrics.plot_roc_curve(self.model, self.X, self.y_true)
                disp_roc.ax_.set_title('ROC curve' + self.dataset_name +
                                       ' : AUC ROC={0:0.2f}'.format(average_roc_auc_score))
                plt.show()
                xpresso_save_plot("ROC_curve" + self.dataset_name, output_path=MOUNT_PATH,
                                  output_folder="report_metrics/" + self.status_desc)
            except Exception as e:
                print(e)
                pass

    def plot_multilabel_confusion_matrix(self):
        try:
            # generate multilabel_confusion_matrix
            cm_arrays = metrics.multilabel_confusion_matrix(self.y_true, self.y_pred)

            fig, axes = plt.subplots(len(cm_arrays), figsize=(6, 10))

            # plot a confusion matrix for each class
            for i in range(len(cm_arrays)):
                df_cm = pd.DataFrame(cm_arrays[i])
                sn.set(font_scale=1.2)  # for label size
                sn.heatmap(df_cm, annot=True,
                           cmap=plt.cm.Blues, ax=axes[i], fmt='g')
                cm_plot_title = metrics.multilabel_confusion_matrix.__name__ + self.dataset_name + "_class_" + str(
                    self.model.classes_[i] )
                axes[i].set_xlabel('Predicted label')
                axes[i].set_ylabel('True label')
                axes[i].set_title(cm_plot_title)

            fig.tight_layout(pad=1.0)
            plt.show()
            xpresso_save_plot(cm_plot_title, output_path=MOUNT_PATH,
                              output_folder="report_metrics/" + self.status_desc)
            logger.info("Multilable Confusion Matrix Saved")
        except Exception as e:
            print(e)
            pass

    def plot_confusion_matrix(self):
        # plot confusion matrix for binary or multiclass classification task
        try:
            cm_disp = metrics.plot_confusion_matrix(self.model, self.X, self.y_true,
                                                    display_labels=self.model.classes_,
                                                    cmap=plt.cm.Blues)
            cm_disp.ax_.set_title("Confusion Matrix" + self.dataset_name)
            plt.show()
            xpresso_save_plot("Confusion_matrix" + self.dataset_name, output_path=MOUNT_PATH,
                              output_folder="report_metrics/" + self.status_desc)
            logger.info("Confusion Matrix Saved")
        except Exception as e:
            print(e)
            pass

    def plot_roc_curve_multi(self):
        # Compute ROC curve and ROC area for multiclass and multilabel classifiers
        if self.y_score is not None:
            try:
                y_true_binarized = label_binarize(self.y_true, classes=self.model.classes_)
                fpr = dict()
                tpr = dict()
                roc_auc = dict()
                for i in range(len(self.model.classes_)):
                    fpr[i], tpr[i], _ = metrics.roc_curve(y_true_binarized[:, i], self.y_score[:, i])
                    roc_auc[i] = metrics.auc(fpr[i], tpr[i])

                # Compute micro-average ROC curve and ROC area
                fpr["micro"], tpr["micro"], _ = metrics.roc_curve(y_true_binarized.ravel(), self.y_score.ravel())
                roc_auc["micro"] = metrics.auc(fpr["micro"], tpr["micro"])

                # First aggregate all false positive rates
                all_fpr = np.unique(np.concatenate([fpr[i] for i in range(len(self.model.classes_))]))

                # Then interpolate all ROC curves at this points
                mean_tpr = np.zeros_like(all_fpr)
                for i in range(len(self.model.classes_)):
                    mean_tpr += np.interp(all_fpr, fpr[i], tpr[i])

                # Finally average it and compute AUC
                mean_tpr /= len(self.model.classes_)

                fpr["macro"] = all_fpr
                tpr["macro"] = mean_tpr
                roc_auc["macro"] = metrics.auc(fpr["macro"], tpr["macro"])

                # Plot all ROC curves
                plt.figure(figsize=(12, 6))
                lw = 2
                plt.plot(fpr["micro"], tpr["micro"],
                         label='micro-average ROC curve (area = {0:0.2f})'
                               ''.format(roc_auc["micro"]),
                         color='deeppink', linestyle=':', linewidth=4)

                plt.plot(fpr["macro"], tpr["macro"],
                         label='macro-average ROC curve (area = {0:0.2f})'
                               ''.format(roc_auc["macro"]),
                         color='navy', linestyle=':', linewidth=4)

                colors = cycle([i for i in mcolors.TABLEAU_COLORS.values()])
                for i, color in zip(range(len(self.model.classes_)), colors):
                    plt.plot(fpr[i], tpr[i], color=color, lw=lw,
                             label='ROC curve of class {0} (area = {1:0.2f})'
                                   ''.format(i, roc_auc[i]))

                plt.plot([0, 1], [0, 1], 'k--', lw=lw)
                plt.xlim([0.0, 1.0])
                plt.ylim([0.0, 1.05])
                plt.xlabel('False Positive Rate')
                plt.ylabel('True Positive Rate')
                roc_plot_title = self.model.__class__.__name__ + '_ROC_curve' + self.dataset_name
                plt.title(roc_plot_title)
                plt.legend(loc="lower right")
                plt.show()
                xpresso_save_plot(roc_plot_title, output_path=MOUNT_PATH,
                                  output_folder="report_metrics/" + self.status_desc)
                logger.info("Multiclass/Multilable ROC Curve Saved")
            except Exception as e:
                print(e)
                pass

    def save_classification_report(self):
        # save classification report for classification task
        try:
            cls_report = metrics.classification_report(self.y_true, self.y_pred,
                                                       output_dict=True)
            cls_report_df = pd.DataFrame(cls_report).transpose()
            # save model to data folder
            pickle.dump(cls_report_df, open(os.path.join(MOUNT_PATH, "report_metrics/classification_report" + self.dataset_name +
                                                         ".pkl"), 'wb'))

            logger.info("Classification Report Saved")
        except Exception as e:
            print(e)
            pass

    def populate_regressor_metrics(self):
        # collect sklearn regression metrics
        sklearn_metrics = [metrics.explained_variance_score,  # Explained variance regression score function
                           metrics.max_error,  # max_error metric calculates the maximum residual error
                           metrics.mean_absolute_error,  # Mean absolute error regression loss
                           metrics.mean_squared_error,  # Mean squared error regression loss
                           metrics.mean_squared_log_error,  # Mean squared logarithmic error regression loss
                           metrics.median_absolute_error,  # Median absolute error regression loss
                           metrics.r2_score,  # R^2 (coefficient of determination) regression score function
                           metrics.mean_poisson_deviance,  # Mean Poisson deviance regression loss.
                           metrics.mean_gamma_deviance,  # Mean Gamma deviance regression loss.
                           metrics.mean_tweedie_deviance] # Mean Tweedie deviance regression loss

        self.calculate_metrics_general(sklearn_metrics)

    @ignore_warnings(category=ConvergenceWarning)
    def plot_validation_curves(self, estimator, X, y,
                               param_name="max_iter",
                               param_range=None,
                               scoring_metrics=None,
                               **kwargs):

        """
            Generate validation plots: the test, train scores vs param_name plot
            While default parameters (param_name) for the plot is max_iter,
             users have the options to plot validation curve for any parameters of the estimator provided.

            Parameters
            ----------
            estimator : object type that implements the "fit" and "predict" methods from SKlearn

            X : array-like, shape (n_samples, n_features) feature matrix

            y : array-like, shape (n_samples) or (n_samples, n_classes) target variable

            param_name : str, any parameter of the estimator the user wants to evaluate
                        default value is max_iter

            param_range : range of parameter values
                        default value is

            scoring_metrics: str or list of str, scoring parameters pre-defined in sklearn, e.g.,"accuracy"
                            see section "The scoring parameter: defining model evaluation rules" within sklearn documentation
                            default for regressor is "neg_mean_squared_error", "neg_root_mean_squared_error"
                            default for classifier is "accuracy", "neg_log_loss"

            groups : groups variable array-like of shape (n_samples,), default=None
                     Group labels for the samples used while splitting the dataset into train/test set.
                     Only used in conjunction with a “Group” cv instance (e.g., GroupKFold).

            **kwargs includes: n_jobs , cv , pre_dispatch , verbose, error_score
            n_jobs :  default None, optional
            cv :  default 5,  optional
            pre_dispatch :  default None, optional
            verbose : int, default=0
            error_score : ‘raise’ or numeric, default=np.nan
        """


        # use default metrics if no values provided to scoring_metrics by end-users
        if scoring_metrics is None:
            if sklearn.base.is_regressor(estimator):
                scoring_metrics = ["neg_mean_absolute_error", "neg_root_mean_squared_error"]
            elif sklearn.base.is_classifier(estimator):
                scoring_metrics = ["accuracy", "neg_log_loss"]

        # if end-users provide only one metrics
        elif type(scoring_metrics) == str:
            scoring_metrics = [scoring_metrics]

        # if no param_range is provided, param_range is list of integers range from 1 to 20 for max_iter
        if param_range is None:
            param_range = list(range(1, 21))

        fig, axes = plt.subplots(len(scoring_metrics), 1, figsize=(6, 8))

        # plot learning curve for each scoring metrics
        for i in range(len(scoring_metrics)):
            try:

                train_scores, test_scores = validation_curve(
                                            estimator=estimator, X=X, y=y,
                                            param_name=param_name, param_range=param_range,
                                            scoring=scoring_metrics[i], **kwargs)


                train_scores_mean = np.mean(train_scores, axis=1)
                train_scores_std = np.std(train_scores, axis=1)
                val_scores_mean = np.mean(test_scores, axis=1)
                val_scores_std = np.std(test_scores, axis=1)

                cv_results_df = pd.DataFrame({param_name: param_range,
                                              "val_scores_mean": val_scores_mean,
                                              "train_scores_mean": train_scores_mean})
                val_score_best = val_scores_mean.max()
                max_iters_best = cv_results_df.loc[cv_results_df.val_scores_mean == val_score_best,
                                                   param_name].values[0]

                # Plot score vs max_iters
                axes[i].grid()
                axes[i].fill_between(param_range,
                                     train_scores_mean - train_scores_std,
                                     train_scores_mean + train_scores_std,
                                     alpha=0.1, color="y")
                axes[i].fill_between(param_range,
                                     val_scores_mean - val_scores_std,
                                     val_scores_mean + val_scores_std,
                                     alpha=0.1, color="g")
                axes[i].plot(param_range,
                             train_scores_mean,
                             'o-', color="y",
                             label="Training score")
                axes[i].plot(param_range,
                             val_scores_mean,
                             'o-', color="g",
                             label="Cross-validation score")
                axes[i].plot(max_iters_best, val_score_best, 'r*', label='Best Param : ' +
                                                                         str(max_iters_best))
                axes[i].set_xlabel(param_name)
                axes[i].set_ylabel(scoring_metrics[i])
                axes[i].set_title("Validation curve :" + estimator.__class__.__name__)
                axes[i].legend(loc="best")

            except Exception as e:
                print(e)
                pass

        fig.tight_layout(pad=3.0)
        plt.show()
        xpresso_save_plot("validation_curve_" + estimator.__class__.__name__, output_path=MOUNT_PATH,
                          output_folder="report_metrics/")
        logger.info("Learning curve plot saved")



    def send_metrics(self, metrics, status_desc ="reporting metrics"):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": status_desc},
                "metric": metrics
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()


    def completed(self, push_exp=True, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===

        # make sure data type is sklearn model
        if isinstance(self.model, sklearn.base.BaseEstimator):
            try:
                # create Output folder
                if not os.path.exists(self.OUTPUT_DIR):
                    os.makedirs(self.OUTPUT_DIR)
                # save model to output folder
                pickle.dump(self.model, open(os.path.join(self.OUTPUT_DIR, self.model.__class__.__name__ + ".pkl"), 'wb'))
            except:
                pass

        try:
            super().completed(push_exp=push_exp, success=success)

        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)


    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)


    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)


    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    # if __name__ == "__main__":
    #     # To run locally. Use following command:
    #     # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py
    #
    #     data_prep = SKLearnComponent()
    #     if len(sys.argv) >= 2:
    #         data_prep.start(run_name=sys.argv[1])
    #     else:
    #         data_prep.start(run_name="")
