"""
This is the implementation of data preparation for sklearn
"""
# import libaries as needed
import sys
import logging
import numpy as np
import pandas as pd
import os
import pickle
from sklearn import metrics
import sklearn
from sklearn import utils
import matplotlib.pyplot as plt
import seaborn as sn
from sklearn.utils._testing import ignore_warnings
from sklearn.exceptions import ConvergenceWarning
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import validation_curve
from sklearn.preprocessing import label_binarize
from itertools import cycle
import matplotlib.colors as mcolors

# Following two imports are required for Xpresso. Do not remove this.
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot

# Define default file path for Xpresso to save plots
MOUNT_PATH = "/data"

__author__ = "### Claire Zhang ###"

# To use the logger please provide the name and log level
#   - name is passed as the project name while generating the logs
#   - level can be DEBUG, INFO, WARNING, ERROR, CRITICAL
logger = XprLogger(name="logistic_reg",
                   level=logging.INFO)


class SKLearnComponent(AbstractPipelineComponent):
    """ Specialized class for pipeline jobs that use SKlearn API for model training and evaluation.
     It is extended from AbstractPipelineComponent, which allows xpresso platform to track and manage the pipeline.

    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self, name=None):

        super().__init__(name)
        """ Initialize all the required constansts and data her """

    def start(self, xpresso_run_name):
        """
        The start method starts the experiment corresponding to the xpresso_run_name
        by calling the superclass start method from the abstract_pipeline_component

        Args:
            xpresso_run_name (str) : Unique identifier of that run instance
        """

        super().start(xpresso_run_name=xpresso_run_name)
        # === Your start code base goes here ===

        # except Exception:
        #     import traceback
        #     traceback.print_exc()
        #     self.completed(success=False)
        # self.completed(success=True)

    def report_metrics(self, model, X, y_true, status_desc="reporting metrics"):
        """
        The report_metrics method collects all metrics available in SKlearn library according to user's model type
        The method takes three required variables
         user's trained model,
         and one calculate and report metrics value back to

        Args:
            model : trained model using SKlearn library
            X : input matrix (either training set, validation set, or test set)
            y_true : target variable vector or matrix (either training set, validation set, or test set)
            status_desc : the status variable that goes into send_metrics method (default value "Reporting Metrics" given)
        """

        # initiate three empty lists for collecting metrics values and names
        metrics_value = []
        metrics_name = []
        sklearn_metrics = []

        # Apply trained model to make predictions on given dataset
        y_pred = model.predict(X)

        # check if the model is sklearn regressor
        if sklearn.base.is_regressor(model):

            # collect sklearn regression metrics
            sklearn_metrics = [metrics.explained_variance_score,  # Explained variance regression score function
                               metrics.max_error,  # max_error metric calculates the maximum residual error
                               metrics.mean_absolute_error,  # Mean absolute error regression loss
                               metrics.mean_squared_error,  # Mean squared error regression loss
                               metrics.mean_squared_log_error,  # Mean squared logarithmic error regression loss
                               metrics.median_absolute_error,  # Median absolute error regression loss
                               metrics.r2_score,  # R^2 (coefficient of determination) regression score function
                               metrics.mean_poisson_deviance,  # Mean Poisson deviance regression loss.
                               metrics.mean_gamma_deviance,  # Mean Gamma deviance regression loss.
                               metrics.mean_tweedie_deviance]  # Mean Tweedie deviance regression loss


        # check if the model is a sklearn classifier
        elif sklearn.base.is_classifier(model):

            # collect metrics that apply to all types of classification task (binary, multiclass and mulitlabel)
            sklearn_metrics.extend([metrics.accuracy_score,
                                    metrics.zero_one_loss,
                                    metrics.hamming_loss])

            # check if the trained model has decision_function
            if hasattr(model, "decision_function"):
                y_score = model.decision_function(X)

            # Check if the trained model can predict probability
            if hasattr(model, "predict_proba"):
                y_pred_proba = model.predict_proba(X)
                # reshape predict probability matrix for multilabel


            # Calculate metrics that use y_true and y_score as inputs
            score_metrics = [metrics.hinge_loss]
            for metric in score_metrics:
                try:
                    metrics_value.append(round(metric(y_true, y_score), 4))
                    metrics_name.append(metric.__name__)
                except Exception as e:
                    print(e)
                    pass


            # Calculate metrics that use y_true and y_pred_proba as inputs
            proba_metrics = [metrics.log_loss]
            for metric in proba_metrics:
                try:
                    metrics_value.append(round(metric(y_true, y_pred_proba), 4))
                    metrics_name.append(metric.__name__)

                except Exception as e:
                    print(e)
                    pass

            # check if the model is binary classifier:
            if utils.multiclass.unique_labels(y_true).size == 2:

                # add additional metrics for binary classifiers
                sklearn_metrics.extend([metrics.balanced_accuracy_score,
                                        metrics.f1_score,
                                        metrics.recall_score,
                                        metrics.precision_score,
                                        metrics.average_precision_score,
                                        metrics.jaccard_score,
                                        metrics.matthews_corrcoef])

                # collect metrics that use prediction probabilities as inputs for binary classifiers
                clf_metrics_proba = [metrics.roc_auc_score,
                                     metrics.brier_score_loss]

                # calculate metrics that use y_true and y_pred_proba as inputs
                for metric in clf_metrics_proba:
                    try:
                        metrics_value.append(round(metric(y_true, y_pred_proba[:, 1]), 2))
                        metrics_name.append(metric.__name__)

                    except Exception as e:
                        print(e)
                        pass

                # define functions
                def true_negative_rate(tn, fp):
                    return round(tn / (tn + fp), 2)

                def false_positive_rate(fp, tn):
                    return round(fp / (fp + tn), 2)

                def false_negative_rate(fn, tp):
                    return round(fn / (fn + tp), 2)

                # add additional metrics from confusion matrix
                try:
                    tn, fp, fn, tp = metrics.confusion_matrix(y_true, y_pred).ravel()
                    metrics_value.extend([true_negative_rate(tn, fp),
                                          false_positive_rate(fp, tn),
                                          false_negative_rate(fn, tp)])
                    metrics_name.extend(["true_negative_rate",
                                         "false_positive_rate",
                                         "false_negative_rate"])

                except Exception as e:
                    print(e)
                    pass

                try:
                    #plot roc curve
                    average_precision = metrics.average_precision_score(y_true, y_score)
                    average_roc_auc_score = metrics.roc_auc_score(y_true, y_pred_proba)
                    disp_roc = metrics.plot_roc_curve(model, X, y_true)
                    disp_roc.ax_.set_title('ROC curve: '
                                              'AP={0:0.2f}'.format(average_roc_auc_score))
                    # plot precision_recall_curve
                    disp_prec = metrics.plot_precision_recall_curve(model, X, y_true)
                    disp_prec.ax_.set_title('Precision-Recall curve: '
                                            'AP={0:0.2f}'.format(average_precision))
                    plt.show()
                    xpresso_save_plot("roc_pr_curve", output_path=MOUNT_PATH,
                                      output_folder="report_metrics/" + status_desc)

                except Exception as e:
                    print(e)
                    pass


            # check if the model is a multiclass or multilabel classifier
            elif utils.multiclass.unique_labels(y_true).size > 2:

                # metrics applicable to both multiclass and multilabel
                averaging_metrics = [metrics.f1_score,
                                      metrics.recall_score,
                                      metrics.precision_score,
                                      metrics.jaccard_score]


                # for multiclass classification
                if not utils.multiclass.is_multilabel(y_true):

                    sklearn_metrics.extend([metrics.balanced_accuracy_score,
                                            metrics.matthews_corrcoef])

                    for metric in averaging_metrics:
                        for avg_method in ["micro", "macro", "weighted"]:
                            try:
                                metrics_value.append(round(metric(y_true, y_pred, average=avg_method), 4))
                                metrics_name.append(metric.__name__ + "_" + avg_method)
                            except Exception as e:
                                print(e)
                                pass

                    # roc_auc_score
                    for avg_method in ["macro", "weighted"]:
                        for config in ["ovr", "ovo"]:
                            try:
                                metrics_value.append(round(metrics.roc_auc_score(y_true, y_pred_proba,
                                                                                 average=avg_method,
                                                                                 multi_class=config), 4))
                                metrics_name.append(metrics.roc_auc_score.__name__ + "_" + avg_method + "_" + config)

                            except Exception as e:
                                print(e)
                                pass

                # for multilabel classification
                elif utils.multiclass.is_multilabel(y_true):
                    sklearn_metrics.extend([metrics.average_precision_score])

                    for metric in averaging_metrics:
                        for avg_method in ["micro", "macro", "weighted", "samples"]:
                            try:
                                metrics_value.append(round(metric(y_true, y_pred, average=avg_method), 4))
                                metrics_name.append(metric.__name__ + "_" + avg_method)

                            except Exception as e:
                                print(e)
                                pass

                    # reshape prediction probability for roc_auc_score calculation
                    y_pred_proba_t = np.transpose(np.array(y_pred_proba)[:, :, 1])

                    # roc_auc_score for multilabel
                    for avg_method in ["micro", "macro", "weighted", "samples"]:
                        try:
                            metrics_value.append(round(metrics.roc_auc_score(y_true, y_pred_proba_t,
                                                                             average=avg_method), 4))
                            metrics_name.append(metrics.roc_auc_score.__name__ + "_" + avg_method)

                        except Exception as e:
                            print(e)
                            pass

                    #Multilabel ranking metrics
                    multilabel_ranking_metrics = [metrics.coverage_error,
                                                  metrics.label_ranking_average_precision_score,
                                                  metrics.label_ranking_loss,
                                                  metrics.ndcg_score]

                    for metric in multilabel_ranking_metrics:
                        try:
                            metrics_value.append(round(metric(y_true, y_score), 4))
                            metrics_name.append(metric.__name__)

                        except Exception as e:
                            print(e)
                            pass

                # Compute ROC curve and ROC area for multiclass and multilabel classifiers
                try:
                    y_true_binarized = label_binarize(y_true, classes=model.classes_)
                    fpr = dict()
                    tpr = dict()
                    roc_auc = dict()
                    for i in range(len(model.classes_)):
                        fpr[i], tpr[i], _ = metrics.roc_curve(y_true_binarized[:, i], y_score[:, i])
                        roc_auc[i] = metrics.auc(fpr[i], tpr[i])

                    # Compute micro-average ROC curve and ROC area
                    fpr["micro"], tpr["micro"], _ = metrics.roc_curve(y_true_binarized.ravel(), y_score.ravel())
                    roc_auc["micro"] = metrics.auc(fpr["micro"], tpr["micro"])

                    # First aggregate all false positive rates
                    all_fpr = np.unique(np.concatenate([fpr[i] for i in range(len(model.classes_))]))

                    # Then interpolate all ROC curves at this points
                    mean_tpr = np.zeros_like(all_fpr)
                    for i in range(len(model.classes_)):
                        mean_tpr += np.interp(all_fpr, fpr[i], tpr[i])

                    # Finally average it and compute AUC
                    mean_tpr /= len(model.classes_)

                    fpr["macro"] = all_fpr
                    tpr["macro"] = mean_tpr
                    roc_auc["macro"] = metrics.auc(fpr["macro"], tpr["macro"])

                    # Plot all ROC curves
                    plt.figure(figsize=(12, 6))
                    lw = 2
                    plt.plot(fpr["micro"], tpr["micro"],
                             label='micro-average ROC curve (area = {0:0.2f})'
                                   ''.format(roc_auc["micro"]),
                             color='deeppink', linestyle=':', linewidth=4)

                    plt.plot(fpr["macro"], tpr["macro"],
                             label='macro-average ROC curve (area = {0:0.2f})'
                                   ''.format(roc_auc["macro"]),
                             color='navy', linestyle=':', linewidth=4)

                    colors = cycle([i for i in mcolors.TABLEAU_COLORS.values()])
                    for i, color in zip(range(len(model.classes_)), colors):
                        plt.plot(fpr[i], tpr[i], color=color, lw=lw,
                                 label='ROC curve of class {0} (area = {1:0.2f})'
                                       ''.format(i, roc_auc[i]))

                    plt.plot([0, 1], [0, 1], 'k--', lw=lw)
                    plt.xlim([0.0, 1.0])
                    plt.ylim([0.0, 1.05])
                    plt.xlabel('False Positive Rate')
                    plt.ylabel('True Positive Rate')
                    roc_plot_title = model.__class__.__name__ + '_ROC_curve'
                    plt.title(roc_plot_title)
                    plt.legend(loc="lower right")
                    plt.show()
                    xpresso_save_plot(roc_plot_title, output_path=MOUNT_PATH,
                                      output_folder="report_metrics/" + status_desc)
                    logger.info("Multiclass/Multilable ROC Curve Saved")
                except Exception as e:
                    print(e)
                    pass

            # save classification report for classification task
            try:
                cls_report = metrics.classification_report(y_true, y_pred,
                                                           output_dict=True)
                cls_report_df = pd.DataFrame(cls_report).transpose()
                # save model to data folder
                pickle.dump(cls_report_df, open(os.path.join(MOUNT_PATH, "report_metrics/classification_report.pkl"), 'wb'))

                logger.info("Classification Report Saved")
            except Exception as e:
                print(e)
                pass

            # plot confusion matrix for binary or multiclass classification task
            try:
                cm_disp = metrics.plot_confusion_matrix(model, X, y_true,
                                                        display_labels=model.classes_,
                                                        cmap=plt.cm.Blues)
                cm_disp.ax_.set_title("Confusion Matrix")
                plt.show()
                xpresso_save_plot("Confusion_matrix", output_path=MOUNT_PATH,
                                  output_folder="report_metrics/" + status_desc)
                logger.info("Confusion Matrix Saved")
            except Exception as e:
                print(e)
                pass


            # plot multilabel confusion matrix for all types of classification task
            try:
                # generate multilabel_confusion_matrix
                cm_arrays = metrics.multilabel_confusion_matrix(y_true, y_pred)

                fig, axes = plt.subplots(len(cm_arrays), figsize=(6, 10))

                # plot a confusion matrix for each class
                for i in range(len(cm_arrays)):
                    df_cm = pd.DataFrame(cm_arrays[i])
                    sn.set(font_scale=1.2)  # for label size
                    sn.heatmap(df_cm, annot=True,
                               cmap=plt.cm.Blues, ax=axes[i], fmt='g')
                    cm_plot_title = metrics.multilabel_confusion_matrix.__name__ + "_class_" + str(
                                    model.classes_[i])
                    axes[i].set_xlabel('Predicted label')
                    axes[i].set_ylabel('True label')
                    axes[i].set_title(cm_plot_title)

                fig.tight_layout(pad=1.0)
                plt.show()
                xpresso_save_plot(cm_plot_title, output_path=MOUNT_PATH,
                                  output_folder="report_metrics/" + status_desc)
                logger.info("Multilable Confusion Matrix Saved")
            except Exception as e:
                print(e)
                pass


        # check if the model is a unsupervised clustering algorithm within sklearn.cluster module
        elif model.__class__.__name__ in sklearn.cluster.__all__:
            sklearn_metrics = [metrics.adjusted_mutual_info_score,
                               metrics.adjusted_rand_score,
                               metrics.completeness_score,
                               metrics.fowlkes_mallows_score,
                               metrics.homogeneity_score,
                               metrics.mutual_info_score,
                               metrics.normalized_mutual_info_score,
                               metrics.v_measure_score]

            clustering_metrics = [metrics.calinski_harabasz_score,
                                  metrics.davies_bouldin_score,
                                  metrics.silhouette_score]


            # calculate metrics that use X and y_pred_proba as inputs
            for metric in clustering_metrics:
                try:
                    metrics_value.append(round(metric(X, y_pred), 4))
                    metrics_name.append(metric.__name__)
                except Exception as e:
                    print(e)
                    pass


        # calculate metrics that use y_true and y_pred as inputs
        for metric in sklearn_metrics:
            try:
                metrics_value.append(round(metric(y_true, y_pred), 2))
                metrics_name.append(metric.__name__)
            except Exception as e:
                print(e)
                pass

        # collect metrics names and values in a dictionary
        metrics_dict = dict(zip(metrics_name, metrics_value))

        # send metrics to xpresso UI
        self.send_metrics(metrics=metrics_dict, status_desc=status_desc)
        logger.info("Metrics Reported")

    @ignore_warnings(category=ConvergenceWarning)
    def plot_validation_curves(self, estimator, X, y, max_epoch,
                               param_name="max_iter",
                               scoring_metrics=None,
                               groups=None, cv=5,
                               n_jobs=None, pre_dispatch='all',
                               verbose=0, error_score=np.nan):

        """
            Generate validation plots: the test, train scores vs param_name plot
            While default parameters (param_name) for the plot is max_iter,
             users have the options to plot validation curve for any parameters of the estimator provided.

            Parameters
            ----------
            estimator : object type that implements the "fit" and "predict" methods from SKlearn

            X : array-like, shape (n_samples, n_features) feature matrix

            y : array-like, shape (n_samples) or (n_samples, n_classes) target variable

            max_epoch : maximum number of epochs to learn (inputs for max_iter)

            param_name : str, any parameter of the estimator the user wants to

            scoring_metrics: str or list of str, scoring parameters pre-defined in sklearn, e.g.,"accuracy"
                            see section "The scoring parameter: defining model evaluation rules" within sklearn documentation
                            default for regressor is "neg_mean_squared_error", "neg_root_mean_squared_error"
                            default for classifier is "accuracy", "neg_log_loss"

            groups : groups variable array-like of shape (n_samples,), default=None
                     Group labels for the samples used while splitting the dataset into train/test set.
                     Only used in conjunction with a “Group” cv instance (e.g., GroupKFold).

            n_jobs :  default None, optional
            cv :  default 5,  optional
            pre_dispatch :  default None, optional
            verboseint, default=0
            error_score‘raise’ or numeric, default=np.nan
        """


        # use default metrics if no values provided to scoring_metrics by end-users
        if scoring_metrics is None:
            if sklearn.base.is_regressor(estimator):
                scoring_metrics = ["neg_mean_absolute_error", "neg_root_mean_squared_error"]
            elif sklearn.base.is_classifier(estimator):
                scoring_metrics = ["accuracy", "neg_log_loss"]

        # if end-users provide only one metrics
        elif type(scoring_metrics) == str:
            scoring_metrics = [scoring_metrics]

        fig, axes = plt.subplots(len(scoring_metrics), 1, figsize=(6, 8))

        # plot learning curve for each scoring metrics
        for i in range(len(scoring_metrics)):
            try:
                param_range = list(range(1, max_epoch + 1))

                train_scores, test_scores = validation_curve(
                                            estimator=estimator, X=X, y=y,
                                            param_name=param_name, param_range=param_range,
                                            scoring=scoring_metrics[i],
                                            groups=groups, cv=cv,
                                            n_jobs=n_jobs, pre_dispatch=pre_dispatch,
                                            verbose=verbose, error_score=error_score)

                train_scores_mean = np.mean(train_scores, axis=1)
                train_scores_std = np.std(train_scores, axis=1)
                val_scores_mean = np.mean(test_scores, axis=1)
                val_scores_std = np.std(test_scores, axis=1)

                cv_results_df = pd.DataFrame({param_name: param_range,
                                              "val_scores_mean": val_scores_mean,
                                              "train_scores_mean": train_scores_mean})
                val_score_best = val_scores_mean.max()
                max_iters_best = cv_results_df.loc[cv_results_df.val_scores_mean == val_score_best,
                                                   param_name].values[0]

                # Plot score vs max_iters
                axes[i].grid()
                axes[i].fill_between(param_range,
                                     train_scores_mean - train_scores_std,
                                     train_scores_mean + train_scores_std,
                                     alpha=0.1, color="y")
                axes[i].fill_between(param_range,
                                     val_scores_mean - val_scores_std,
                                     val_scores_mean + val_scores_std,
                                     alpha=0.1, color="g")
                axes[i].plot(param_range,
                             train_scores_mean,
                             'o-', color="y",
                             label="Training score")
                axes[i].plot(param_range,
                             val_scores_mean,
                             'o-', color="g",
                             label="Cross-validation score")
                axes[i].plot(max_iters_best, val_score_best, 'r*', label='Best Param : ' +
                                                                         str(max_iters_best))
                axes[i].set_xlabel(param_name)
                axes[i].set_ylabel(scoring_metrics[i])
                axes[i].set_title("Validation curve :" + estimator.__class__.__name__)
                axes[i].legend(loc="best")

            except Exception as e:
                print(e)
                pass

        fig.tight_layout(pad=3.0)
        plt.show()
        xpresso_save_plot("validation_curve_" + estimator.__class__.__name__, output_path=MOUNT_PATH,
                          output_folder="report_metrics/")
        logger.info("Learning curve plot saved")



    def send_metrics(self, metrics, status_desc ="reporting metrics"):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": status_desc},
                "metric": metrics
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()


    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        try:
            super().completed(push_exp=push_exp, success=success)
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)


    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)


    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)


    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    # if __name__ == "__main__":
    #     # To run locally. Use following command:
    #     # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py
    #
    #     data_prep = SKLearnComponent()
    #     if len(sys.argv) >= 2:
    #         data_prep.start(run_name=sys.argv[1])
    #     else:
    #         data_prep.start(run_name="")
