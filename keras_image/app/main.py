"""
This is the implementation of data preparation for sklearn
"""
from keras_component import KerasComponent
import sys
import logging
import os
import pickle
import pandas as pd
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.datasets import mnist
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Flatten
from tensorflow.keras.layers import Conv2D, MaxPooling2D
from tensorflow.keras import backend as K


MOUNT_PATH ="/data"
PICKLE_PATH ="/data"

# Following two imports are required for Xpresso. Do not remove this.
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "### Author ###"

# To use the logger please provide the name and log level
#   - name is passed as the project name while generating the logs
#   - level can be DEBUG, INFO, WARNING, ERROR, CRITICAL
logger = XprLogger(name="keras_binary",
                   level=logging.INFO)


class KerasBinary(KerasComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self):
        super().__init__(name="KerasBinary")
        """ Initialize all the required constansts and data her """
        # self.train_x = pickle.load(open(f"{PICKLE_PATH}/train_x_trans.pkl", "rb"))
        # self.train_y = pickle.load(open(f"{PICKLE_PATH}/labels_train.pkl", "rb"))
        # self.test_x = pickle.load(open(f"{PICKLE_PATH}/test_x_trans.pkl", "rb"))
        # self.test_y = pickle.load(open(f"{PICKLE_PATH}/labels_test.pkl", "rb"))
        # self.test_set = pickle.load(open(f"{PICKLE_PATH}/test_set.pkl", "rb"))
        # self.train_set = pickle.load(open(f"{PICKLE_PATH}/train_set.pkl", "rb"))
        #
        # self.model = None

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        try:
            super().start(xpresso_run_name=run_name)
            # === Your start code base goes here ===

            # The codes below adopted from
            # https://github.com/keras-team/keras/blob/master/examples/mnist_cnn.py

            batch_size = 128
            num_classes = 10
            epochs = 1

            # input image dimensions
            img_rows, img_cols = 28, 28

            # the data, split between train and test sets
            (x_train, y_train), (x_test, y_test) = mnist.load_data()

            if K.image_data_format() == 'channels_first':
                x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
                x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
                input_shape = (1, img_rows, img_cols)
            else:
                x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
                x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
                input_shape = (img_rows, img_cols, 1)

            x_train = x_train.astype('float32')
            x_test = x_test.astype('float32')
            x_train /= 255
            x_test /= 255
            print('x_train shape:', x_train.shape)
            print(x_train.shape[0], 'train samples')
            print(x_test.shape[0], 'test samples')

            # convert class vectors to binary class matrices
            y_train = keras.utils.to_categorical(y_train, num_classes)
            y_test = keras.utils.to_categorical(y_test, num_classes)

            self.model = Sequential()
            self.model.add(Conv2D(32, kernel_size=(3, 3),
                             activation='relu',
                             input_shape=input_shape))
            self.model.add(Conv2D(64, (3, 3), activation='relu'))
            self.model.add(MaxPooling2D(pool_size=(2, 2)))
            self.model.add(Dropout(0.25))
            self.model.add(Flatten())
            self.model.add(Dense(128, activation='relu'))
            self.model.add(Dropout(0.5))
            self.model.add(Dense(num_classes, activation='softmax'))

            self.model.compile(loss=keras.losses.categorical_crossentropy,
                          optimizer=keras.optimizers.Adadelta(),
                          metrics=['accuracy'])

            hist=self.model.fit(x_train[:20], y_train[:20],
                      batch_size=batch_size,
                      epochs=epochs,
                      verbose=1,
                      validation_data=(x_test, y_test))
            score = self.model.evaluate(x_test, y_test, verbose=0)
            print('Test loss:', score[0])
            print('Test accuracy:', score[1])

            # print(self.model.evaluate(self.train_x, self.train_y))
            # print(self.model.evaluate(self.test_x, self.test_y))

            self.report_metrics(self.model, x_train, y_train)
            #self.report_metrics(self.model, self.test_x, self.test_y)
            self.plot_learning_curve(hist)

            # create SHAP plots
            self.explain_shap(self.model, x_train)





        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed(success=True)

    # def send_metrics(self):
    #     """ It is called to report intermediate status. It reports status and
    #     metrics back to the xpresso.ai controller through the report_status
    #     method of the superclass. The Controller stores any metrics reported in
    #     a database, and makes these available for comparison. It needs the
    #     following format:
    #     - status:
    #        - status - <single word description>
    #     - metric:
    #        - Key-Value - Of the metrics that needs to be tracked and visualized
    #                      realtime. This could be data size, accuracy, loss etc.
    #     """
    #     try:
    #         report_status = {
    #             "status": {"status": "data_preparation"},
    #             "metric": {"metric_key": 1}
    #         }
    #         self.report_status(status=report_status)
    #     except Exception:
    #         import traceback
    #         traceback.print_exc()
    #
    # def completed(self, push_exp=False, success=True):
    #     """
    #     This is the completed method. It stores the output data files on the
    #     file system, and then calls the superclass completed method, which notes
    #     the fact that the component has completed processing, along with the end time.
    #
    #     User must need to call super completed method at the end of the method
    #     Args:
    #         push_exp: Whether to push the data present in the output folder
    #            to the versioning system. This is required once training is
    #            completed and model needs to be versioned
    #         success: Use to handle failure cases
    #
    #     """
    #     # === Your start code base goes here ===
    #     try:
    #         super().completed(push_exp=push_exp, success=success)
    #     except Exception:
    #         import traceback
    #         traceback.print_exc()
    #         sys.exit(1)

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py

    data_prep = KerasBinary()
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
