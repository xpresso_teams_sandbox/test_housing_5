"""
This is the implementation of data preparation for sklearn
"""

from lightgbm_component import LightGBMComponent
import sys
import logging
import os
import pickle
import pandas as pd
import lightgbm as lgb


PICKLE_PATH = "/data"
MOUNT_PATH = "/data"

# Following two imports are required for Xpresso. Do not remove this.
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "### Author ###"

# To use the logger please provide the name and log level
#   - name is passed as the project name while generating the logs
#   - level can be DEBUG, INFO, WARNING, ERROR, CRITICAL
logger = XprLogger(name="lgbm_booster_bi",
                   level=logging.INFO)


class LgbmBoosterBi(LightGBMComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self):
        super().__init__(name="LgbmBoosterBi")
        """ Initialize all the required constants and data here """
        self.train_x = pickle.load(open(f"{PICKLE_PATH}/train_x_trans.pkl", "rb"))
        self.train_y = pickle.load(open(f"{PICKLE_PATH}/labels_train.pkl", "rb"))
        self.test_x = pickle.load(open(f"{PICKLE_PATH}/test_x_trans.pkl", "rb"))
        self.test_y = pickle.load(open(f"{PICKLE_PATH}/labels_test.pkl", "rb"))
        self.test_set = pickle.load(open(f"{PICKLE_PATH}/test_set.pkl", "rb"))
        self.train_set = pickle.load(open(f"{PICKLE_PATH}/train_set.pkl", "rb"))

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        try:
            super().start(xpresso_run_name=run_name)
            # === Your start code base goes here ===

            ### $xpr_param_import_start
            self.train_set["price_high_low"] = pd.qcut(self.train_set.median_house_value, q=[0, 0.5, 1])
            self.train_y = self.train_set["price_high_low"].astype('category').cat.codes

            self.test_set["price_high_low"] = pd.qcut(self.test_set.median_house_value, q=[0, 0.5, 1])
            self.test_y = self.test_set["price_high_low"].astype('category').cat.codes

            lgb_train = lgb.Dataset(self.train_x, self.train_y,
                                    categorical_feature=[8])
            lgb_eval = lgb.Dataset(self.test_x, self.test_y, reference=lgb_train,
                                   categorical_feature=[8])

            params = {
                'boosting_type': 'gbdt',
                'objective': 'binary',
                'metric': ['auc', 'binary_logloss'],
                'num_leaves': 31,
                'learning_rate': 0.05,
                'feature_fraction': 0.9,
                'bagging_fraction': 0.8,
                'bagging_freq': 5,
                'verbose': 0}

            evals_result = {}
            self.model = lgb.train(params,
                            lgb_train,
                            num_boost_round=1000,
                            evals_result=evals_result,
                            valid_sets=lgb_eval,  # eval training data
                            early_stopping_rounds=5)

            # Apply model to predict training set and report metrics
            self.report_metrics(self.model, self.train_x, self.train_y, generate_validation_metrics=False)

            # create shap plots - model explanability
            self.explain_shap(self.model, self.train_x[:100])



        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed(success=True)

    # def send_metrics(self):
    #     """ It is called to report intermediate status. It reports status and
    #     metrics back to the xpresso.ai controller through the report_status
    #     method of the superclass. The Controller stores any metrics reported in
    #     a database, and makes these available for comparison. It needs the
    #     following format:
    #     - status:
    #        - status - <single word description>
    #     - metric:
    #        - Key-Value - Of the metrics that needs to be tracked and visualized
    #                      realtime. This could be data size, accuracy, loss etc.
    #     """
    #     try:
    #         report_status = {
    #             "status": {"status": "data_preparation"},
    #             "metric": {"metric_key": 1}
    #         }
    #         self.report_status(status=report_status)
    #     except Exception:
    #         import traceback
    #         traceback.print_exc()

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        try:
            super().completed(push_exp=push_exp, success=success)
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py

    data_prep = LgbmBoosterBi()
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
