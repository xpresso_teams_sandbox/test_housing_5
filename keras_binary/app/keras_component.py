"""
This is the implementation of data preparation for sklearn
"""
# import system libraries
import sys
import logging
import os
import types

import shap
shap.initjs()

# import common python libraries
import re
import pickle
import random
import numpy as np
import pandas as pd
import itertools
from itertools import cycle
from itertools import combinations


# import sklearn libraries
import sklearn
from sklearn.preprocessing import label_binarize
from sklearn.utils._testing import ignore_warnings
from sklearn.model_selection import train_test_split
from sklearn.utils.multiclass import type_of_target


# libraries for plotting
import matplotlib.pyplot as plt
import seaborn as sn
import matplotlib.colors as mcolors
from matplotlib import rcParams

# import tensorflow libraries
import tensorflow as tf
from tensorflow.keras import metrics
from tensorflow.keras import losses

# Following two imports are required for Xpresso. Do not remove this.
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot

# Define default file path for Xpresso to save plots
MOUNT_PATH = "/data"
PICKLE_PATH = "/data"



__author__ = "### Claire Zhang ###"

# To use the logger please provide the name and log level
#   - name is passed as the project name while generating the logs
#   - level can be DEBUG, INFO, WARNING, ERROR, CRITICAL
logger = XprLogger(name="logistic_reg",
                   level=logging.INFO)


class KerasComponent(AbstractPipelineComponent):
    """ Specialized class for pipeline jobs that use SKlearn API for model training and evaluation.
     It is extended from AbstractPipelineComponent, which allows xpresso platform to track and manage the pipeline.

    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self, name=None):

        super().__init__(name)
        """ Initialize all the required constansts and data her """

    def start(self, xpresso_run_name):
        """
        The start method starts the experiment corresponding to the xpresso_run_name
        by calling the superclass start method from the abstract_pipeline_component

        Args:
            xpresso_run_name (str) : Unique identifier of that run instance
        """

        super().start(xpresso_run_name=xpresso_run_name)
        # === Your start code base goes here ===

        # except Exception:
        #     import traceback
        #     traceback.print_exc()
        #     self.completed(success=False)
        # self.completed(success=True)

    def report_metrics(self, model, X, y,
                       is_classifier=None,
                       pred_thld=0.5,
                       generate_validation_metrics=True,
                       validation_size=0.2,
                       status_desc="reporting metrics"):
        """
         The method automatically detects user's model type (xgboost.XGBRegressor, xgboost.XGBClassifier) and
         collects, calculates and reports all metrics available in SKlearn library

        Args:
            model : Keras model object
                    Trained Keras classification or regression model using Keras API (see https://keras.io)

            X : array_like or sparse matrix of shape (n_samples, n_features)
                Feature matrix consists of independent variables (either training set, validation set, or test set)

            y : array-like of shape (n_samples,) or (n_samples, n_classes)
                Target variable vector or matrix (either training set, validation set, or test set)

            is_classifier : boolean (default = None)
                          An indicator whether the model is a classification model as opposite to a regression model.
                          If no value is given,the model type will be inferred from the model object or the data type of
                           target variable (y)


            pred_thld : float (default = 0.5)
                        Prediction threshold used for determining the prediction label for classification model if the
                        model outputs prediction probability

            generate_validation_metrics : boolean (default=True)
                                          An indicator of whether to create a validation set and report validation
                                          metrics.
                                          If True, the method creates training and validation set from X using sklearn
                                          train_test_split, and report metrics for both sets. Metrics names ending with
                                          “(Tr)” represent metrics for the training set. Metrics names ending with
                                          “(Val)” represent metrics for the validation set.
                                          If False, one set of metrics will be computed on the entire X dataset.


            validation_size: float (default=0.2)
                            Proportional of X dataset to create a validation set (used only when
                            generate_validation_metrics is True)

            status_desc : string (default = "Reporting Metrics")
                          Any description that users want to provide, such as name of the dataset that is being sent to
                           report metrics method
        """

        # datatype check : if the model is a tensorflow.keras model
        if not isinstance(model, tf.keras.Model):
            return
        self.model = model

        # assign attributes
        self.status_desc = status_desc
        self.is_classifier = is_classifier
        self.is_regressor = None
        self.pred_thld = pred_thld

        # identify model type (regressor or classifier) based on user's input,
        # model loss or data type of the target variable
        self.identify_model_type()

        # initiate two empty lists for collecting metrics values and names
        self.metrics_value = []
        self.metrics_name = []

        # if validation = True, create train and validation sets from X,y dataset provided
        if generate_validation_metrics:
            try:
                X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=validation_size,
                                                                  random_state=42)

                # create training and validation set
                for x, y, dname in [(X_train, y_train, " (Tr)"), (X_val, y_val, " (Val)")]:
                    # make prediction and report metrics
                    self.predict_and_evaluate(x, y, dname)


            except Exception:
                import traceback
                traceback.print_exc()
                generate_validation_metrics = False

        # if validation = False
        if not generate_validation_metrics:
            dname = ""
            # make prediction and report metrics
            self.predict_and_evaluate(X, y, dname)

        # convert metrics value to datatype float64
        self.metrics_value_re = [np.float64(values) for values in self.metrics_value]

        # collect metrics names and values in a dictionary
        self.metrics_dict = dict(zip(self.metrics_name, self.metrics_value_re))

        # send metrics to xpresso UI
        self.send_metrics(metrics=self.metrics_dict, status_desc=self.status_desc)

        logger.info("Metrics Reported")

    def predict_and_evaluate(self, x, y, dname):
        """

        Args:
            x: array_like or sparse matrix, shape (n_samples, n_features)
                input matrix (either training set, validation set, or test set)
            y: array-like of shape (n_samples,) or (n_samples, n_classes)
                target variable vector or matrix (either training set, validation set, or test set)
            dname: string
                 A label indicating the dataset name, automaticall assigned by KerasComponent.
                The value will be "tr" and "val", indicating training set, validation set, if generate_validation_metrics
                 is True. Otherwise, the value will be "" if generate_validation_metrics is False or the data type is
                 not able to be split into training and validation set with sklearn train_test_split.

        Returns: metrics values(list) and metrics names(list)

        """
        # assign input values
        self.X = x
        # assign target variable
        self.y_true = y
        # assign dataset name
        self.dataset_name = dname

        # make predictions
        self.generate_prediction()

        # populate metrics
        self.populate_metrics()

    def identify_model_type(self):
        """
        Determine the model is a regressor or classifier.
        If the user provides True/False to is_regressor parameter, apply user's input. If not, first infer model type
        based on the model loss used in training . If model loss is not clear, then use Sklearn type_of_target
        method to identify data type of target variable.

        """

        regression_losses = ["mean_squared_error", "mse",
                             "mean_absolute_error", "mae",
                             "mean_absolute_percentage_error", "mape",
                             "mean_squared_logarithmic_error", "msle",
                             "cosine_similarity", "cosine",
                             "huber_loss",
                             "log_cosh"]

        probabilistic_losses = ["binary_crossentropy",
                                "categorical_crossentropy",
                                "sparse_categorical_crossentropy",
                                "poisson",
                                "KLDivergence",
                                "hinge",
                                "squared_hinge",
                                "categorical_hinge"]

        # if user provides a value ( True or False) to is_classifier parameter
        if type(self.is_classifier) == bool:
            self.is_classifier = self.is_classifier
            self.is_regressor = not self.is_classifier

        # determine model type based on model loss
        elif self.model.loss:
            if isinstance(self.model.loss, str):
                self.model_loss = self.model.loss

            elif isinstance(self.model.loss, types.FunctionType):
                self.model_loss = self.model.loss.__name__

            elif isinstance(self.model.loss, tf.keras.losses.Loss):
                self.model_loss = self.model.loss.__class__.__name__

            self.is_regressor = self.model_loss in regression_losses
            self.is_classifier = self.model_loss in probabilistic_losses

            # if model loss is unclear, infer model type based on type of target variable
        else:
            target_type_classifier = ["binary", "multiclass", "multiclass-multioutput",
                                      "multilabel-indicator"]

            self.target_type = sklearn.utils.multiclass.type_of_target(self.y_true)

            self.is_regressor = self.target_type.startswith("continuous")
            self.is_classifier = self.target_type in target_type_classifier

    def generate_prediction(self):

        """
        Generate prediction using model.predict()

        Returns: Predictions, the output from running Keras model.predict() (labeled as y_pred).
                For Keras classification models, calling model.predict() outputs prediction probabilities.
                Generate y_pred_label by comparing prediction probabilities with threshold (default 0.5)

        """
        try:
            # Apply trained model to make predictions on given dataset
            y_pred = self.model.predict(self.X)
            self.y_pred = y_pred.astype("float64")

            target_type_classifier = ["binary", "multiclass", "multiclass-multioutput",
                                      "multilabel-indicator"]

            # Check if type_of_target of y_true is continuous (indicating regression model)
            if not sklearn.utils.multiclass.type_of_target(self.y_true) in target_type_classifier:
                return

            # for classification model, generate y_pred_proba array and y_pred_label array
            self.model_classes = sklearn.utils.multiclass.unique_labels(self.y_true)

            # for binary classification model
            if (sklearn.utils.multiclass.type_of_target(self.y_true) == "binary") or (len(self.model_classes) == 2):
                self.y_pred_proba = np.c_[1 - self.y_pred, self.y_pred]
                self.y_pred_label = (self.y_pred > self.pred_thld).astype(int)

            # when there are more than two label classes
            elif len(self.model_classes) > 2:
                # user uses SparseCategoricalCrossentropy as loss function
                if sklearn.utils.multiclass.type_of_target(self.y_true).startswith("multiclass") or \
                        (not sklearn.utils.multiclass.is_multilabel(self.y_true)):
                    self.y_pred_proba = self.y_pred
                    self.y_pred_label = np.argmax(self.y_pred, axis=1)

                # user uses CategoricalCrossentropy (y labels using one-hot representation)
                elif sklearn.utils.multiclass.type_of_target(self.y_true).startswith("multilabel") or \
                        sklearn.utils.multiclass.is_multilabel(self.y_true):
                    self.y_pred_proba = self.y_pred
                    self.y_pred_label = (self.y_pred > self.pred_thld).astype(int)

        except Exception:
            import traceback
            traceback.print_exc()

    def populate_metrics(self):
        """

        Returns: populate metrics based on model type (regressor or classifier)

        """
        # create a folder to save images or csv files generated from calling report metrics method
        report_metrics_path = MOUNT_PATH + "/report_metrics"
        if not os.path.exists(report_metrics_path):
            os.makedirs(report_metrics_path)

        # if the model is a regressor, populate regressor metrics
        if self.is_regressor:
            self.populate_metrics_regressor()

        # if the model is a classifier, populate classifier metrics
        elif self.is_classifier:
            self.populate_metrics_classifier()

    def calculate_metrics_general_sk(self, sklearn_metrics):
        """
        Calculate and record metrics available in sklearn library
        Args:
            sklearn_metrics: sklearn metrics APIs, automatically assigned by KerasComponent based on model type.

        Returns:
            metrics values and metrics name

        """

        for metric in sklearn_metrics:
            try:
                self.metrics_value.append(round(metric(self.y_true, self.y_pred_label), 2))
                self.metrics_name.append(self.get_name(metric) + self.dataset_name)
            except Exception:
                import traceback
                traceback.print_exc()

    def calculate_metrics_general_ke(self, keras_metrics):
        # calculate metrics that use y_true and y_pred as inputs
        """
        Calculate and record metrics available in Keras library
        Args:
            keras_metrics: Keras metrics APIs, automatically assigned by KerasComponent based on model type.

        Returns:
            metrics values and metrics name

        """
        for metric in keras_metrics:
            try:
                self.metrics_value.append(round(metric(self.y_true, self.y_pred).numpy(), 2))
                self.metrics_name.append(self.get_name(metric) + self.dataset_name)
            except Exception:
                import traceback
                traceback.print_exc()

    def populate_metrics_regressor(self):
        """
        Populate metrics for regression models using sklearn and Keras metrics APIs

        Returns: metrics values and metrics name

        """

        # collect sklearn regression metrics
        sklearn_reg_metrics = [sklearn.metrics.mean_absolute_error,
                           sklearn.metrics.mean_squared_error,
                           sklearn.metrics.mean_squared_log_error,
                           sklearn.metrics.median_absolute_error,
                           sklearn.metrics.explained_variance_score,
                           sklearn.metrics.max_error,
                           sklearn.metrics.r2_score,
                           sklearn.metrics.mean_poisson_deviance,
                           sklearn.metrics.mean_gamma_deviance,
                           sklearn.metrics.mean_tweedie_deviance]

        self.calculate_metrics_general_sk(sklearn_reg_metrics)

        # collect keras regression metrics
        keras_reg_metrics = [#losses.MeanAbsoluteError(),     #repeated
                             #losses.MeanSquaredError(),      #repeated
                             #losses.MeanSquaredLogarithmicError(),   #repeated
                             losses.MeanAbsolutePercentageError(),
                             losses.CosineSimilarity(),
                             losses.Huber(),
                             losses.LogCosh()]

        self.calculate_metrics_general_ke(keras_reg_metrics)


    def populate_metrics_classifier(self):
        """
        Populate metrics, save classification report (as csv file) and plot confusion matrix, roc curve and
        precision and recall curve for all three type of classifiers ((binary, multiclass or multilable).
        KerasComponent leverage metrics available from sklearn and Keras library. The metrics that will be
        finally reported depends on the classifier type (binary, multiclass or multilable)

        Returns: metrics values and metrics name

        """

        # save classification report for all types
        self.save_classification_report()

        # Calculate metrics that use y_true and y_pred_proba as inputs
        if self.y_pred_proba is not None:
            self.populate_metrics_yproba([sklearn.metrics.log_loss])

        # check if the model is binary classifier:
        if sklearn.utils.multiclass.unique_labels(self.y_true).size == 2:
            # populate metrics for binary classification task
            self.populate_metrics_binary()

            # plot confusion matrix for binary classification task
            self.plot_confusion_matrix()

            if self.y_pred_proba is not None:
                # plot roc curve for binary classifier
                self.plot_roc_curve_binary()

                # plot precision_recall_curve for binary classifier
                self.plot_precision_recall_curve_binary()

        # check if the model is a multiclass or multilabel classifier
        elif sklearn.utils.multiclass.unique_labels(self.y_true).size > 2:
            # plot precision recall curve for multiclass or multilabels
            self.plot_precision_recall_curve_multi()

            # plot roc curve for multiclass or multilabels
            self.plot_roc_curve_multi()

            # for multiclass classification
            if not sklearn.utils.multiclass.is_multilabel(self.y_true):
                self.populate_metrics_multiclass()

                # plot confusion matrix for binary or multiclass classification task
                self.plot_confusion_matrix()

            # for multilabel classification
            elif sklearn.utils.multiclass.is_multilabel(self.y_true):
                self.populate_metrics_multilabel()

                # plot multilabel confusion matrix for all types of classification task
                self.plot_multilabel_confusion_matrix()



    def populate_metrics_yproba(self, proba_metrics):
        """
        Populate metrics that use y_true and y_pred_proba (predicted probability) as inputs

        Args:
            proba_metrics: list of sklearn metrics APIs

        Returns: metrics values and metrics name

        """
        for metric in proba_metrics:
            try:
                self.metrics_value.append(round(metric(self.y_true, self.y_pred_proba), 4))
                self.metrics_name.append(self.get_name(metric) + self.dataset_name)
            except Exception:
                import traceback
                traceback.print_exc()

    def populate_metrics_binary(self):
        """
        Populate metrics for binary classifiers using both sklearn and Keras library.

        Returns: metrics values and metrics name

        """

        # sklearn metrics using y_true and y_pred(label) as inputs
        sklearn_metrics = [sklearn.metrics.accuracy_score,
                           sklearn.metrics.balanced_accuracy_score,
                           sklearn.metrics.recall_score,
                           sklearn.metrics.precision_score,
                           sklearn.metrics.f1_score,
                           sklearn.metrics.zero_one_loss,
                           sklearn.metrics.hamming_loss,
                           sklearn.metrics.jaccard_score,
                           sklearn.metrics.matthews_corrcoef]

        self.calculate_metrics_general_sk(sklearn_metrics)

        # calculate sklearn metrics that use prediction probabilities as inputs
        if self.y_pred_proba is not None:

            # AUC score
            try:
                fpr, tpr, thresholds = sklearn.metrics.roc_curve(self.y_true, self.y_pred_proba[:, 1])
                self.metrics_value.append(round(sklearn.metrics.auc(fpr, tpr), 2))
                self.metrics_name.append(self.get_name(sklearn.metrics.auc) + self.dataset_name)
            except Exception:
                import traceback
                traceback.print_exc()

            clf_metrics_proba = [sklearn.metrics.roc_auc_score,
                                 sklearn.metrics.average_precision_score,
                                 sklearn.metrics.brier_score_loss]
            # calculate metrics that use y_true and y_pred_proba as inputs
            for metric in clf_metrics_proba:
                try:
                    self.metrics_value.append(round(metric(self.y_true, self.y_pred_proba[:, 1]), 2))
                    self.metrics_name.append(self.get_name(metric) + self.dataset_name)
                except Exception:
                    import traceback
                    traceback.print_exc()

        # add additional metrics from confusion matrix
        try:
            tn, fp, fn, tp = sklearn.metrics.confusion_matrix(self.y_true, self.y_pred_label).ravel()
            self.metrics_value.extend([self.true_negative_rate(tn, fp),
                                       self.false_positive_rate(fp, tn),
                                       self.false_negative_rate(fn, tp)])
            self.metrics_name.extend([self.get_name(self.true_negative_rate) + self.dataset_name,
                                      self.get_name(self.false_positive_rate) + self.dataset_name,
                                      self.get_name(self.false_negative_rate) + self.dataset_name])
        except Exception:
            import traceback
            traceback.print_exc()

        # Keras metrics using y_true and y_pred as inputs
        keras_binary_metrics = [# metrics.BinaryAccuracy(),   #repeated
                                # metrics.AUC(),              #repeated
                                # metrics.Precision(),        #repeated
                                # metrics.Recall(),           #repeated
                                metrics.TruePositives(),
                                metrics.TrueNegatives(),
                                metrics.FalsePositives(),
                                metrics.FalseNegatives(),
                                metrics.PrecisionAtRecall(0.5),
                                metrics.SensitivityAtSpecificity(0.5),
                                metrics.SpecificityAtSensitivity(0.5),
                                losses.BinaryCrossentropy(),
                                losses.Poisson(),
                                losses.Hinge(),
                                losses.SquaredHinge()]

        self.calculate_metrics_general_ke(keras_binary_metrics)


    def true_negative_rate(self, tn, fp):
        """
        Args:
            tn: True Negatives (count)
            fp: False Positives (count)

        Returns: true_negative_rate
        """
        return round(tn / (tn + fp), 2)


    def false_positive_rate(self, fp, tn):
        """
        Args:
            fp: False Positives (count)
            tn: True Negatives (count)

        Returns: false_positive_rate
        """
        return round(fp / (fp + tn), 2)


    def false_negative_rate(self, fn, tp):
        """
        Args:
            fn: False Negatives (count)
            tp: True Positives (count)

        Returns: false_negative_rate
        """
        return round(fn / (fn + tp), 2)


    def populate_metrics_multiclass(self):
        """

        Populate metrics for multiclass classifiers using both sklearn and Keras library.

        Returns: metrics values and metrics name

        """

        sklearn_multicls_metrics = [sklearn.metrics.accuracy_score,
                                   sklearn.metrics.balanced_accuracy_score,
                                   sklearn.metrics.zero_one_loss,
                                   sklearn.metrics.hamming_loss,
                                   sklearn.metrics.matthews_corrcoef]

        self.calculate_metrics_general_sk(sklearn_multicls_metrics)

        # averaging metrics applicable to both multiclass
        averaging_metrics = [sklearn.metrics.f1_score,
                             sklearn.metrics.recall_score,
                             sklearn.metrics.precision_score,
                             sklearn.metrics.jaccard_score]

        for metric in averaging_metrics:
            for avg_method in ["micro", "macro", "weighted"]:
                try:
                    self.metrics_value.append(round(metric(self.y_true, self.y_pred_label, average=avg_method), 4))
                    self.metrics_name.append(self.get_name(metric) + "_" + avg_method + self.dataset_name)
                except Exception:
                    import traceback
                    traceback.print_exc()

        # roc_auc_score
        if self.y_pred_proba is not None:
            for avg_method, config in itertools.product(["macro", "weighted"], ["ovr", "ovo"]):
                try:
                    self.metrics_value.append(round(sklearn.metrics.roc_auc_score(self.y_true, self.y_pred_proba,
                                                                          average=avg_method,
                                                                          multi_class=config), 4))
                    self.metrics_name.append(self.get_name(sklearn.metrics.roc_auc_score)+ "_" + avg_method + "_" +
                                             config + self.dataset_name)


                except Exception:
                    import traceback
                    traceback.print_exc()

        # add applicable Keras metrics APIs
        keras_multiclass_metrics = [losses.SparseCategoricalCrossentropy()]
        self.calculate_metrics_general_ke(keras_multiclass_metrics)


    def populate_metrics_multilabel(self):
        """

        Populate metrics for multilabel classifiers or multiclass classifiers, of which, the target variable using
        one-hot representation) with both sklearn and Keras library

        Returns: metrics values and metrics name

        """
        # collect and calculate sklearn metrics that use y_true, y_pred as inputs
        sklearn_metrics = [sklearn.metrics.accuracy_score,
                           sklearn.metrics.zero_one_loss,
                           sklearn.metrics.hamming_loss,
                           sklearn.metrics.average_precision_score]

        self.calculate_metrics_general_sk(sklearn_metrics)

        # sklearn metrics that needs averaging method
        averaging_metrics = [sklearn.metrics.f1_score,
                             sklearn.metrics.recall_score,
                             sklearn.metrics.precision_score,
                             sklearn.metrics.jaccard_score]

        for metric in averaging_metrics:
            for avg_method in ["micro", "macro", "weighted", "samples"]:
                try:
                    self.metrics_value.append(round(metric(self.y_true, self.y_pred_label, average=avg_method), 4))
                    self.metrics_name.append(self.get_name(metric) + "_" + avg_method + self.dataset_name)
                except Exception:
                    import traceback
                    traceback.print_exc()

        # roc_auc_score
        if self.y_pred_proba is not None:

            # roc_auc_score for multilabel
            for avg_method in ["micro", "macro", "weighted", "samples"]:
                try:
                    self.metrics_value.append(round(sklearn.metrics.roc_auc_score(self.y_true, self.y_pred_proba,
                                                                          average=avg_method), 4))
                    self.metrics_name.append(self.get_name(sklearn.metrics.roc_auc_score) + "_" + avg_method +
                                             self.dataset_name)


                except Exception:
                    import traceback
                    traceback.print_exc()

            # populate ranking metrics
            ranking_metrics = [sklearn.metrics.coverage_error,
                               sklearn.metrics.label_ranking_average_precision_score,
                               sklearn.metrics.label_ranking_loss,
                               sklearn.metrics.ndcg_score]

            self.populate_metrics_yproba(ranking_metrics)

        # add applicable Keras metrics APIs
        keras_multiclass_metrics = [losses.CategoricalCrossentropy(),
                                    losses.Poisson(),
                                    losses.KLDivergence(),
                                    losses.Hinge(),
                                    losses.SquaredHinge(),
                                    losses.CategoricalHinge()]
        self.calculate_metrics_general_ke(keras_multiclass_metrics)

    def plot_precision_recall_curve_binary(self):
        """

        Plot precision recall curve for binary classifiers

        """
        try:
            precision = dict()
            recall = dict()
            average_precision = dict()
            # for i in range(n_classes):
            i = 1
            precision[i], recall[i], _ = sklearn.metrics.precision_recall_curve(self.y_true,
                                                                                self.y_pred_proba[:, 1])
            average_precision[i] = sklearn.metrics.average_precision_score(self.y_true, self.y_pred_proba[:, 1])

            plt.figure()
            plt.step(recall[i], precision[i], where='post')
            plt.xlabel('Recall')
            plt.ylabel('Precision')
            plt.ylim([0.0, 1.05])
            plt.xlim([0.0, 1.0])
            plt.title(
                'Precision-Recall Curve' + self.dataset_name +
                ': Average Precision={0:0.2f}'.format(average_precision[i]))
            plt.show()
            xpresso_save_plot("Precision_recall_curve" + self.dataset_name, output_path=MOUNT_PATH,
                              output_folder="report_metrics/" + self.status_desc)


        except Exception:
            import traceback
            traceback.print_exc()

    def plot_precision_recall_curve_multi(self):
        """
        Plot precision recall curve for multiclass and multilabel classifiers

        """
        try:
            precision = dict()
            recall = dict()
            average_precision = dict()

            y_true_binarized = sklearn.preprocessing.label_binarize(self.y_true, classes=self.model_classes)

            for i in range(len(self.model_classes)):
                precision[i], recall[i], _ = sklearn.metrics.precision_recall_curve(y_true_binarized[:, i],
                                                                                    self.y_pred_proba[:, i])
                average_precision[i] = sklearn.metrics.average_precision_score(y_true_binarized[:, i],
                                                                               self.y_pred_proba[:, i])

            # A "micro-average": quantifying score on all classes jointly
            precision["micro"], recall["micro"], _ = sklearn.metrics.precision_recall_curve(y_true_binarized.ravel(),
                                                                                            self.y_pred_proba.ravel())
            average_precision["micro"] = sklearn.metrics.average_precision_score(y_true_binarized, self.y_pred_proba,
                                                                                 average="micro")
            # Plot all precision_recall curves
            plt.figure(figsize=(10, 6))

            colors = cycle([i for i in mcolors.TABLEAU_COLORS.values()])
            lines = []
            labels = []
            l, = plt.plot(recall["micro"], precision["micro"], color='gold', lw=2)
            lines.append(l)
            labels.append('micro-average Precision-recall (area = {0:0.2f})'
                          ''.format(average_precision["micro"]))
            for i, color in zip(range(len(self.model_classes)), colors):
                l, = plt.plot(recall[i], precision[i], color=color, lw=2)
                lines.append(l)
                labels.append('Precision-recall for class {0} (area = {1:0.2f})'
                              ''.format(i, average_precision[i]))
            fig = plt.gcf()
            fig.subplots_adjust(bottom=0.25)
            plt.xlim([0.0, 1.0])
            plt.ylim([0.0, 1.05])
            plt.xlabel('Recall')
            plt.ylabel('Precision')
            plt.title(
                'Precision-Recall curve' + self.dataset_name + ': micro AP={0:0.2f}'.format(average_precision["micro"]))
            plt.legend(lines, labels, loc="lower left", prop=dict(size=9))
            plt.show()
            xpresso_save_plot("Precision_recall_curve" + self.dataset_name, output_path=MOUNT_PATH,
                              output_folder="report_metrics/" + self.status_desc)

        except Exception:
            import traceback
            traceback.print_exc()

    def plot_confusion_matrix(self):
        """
        Plot confusion matrix for binary or multiclass classifiers

        """
        try:
            cm = sklearn.metrics.confusion_matrix(self.y_true, self.y_pred_label)
            accuracy = np.trace(cm) / float(np.sum(cm))
            cmap = plt.get_cmap('Blues')

            plt.figure(figsize=(8, 6))
            plt.imshow(cm, interpolation='nearest', cmap=cmap)
            plt.colorbar()
            if self.model_classes is not None:
                tick_marks = np.arange(len(self.model_classes))
                plt.xticks(tick_marks, self.model_classes)
                plt.yticks(tick_marks, self.model_classes)
            thresh = cm.max() / 2
            for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
                plt.text(j, i, "{:,}".format(cm[i, j]),
                         horizontalalignment="center",
                         color="white" if cm[i, j] > thresh else "black")

            plt.tight_layout()
            plt.ylabel('True label')
            plt.xlabel('Predicted label\naccuracy={:0.4f}'.format(accuracy))
            plt.title("Confusion Matrix" + self.dataset_name)
            plt.show()
            xpresso_save_plot("Confusion_matrix" + self.dataset_name, output_path=MOUNT_PATH,
                              output_folder="report_metrics/" + self.status_desc)
            logger.info("Confusion Matrix Saved")
        except Exception:
            import traceback
            traceback.print_exc()


    def plot_multilabel_confusion_matrix(self):
        """

        Plot confusion matrix for mulitlabel classifiers

        """
        try:
            # generate multilabel_confusion_matrix
            cm_arrays = sklearn.metrics.multilabel_confusion_matrix(self.y_true, self.y_pred_label)

            fig, axes = plt.subplots(len(cm_arrays), figsize=(6, 10))

            # plot a confusion matrix for each class
            for i in range(len(cm_arrays)):
                df_cm = pd.DataFrame(cm_arrays[i])
                sn.set(font_scale=1)  # for label size
                sn.heatmap(df_cm, annot=True,
                           cmap=plt.cm.Blues, ax=axes[i], fmt='g')
                cm_plot_title = self.get_name(sklearn.metrics.multilabel_confusion_matrix) + self.dataset_name + "_class_" + \
                                str(self.model_classes[i])
                axes[i].set_xlabel('Predicted label')
                axes[i].set_ylabel('True label')
                axes[i].set_title(cm_plot_title)

            fig.tight_layout(pad=1.0)
            plt.show()
            xpresso_save_plot(cm_plot_title, output_path=MOUNT_PATH,
                              output_folder="report_metrics/" + self.status_desc)
            logger.info("Multilable Confusion Matrix Saved")
        except Exception:
            import traceback
            traceback.print_exc()



    def plot_roc_curve_binary(self):
        """
        Plot ROC curve and compute ROC area for binary classifiers

        """
        try:
            fpr = dict()
            tpr = dict()
            roc_auc = dict()
            i = 1
            average_roc_auc_score = sklearn.metrics.roc_auc_score(self.y_true, self.y_pred_proba[:, i])
            fpr[i], tpr[i], _ = sklearn.metrics.roc_curve(self.y_true, self.y_pred_proba[:, i])
            roc_auc[i] = sklearn.metrics.auc(fpr[i], tpr[i])

            plt.figure()

            lw = 1
            plt.plot(fpr[lw], tpr[lw], color='darkorange',
                     lw=lw, label='ROC curve (area = %0.2f)' % roc_auc[lw])
            plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
            plt.xlim([0.0, 1.0])
            plt.ylim([0.0, 1.05])
            plt.xlabel('False Positive Rate')
            plt.ylabel('True Positive Rate')
            plt.title('ROC Curve' + self.dataset_name +
                      ' : AUC ROC={0:0.2f}'.format(average_roc_auc_score))
            plt.legend(loc="lower right")
            plt.show()
            xpresso_save_plot("ROC_curve", output_path=MOUNT_PATH,
                              output_folder="report_metrics/" + self.status_desc)
        except Exception:
            import traceback
            traceback.print_exc()

    def plot_roc_curve_multi(self):
        """

        Plot ROC curve and compute ROC area for multiclass and multilabel classifiers

        """
        if self.y_pred_proba is not None:
            try:
                y_true_binarized = sklearn.preprocessing.label_binarize(self.y_true, classes=self.model_classes)
                fpr = dict()
                tpr = dict()
                roc_auc = dict()
                for i in range(len(self.model_classes)):
                    fpr[i], tpr[i], _ = sklearn.metrics.roc_curve(y_true_binarized[:, i], self.y_pred_proba[:, i])
                    roc_auc[i] = sklearn.metrics.auc(fpr[i], tpr[i])

                # Compute micro-average ROC curve and ROC area
                fpr["micro"], tpr["micro"], _ = sklearn.metrics.roc_curve(y_true_binarized.ravel(), self.y_pred_proba.ravel())
                roc_auc["micro"] = sklearn.metrics.auc(fpr["micro"], tpr["micro"])

                # First aggregate all false positive rates
                all_fpr = np.unique(np.concatenate([fpr[i] for i in range(len(self.model_classes))]))

                # Then interpolate all ROC curves at this points
                mean_tpr = np.zeros_like(all_fpr)
                for i in range(len(self.model_classes)):
                    mean_tpr += np.interp(all_fpr, fpr[i], tpr[i])

                # Finally average it and compute AUC
                mean_tpr /= len(self.model_classes)

                fpr["macro"] = all_fpr
                tpr["macro"] = mean_tpr
                roc_auc["macro"] = sklearn.metrics.auc(fpr["macro"], tpr["macro"])

                # Plot all ROC curves
                plt.figure(figsize=(12, 6))
                lw = 2
                plt.plot(fpr["micro"], tpr["micro"],
                         label='micro-average ROC curve (area = {0:0.2f})'
                               ''.format(roc_auc["micro"]),
                         color='deeppink', linestyle=':', linewidth=4)

                plt.plot(fpr["macro"], tpr["macro"],
                         label='macro-average ROC curve (area = {0:0.2f})'
                               ''.format(roc_auc["macro"]),
                         color='navy', linestyle=':', linewidth=4)

                # create different colors for each class
                colors = cycle([i for i in mcolors.TABLEAU_COLORS.values()])
                for i, color in zip(range(len(self.model_classes)), colors):
                    plt.plot(fpr[i], tpr[i], color=color, lw=lw,
                             label='ROC curve of class {0} (area = {1:0.2f})'
                                   ''.format(i, roc_auc[i]))

                plt.plot([0, 1], [0, 1], 'k--', lw=lw)
                plt.xlim([0.0, 1.0])
                plt.ylim([0.0, 1.05])
                plt.xlabel('False Positive Rate')
                plt.ylabel('True Positive Rate')
                roc_plot_title = 'ROC Curve' + self.dataset_name
                plt.title(roc_plot_title)
                plt.legend(loc="lower right")
                plt.show()
                xpresso_save_plot(roc_plot_title, output_path=MOUNT_PATH,
                                  output_folder="report_metrics/" + self.status_desc)
                logger.info("Multiclass/Multilable ROC Curve Saved")
            except Exception:
                import traceback
                traceback.print_exc()

    def save_classification_report(self):
        """

        Save classification report (as csv file) for classification models

        """

        try:
            cls_report = sklearn.metrics.classification_report(self.y_true, self.y_pred_label,
                                                               output_dict=True)
            cls_report_df = pd.DataFrame(cls_report).transpose()
            # save classification report to data folder
            pickle.dump(cls_report_df, open(os.path.join(MOUNT_PATH, "report_metrics/classification_report"
                                                         + self.dataset_name + ".pkl"), 'wb'))

            logger.info("Classification Report Saved")
        except Exception:
            import traceback
            traceback.print_exc()


    def get_name(self, metric):
        """
        Reformat metrics name to make sure metric from different libraries to be consistent
        Args:
            metrics_name_str: string, metrics name

        Returns: reformatted metrics name

        """

        if "tensorflow.python.keras" in metric.__module__:
            metric_name = metric.__class__.__name__
            metrics_name_split = [w for w in re.split("([A-Z][^A-Z]*)", metric_name) if w]
            reformated_name = " ".join(metrics_name_split)
        else:
            # "sklearn" in metric.__module__ or custom metrics
            metric_name = metric.__name__
            removed_punct = metric_name.replace("_", " ")
            reformated_name = " ".join([w.capitalize() for w in removed_punct.split()])
        return reformated_name

    def plot_learning_curve(self, history=None):
        """
        Generate learning curves(the test, train scores vs epoch) with the saved history of training Keras model.

        Args:
            history: A History object return from Keras model.fit()

        Returns: Learning curve

        """

        # get keys in training history
        hist_keys = [k for k in history.history.keys()]
        # unique metrics in training history
        metrics_list = [hist_keys[i] for i in range(len(hist_keys)) if i < len(hist_keys) / 2]
        # number of metrics
        n_metrics = len(metrics_list)

        # create subplots
        fig, axes = plt.subplots(n_metrics, 1, figsize=(6, 8))

        # plot learning curve for each scoring metrics
        for i in range(n_metrics):
            try:
                # get training and validation scores for the current metric
                results = pd.DataFrame(history.history).iloc[:, [i, i + n_metrics]]

                # collect number of epoch
                num_epoch_range = list(range(1, results.shape[0] + 1))

                # identify best epoch and the score
                if ("loss" in metrics_list[i]) or ("error" in metrics_list[i]):
                    val_score_best = results.iloc[:, 1].min()
                    max_iters_best = results.iloc[:, 1].idxmin() + 1
                else:
                    val_score_best = results.iloc[:, 1].max()
                    max_iters_best = results.iloc[:, 1].idxmax() + 1

                # Plot score vs max_iters
                axes[i].grid()

                axes[i].plot(num_epoch_range,
                             results.iloc[:, 0],
                             color="b",
                             label=results.columns[0])
                axes[i].plot(num_epoch_range,
                             results.iloc[:, 1],
                             color="g",
                             label=results.columns[1])
                axes[i].plot(max_iters_best, val_score_best, 'r*', label='Best num_epoch : ' +
                                                                         str(max_iters_best))
                axes[i].set_xlabel("num_epoch")
                axes[i].set_ylabel(metrics_list[i])
                axes[i].set_title("Keras Learning Curve :" + metrics_list[i])
                axes[i].legend(loc="best")


            except Exception:
                import traceback
                traceback.print_exc()

        fig.tight_layout(pad=3.0)
        plt.show()
        xpresso_save_plot("Keras_learning_curve", output_path=MOUNT_PATH,
                          output_folder="report_metrics/" + self.status_desc)
        logger.info("Keras Learning Curve Saved")



    def explain_shap(self,
                     model,
                     X,
                     shap_explainer=None,
                     shap_explaining_set_size=None,
                     shap_n_individual_instance=1,
                     shap_n_top_features=5,
                     shap_n_top_interaction_features=3,
                     shap_link="identity",
                     shap_feature_names=None):
        """
        Create Shapley plots for the trained model.

       Allows an approximation of shapley values for a given model based on a number of samples
       from data source X

        Args:
             X (dataframe, numpy array, or pixel_values) : dataset to be explain

             model(object) : model to be explained

             shap_explainer (string, optional): choose one of explainer in the following list or if leave as None, the
                                                 method will try each explainer in the list in order until it reaches
                                                 one that that is compatible with the given model type

                                                {"DeepExplainer","GradientExplainer",
                                                "KernelExplainer", "SamplingExplainer"}

            shap_feature_names (list of strings, optional) : a list of column names

            shap_explaining_set_size(int, optional): Number of samples in X to be explained by SHPA.
                                                    Any number ranging from 1 to the size of X set is valid, and if the
                                                    number is smaller than size of X, the explaining set is created by
                                                    random sampling from the X set.
                                                    If pass -1, the entire dataset will be used as explaining set.
                                                    If no values provided, some default numbers will be used according
                                                    to the type of SHAP explainer or data type: for Kernel explainer,
                                                    200 random samples from X will be used. For image dataset, 20 random
                                                    samples from X will be used. For any other type of explainers,
                                                    the entire X set will be used as the explaining set.
                                                    Typically 1K to 10K samples is good for visualization and enough to
                                                    detect patterns in the data.  Users are encouraged to experiment
                                                    with the number of samples in order to determine a value that
                                                    balances explanation accuracy and runtime.

            shap_n_individual_instance (int, optional): Number of individual instance(s)/sample(s)/observation(s) in the
                                                        explaining set to be randomly chosen to create SHAP Force Plots
                                                        and SHAP Decision Plots.  Default is 1.

            shap_n_top_features(int, optional): Number of top features (by SHAP Values) to create SHAP Dependence Plots.
                                                If values is set to -1, all independent features will be used (one SHAP
                                                Dependence Plot for each feature).
                                                Default is 5.

             shap_n_top_interaction_features(int, optional): Number of top features (by SHAP Interaction Values) to
                                                create SHAP Interaction Values Dependence Plot (only supporting
                                                TreeExpaliner for now ).
                                                If values is set to -1, all independent features will be used (one SHAP
                                                Interaction Values Dependence Plot for each pair of features).
                                                Default is 3.

            shap_link (string, optional) : default is 'identity' or use 'logit' to transform log odds to probabilities

            Returns:
                Returns: SHAP plots

        """
        # assign variables
        self.model = model
        # generate explainer based on model type
        self.explainer = self.generate_explainer(X, shap_explainer)

        # end the program if the explainer is None
        if not self.explainer:
            return

        # print the type of SHAP explainer used if explainer generated successfully
        print("SHAP Explainer used : ", self.return_name(self.explainer))

        # assign variables
        self.shap_explaining_set_size = shap_explaining_set_size
        self.shap_n_individual_instance = shap_n_individual_instance
        # define number of top features to plot
        if shap_n_top_features == -1:
            self.shap_n_top_features = X.shape[1]
        else:
            self.shap_n_top_features = shap_n_top_features
        # define number of top features to plot interaction values plots
        if shap_n_top_interaction_features == -1:
            self.shap_n_top_interaction_features = X.shape[1]
        else:
            self.shap_n_top_interaction_features = shap_n_top_interaction_features

        # prepare explaining set
        x_explain = self.generate_explaining_set(X)

        # obtain SHAP value, expected value, interaction values(if support)
        shap_values = self.explainer.shap_values(x_explain)
        expected_value = self.explainer.expected_value
        try:
            shap_interaction_values = self.explainer.shap_interaction_values(x_explain)
        except:
            shap_interaction_values = None

        # obtain feature names
        feature_names = self.shap_generate_feature_names(X, shap_feature_names)

        # For image datasets
        if len(X.shape) > 2:
            try:
                self.shap_plots_image(shap_values=shap_values,
                                      x_explain=x_explain)
            except Exception:
                import traceback
                traceback.print_exc()

        # for two dimensional dataset
        else:
            # check if shap_values have multiple elements, as a result of a multioutput model
            if (isinstance(shap_values, list)) and (len(shap_values) > 1):
                n_outputs = len(shap_values)

                for i in range(n_outputs):
                    # provide a label for the output
                    multioutput_n = f"multioutput_{i}"

                    if shap_interaction_values is not None:
                        shap_interaction_values_i = shap_interaction_values[i]
                    else:
                        shap_interaction_values_i = None

                    self.shap_plots_single_output(shap_values=shap_values[i],
                                                  expected_value=expected_value[i].numpy(),
                                                  shap_interaction_values=shap_interaction_values_i,
                                                  x_explain=x_explain,
                                                  feature_names=feature_names,
                                                  multioutput_n=multioutput_n,
                                                  link=shap_link)

            else:
                multioutput_n = ""
                self.shap_plots_single_output(shap_values=shap_values[0],
                                              expected_value=expected_value[0].numpy(),
                                              shap_interaction_values=shap_interaction_values,
                                              x_explain=x_explain,
                                              feature_names=feature_names,
                                              multioutput_n=multioutput_n,
                                              link=shap_link)

    def generate_explaining_set(self, X):
        """

        Args:
            X: dataframe to be explained (provided by user)

        Returns:
            A dataset to be explained by SHAP explainers

        """
        # Define SHAP explaining set size
        # if user provide a value, apply user's input
        if self.shap_explaining_set_size:
            n_explain = self.shap_explaining_set_size
        # Provide default value if user is not providing number of data to explain
        else:
            # For image datasets, use 20 samples
            if len(X.shape) > 2:
                n_explain = min(20, X.shape[0])
            # use 200 samples to speed up the process as other SHAP explainers are slow
            elif isinstance(self.explainer, shap.KernelExplainer):
                n_explain = min(200, X.shape[0])
            else:
                n_explain = X.shape[0]

        # random sample n_explain number of observations from X dataset
        explain_ind = np.random.choice(X.shape[0], min(n_explain, X.shape[0]), replace=False)

        # if X is a dataframe, convert X to numpy array
        if isinstance(X, pd.DataFrame):
            X = X.to_numpy()

        # create explaining set (x matrix to be explained by SHAP)
        x_explain = X[explain_ind]
        return x_explain

    def shap_generate_feature_names(self, X, shap_feature_names):
        """
        Define feature names for SHAP plots. If user provides feature names(as a list), use user's input, or use column
        names of the X dataframe if exist, or create index-based feature names ("Feature_1", "Feature_2",...)

        Args:
            X: feature matrix
            shap_feature_names: a list of feature names given by users (a parameter of explain_shap)

        Returns:
            feature names for SHAP plots

        """
        # Define feature names
        try:
            if shap_feature_names:
                # if user provide feature names, apply user's inputs
                feature_names = shap_feature_names
            # if X is a dataframe, get feature names from dataframe
            elif isinstance(X, pd.DataFrame):
                feature_names = X.columns.values.tolist()
            # else, label feature with index
            else:
                feature_names = [f"Feature_{i}".format(i) for i in range(X.shape[1])]
            return feature_names

        except Exception:
            import traceback
            traceback.print_exc()

    # Define function to generate SHAP explainer
    def generate_explainer(self,X, shap_explainer):
        """

        Args:
            X: dataset to be explained by SHAP
            shap_explainer: user's preferred SHAP explainer to used (optional)

        Returns:
            A SHAP explainer

        """

        # get background data as numpy array, use default 100 samples
        if isinstance(X, pd.DataFrame):
            X = X.to_numpy()
        bdata = X[np.random.choice(X.shape[0], min(1000, X.shape[0]), replace=False)]

        # Listing of SHAP Explainers for deep learning models
        shapExplainers = [shap.DeepExplainer,
                          shap.GradientExplainer,
                          shap.KernelExplainer,
                          shap.SamplingExplainer]
        shapExplainers_names = ["DeepExplainer",
                                "GradientExplainer",
                                "KernelExplainer",
                                "SamplingExplainer"]
        shapExplainer_dict = dict(zip(shapExplainers_names, shapExplainers))

        # if user choose a SHAPExplainer, go with user's preference
        if shap_explainer:
            if shap_explainer in ["SamplingExplainer", "KernelExplainer"]:
                if shap_explainer == "KernelExplainer":
                    #if using KernelExplainer, the size of background data is limited to 100 otherwise shap gives a warning
                    bdata = bdata[:100]
                try:
                    explainer = shapExplainer_dict[shap_explainer](self.model.predict, bdata)
                    return explainer
                except Exception:
                    import traceback
                    traceback.print_exc()
            else:
                try:
                    explainer = shapExplainer_dict[shap_explainer](self.model, bdata)
                    return explainer
                except Exception:
                    import traceback
                    traceback.print_exc()

        # if user doesn't provide a SHAPExplainer of choice or the SHAPExplainer provided by user is not compatible with
        # the model, try other SHAPexplainers in the order of the list
        for i in shapExplainers_names:
            if i in ["SamplingExplainer", "KernelExplainer"]:
                if shap_explainer == "KernelExplainer":
                    # if using KernelExplainer, the size of background data is limited to 100 otherwise shap gives a warning
                    bdata = bdata[:100]
                try:
                    explainer = shapExplainer_dict[i](self.model.predict, bdata)
                    return explainer
                except Exception:
                    import traceback
                    traceback.print_exc()
            # for any other type explainer
            else:
                try:
                    explainer = shapExplainer_dict[i](self.model, bdata)
                    return explainer
                except Exception:
                    import traceback
                    traceback.print_exc()

        return None

    def shap_plots_image(self, shap_values, x_explain):

        # plot the feature attributions
        shap.image_plot(shap_values, -x_explain)

    def shap_plots_single_output(self, shap_values,
                                 expected_value,
                                 shap_interaction_values,
                                 x_explain,
                                 feature_names,
                                 multioutput_n,
                                 link):
        """
        Greate SHAP plots for single output of regressor or classifier model


        """

        # save features and shap values as dataframe in csv file
        self.save_shap_values_df(feature_names, multioutput_n, shap_values)

        # Summary Plot with SHAP Values (dot plot)
        self.shap_summary_plot_dot(feature_names, multioutput_n, shap_values, x_explain)

        # # Summary Plot with SHAP mean absolute Values (bar plot)
        self.shap_summary_plot_bar(feature_names, multioutput_n, shap_values, x_explain)

        # dependence_plot for top n most important features
        self.shap_dependence_plot(feature_names, multioutput_n, shap_values, x_explain)

        # SHAP force plot collective
        self.shap_force_plot_collective(expected_value, feature_names, link, multioutput_n, shap_values, x_explain)

        # SHAP force plot single instance
        self.shap_force_plot_single_instance(expected_value, feature_names, link, multioutput_n,
                                             shap_values, x_explain)

        # SHAP decision plot collective
        self.shap_decision_plot_collective(expected_value, feature_names, link,
                                                                           multioutput_n, shap_values, x_explain)

        # SHAP decision plot single instance
        self.shap_decision_plot_single_instance(expected_value, feature_names, link, multioutput_n,
                                                shap_values, x_explain)

        # SHAP waterfall plots
        self.shap_waterfall_plots(expected_value, feature_names, multioutput_n, shap_values, x_explain)

        # if SHAP interaction values are available
        if shap_interaction_values is not None:

            # SHAP summary plot with interaction values
            self.shap_summary_plot_interaction(feature_names, multioutput_n, shap_interaction_values, x_explain)

            # Heatmap by SHAP Interaction Values
            self.shap_inter_values_heatmap(feature_names, multioutput_n,
                                                                   shap_interaction_values)

            # SHAP dependence plot with interaction values
            self.shap_dependence_plot_interaction(feature_names, multioutput_n, shap_interaction_values, x_explain)

            # SHAP interaction values decision plot
            self.shap_decision_plot_interaction(shap_values, expected_value, feature_names, link, multioutput_n,
                                                shap_interaction_values, x_explain)

    def shap_summary_plot_bar(self, feature_names, multioutput_n, shap_values, x_explain):
        """
        Create SHAP Summary Plot - SHAP Values(mean absolute value)

        """
        try:
            shap.summary_plot(shap_values,
                              x_explain,
                              feature_names=feature_names,
                              plot_type="bar",
                              show=False)
            plt.title(f"Feature Importance by SHAP Values(mean absolute value) {multioutput_n}")
            plt.show()
            xpresso_save_plot(f"shap_values_summary_bar_plot{multioutput_n}", output_path=MOUNT_PATH,
                              output_folder="report_metrics/")
        except Exception:
            import traceback
            traceback.print_exc()

    def shap_summary_plot_interaction(self, feature_names, multioutput_n, shap_interaction_values, x_explain):
        """
        Create SHAP Summary Plot with SHAP Interaction Values

        """
        try:
            shap.summary_plot(shap_interaction_values,
                              x_explain,
                              plot_type="compact_dot",
                              feature_names=feature_names,
                              show=False)
            plt.title(f"SHAP Interaction Value Summary Plot {multioutput_n}")
            plt.show()
            xpresso_save_plot(f"shap_interaction_values_summary_plot{multioutput_n}", output_path=MOUNT_PATH,
                              output_folder="report_metrics/")
        except Exception:
            import traceback
            traceback.print_exc()

    def shap_decision_plot_interaction(self, shap_values, expected_value, feature_names, link, multioutput_n,
                                       shap_interaction_values, x_explain):
        """
        Create SHAP Decision Plot with SHAP Interaction Values

        """
        # use up to 200 samples for better visualization
        sm_x_explain = min(200, len(shap_values))

        if shap_interaction_values is not None:
            try:
                shap.decision_plot(expected_value,
                                   shap_interaction_values[0:sm_x_explain],
                                   x_explain[0:sm_x_explain],
                                   feature_names=feature_names,
                                   link=link,
                                   show=False)
                plt.title(f"SHAP Interaction Values Decision Plot (collective) {multioutput_n}")
                plt.show()
                xpresso_save_plot(f"shap_interaction_values_decision_plot_collective{multioutput_n}",
                                  output_path=MOUNT_PATH,
                                  output_folder="report_metrics/")
            except Exception:
                import traceback
                traceback.print_exc()

    def shap_decision_plot_single_instance(self, expected_value, feature_names, link, multioutput_n,
                                           shap_values, x_explain):

        """
        Create SHAP Decision Plot with one instance/observation

        """
        # Individual Decision Plot
        for ob in self.random_obs:
            try:
                # plot the SHAP values for the randomly selected observations
                shap.decision_plot(expected_value,
                                   shap_values[ob],
                                   x_explain[ob],
                                   feature_names=feature_names,
                                   link=link,
                                   show=False)
                plt.title(f"SHAP Decision Plot of observation {ob} {multioutput_n}")
                plt.show()
                xpresso_save_plot(f"shap_decision_plot_sample#{ob}{multioutput_n}",
                                  output_path=MOUNT_PATH,
                                  output_folder="report_metrics/")
            except Exception:
                import traceback
                traceback.print_exc()

    def shap_decision_plot_collective(self, expected_value, feature_names, link, multioutput_n, shap_values, x_explain):
        """
        Create SHAP Decision Plot with a number of instances/observations

        """
        # For better visualization, limit to 200 samples, otherwise SHAP gives a warning
        sm_x_explain = min(200, len(shap_values))
        try:
            rcParams['axes.titlepad'] = 24
            shap.decision_plot(expected_value,
                             shap_values[0:sm_x_explain],
                             x_explain[0:sm_x_explain],
                             feature_names=feature_names,
                             link=link,
                             return_objects=True,
                             show=False)
            plt.title(f"SHAP Decision Plot (collective) {multioutput_n}")
            plt.show()
            xpresso_save_plot(f"shap_decision_plot_collective{multioutput_n}", output_path=MOUNT_PATH,
                              output_folder="report_metrics/")
        except Exception:
            import traceback
            traceback.print_exc()



    def shap_force_plot_single_instance(self, expected_value, feature_names, link, multioutput_n, shap_values,
                                        x_explain):
        """
        Create SHAP Force Plot with one random sampled instance/observation

        """
        # Randome Sample observations
        self.random_obs = random.sample(range(len(x_explain)), self.shap_n_individual_instance)
        # Individual force plot
        for ob in self.random_obs:
            try:
                # plot the SHAP values for the random sampled observations
                rcParams['axes.titlepad'] = 24
                ind_force_plot = shap.force_plot(expected_value,
                                                 shap_values[ob],
                                                 x_explain[ob],
                                                 feature_names=feature_names,
                                                 link=link)
                shap.save_html(
                    os.path.join(MOUNT_PATH, f"report_metrics/shap_force_plot_sample#{ob}{multioutput_n}.html"),
                    ind_force_plot)
            except Exception:
                import traceback
                traceback.print_exc()


    def shap_force_plot_collective(self, expected_value, feature_names, link, multioutput_n, shap_values, x_explain):
        """
        Create SHAP Force Plot with a number of instances/observations

        """
        try:
            rcParams['axes.titlepad'] = 24
            force_plot = shap.force_plot(expected_value,
                                         shap_values,
                                         x_explain,
                                         feature_names=feature_names,
                                         link=link)
            shap.save_html(os.path.join(MOUNT_PATH, f"report_metrics/shap_force_plot(collective){multioutput_n}.html"),
                           force_plot)
        except Exception:
            import traceback
            traceback.print_exc()

    def shap_waterfall_plots(self, expected_value, feature_names, multioutput_n, shap_values, x_explain):
        """
        Create SHAP Waterfall Plot with a number of instances/observations

        """
        # make sure expected value is a float value instead of a array containing the float value
        try:
            expected_value = expected_value[0]
        except Exception:
            import traceback
            traceback.print_exc()

        # plot the waterfall plots for the random selected observations
        for ob in self.random_obs:
            try:
                shap.waterfall_plot(expected_value,
                                    shap_values[ob],
                                    x_explain[ob],
                                    feature_names=feature_names,
                                    show=False)
                plt.title(f"SHAP Waterfall Plot of observation {ob} {multioutput_n}")
                plt.show()
                xpresso_save_plot(f"shap_waterfall_plot_sample#{ob}{multioutput_n}",
                                  output_path=MOUNT_PATH,
                                  output_folder="report_metrics/")
            except Exception:
                import traceback
                traceback.print_exc()

    def shap_dependence_plot(self, feature_names, multioutput_n, shap_values, x_explain):
        """"
        Create SHAP Dependence Plot for a number of features (determined by users or default is 5)

        """
        rank_inds = np.argsort(-np.sum(np.abs(shap_values), 0))
        top_inds = rank_inds[:self.shap_n_top_features]
        for i in top_inds:
            try:
                shap.dependence_plot(feature_names[i],
                                     # f"rank({i})".format(i),
                                     shap_values,
                                     x_explain,
                                     feature_names=feature_names,
                                     show=False)
                plt.title(f"Dependence Plot : {feature_names[i]} {multioutput_n}")
                plt.show()
                xpresso_save_plot(f"shap_dependence_plot_{feature_names[i]}{multioutput_n}", output_path=MOUNT_PATH,
                                  output_folder="report_metrics/")
            except Exception:
                import traceback
                traceback.print_exc()

    def shap_dependence_plot_interaction(self, feature_names, multioutput_n, shap_interaction_values, x_explain):
        """
        Create SHAP Dependence Plot with SHAP interaction values for a number of feature pairs (determined by users or
        default is 3)

        """

        # sort features based on interaction values(absolute)
        tmp_abs = np.abs(shap_interaction_values).sum(0)
        for i in range(tmp_abs.shape[0]):
            tmp_abs[i, i] = 0
        sorted_inds = np.argsort(-tmp_abs.sum(0))
        sorted_features_inter = [feature_names[ind] for ind in sorted_inds]

        # Plot n top features interaction values dependence plot, default is 5
        top_features_inter = sorted_features_inter[0:self.shap_n_top_interaction_features]
        top_feature_combs = []
        for c in combinations(top_features_inter, 2):
            top_feature_combs.append(c)
        for i in range(len(top_feature_combs)):
            try:
                fea_0 = top_feature_combs[i][0]
                fea_1 = top_feature_combs[i][1]
                # make plots
                shap.dependence_plot((fea_0, fea_1),
                                     shap_interaction_values,
                                     x_explain,
                                     feature_names=feature_names,
                                     show=False)
                plt.title(f"Dependence Plot Interaction Values: {fea_0} vs {fea_1}")
                plt.show()
                xpresso_save_plot(f"shap_dependence_plot_{fea_0}_vs_{fea_1}{multioutput_n}", output_path=MOUNT_PATH,
                                  output_folder="report_metrics/")
            except Exception:
                import traceback
                traceback.print_exc()

    def shap_inter_values_heatmap(self, feature_names, multioutput_n, shap_interaction_values):
        """

        Create a heat map for SHAP Interaction Values (Within SHAP library, interaction values are only available for
        TreeExplainer for now)

        """
        try:
            # sort features based on interaction values(absolute)
            tmp_abs = np.abs(shap_interaction_values).sum(0)
            for i in range(tmp_abs.shape[0]):
                tmp_abs[i, i] = 0
            sorted_inds = np.argsort(-tmp_abs.sum(0))
            sorted_features_inter = [feature_names[ind] for ind in sorted_inds]

            # sum interaction values across all samples for each feature
            tmp = shap_interaction_values.sum(0)
            # re-arrange heatmap feature based on interaction values (absoluate value) use top 20 features
            tmp_inds = sorted_inds[0: max(20, self.shap_n_top_features)]
            tmp_ranked = tmp[tmp_inds, :][:, tmp_inds]
            tem_feature_inter = [feature_names[ind] for ind in tmp_inds]

            # plot heatmap
            plt.figure(figsize=(12, 12))
            heat_map = sn.heatmap(tmp_ranked, annot=True, cmap="YlGnBu")
            plt.ylabel('Features')
            heat_map.set_yticklabels(tem_feature_inter,
                                     rotation=50.4,
                                     horizontalalignment="right")
            heat_map.set_xticklabels(tem_feature_inter,
                                     rotation=50.4,
                                     horizontalalignment="left")
            plt.title(f"Heatmap by SHAP Interaction Values {multioutput_n}")
            plt.gca().xaxis.tick_top()
            plt.show()
            xpresso_save_plot(f"shap_heatmap_by_interaction_values{multioutput_n}", output_path=MOUNT_PATH,
                              output_folder="report_metrics/")
        except Exception:
            import traceback
            traceback.print_exc()


    def shap_summary_plot_dot(self, feature_names, multioutput_n, shap_values, x_explain):
        """
        Create SHAP summary plots (dot plot type)

        """
        try:
            shap.summary_plot(shap_values,
                              x_explain,
                              feature_names=feature_names,
                              show=False)
            plt.title(f"Feature Importance by SHAP Values {multioutput_n}")
            plt.ylabel("Features")
            plt.show()
            xpresso_save_plot(f"shap_values_summary_plot{multioutput_n}", output_path=MOUNT_PATH,
                              output_folder="report_metrics/")
        except Exception:
            import traceback
            traceback.print_exc()


    def save_shap_values_df(self, feature_names, multioutput_n, shap_values):
        """

        Compute SHAP Values for the explaining set and save as a dataframe in NFS

        """
        # Rank features based on SHAP Values (sum of the absolute value of shap values)
        shap_values_abs = (np.abs(shap_values)).sum(axis=0)
        sorted_features = [f for _, f in sorted(zip(shap_values_abs, feature_names), reverse=True)]
        sorted_shap_values = [round(v, 4) for v, _ in sorted(zip(shap_values_abs, feature_names),
                                                             reverse=True)]
        # save SHAP values to a dataframe
        try:
            shap_values_df = pd.DataFrame({"features": sorted_features,
                                           "SHAP_values": sorted_shap_values})
            shap_values_df.to_csv(f"shap_values" + multioutput_n + ".csv")
        except Exception:
            import traceback
            traceback.print_exc()


    def return_name(self, shap_explainer_obj):
        type_str = re.search(r"<class '(.*)'>", str(type(shap_explainer_obj)))
        return type_str.group(1)


    def send_metrics(self, metrics, status_desc="reporting metrics"):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": status_desc},
                "metric": metrics
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=True, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        try:
            # create Output folder
            if not os.path.exists(self.OUTPUT_DIR):
                os.makedirs(self.OUTPUT_DIR)

            # save keras model
            self.model.save(os.path.join(self.OUTPUT_DIR, 'model_saved.h5'))
            self.model.save(os.path.join(self.MOUNT_PATH, 'model_saved.h5'))
            self.model.save_weights(os.path.join(self.MOUNT_PATH, 'saved_weights.h5'),
                                    overwrite=True, save_format="hs")
        except:
            pass

        try:
            super().completed(push_exp=push_exp, success=success)

        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    # if __name__ == "__main__":
    #     # To run locally. Use following command:
    #     # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py
    #
    #     data_prep = SKLearnComponent()
    #     if len(sys.argv) >= 2:
    #         data_prep.start(run_name=sys.argv[1])
    #     else:
    #         data_prep.start(run_name="")
